﻿using System.ServiceProcess;
using Pvs.Autopost.Core;
using Pvs.Pastebin.CodeCollectorServer;

namespace Pvs.Pastebin.CollectorService
{
   public partial class PastebinCollectorService : ServiceBase
   {
      private IPostService _pastebinPostCollector;
      private const string DefaultPastebinCollectorDir = "d:\\PastebinPosts";

      public PastebinCollectorService()
      {
         InitializeComponent();
      }

      protected override void OnStart(string[] args)
      {
         _pastebinPostCollector = new PastebinPostCollector(DefaultPastebinCollectorDir);
         _pastebinPostCollector.Start();
      }

      protected override void OnStop()
      {
         _pastebinPostCollector.Stop();
      }

      protected override void OnPause()
      {
         _pastebinPostCollector.Suspend();
      }

      protected override void OnContinue()
      {
         _pastebinPostCollector.Resume();
      }
   }
}
