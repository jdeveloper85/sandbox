﻿using System;
using System.Collections.Generic;
using System.Linq;
using CppQuiz.Data;
using WatchfulEye.DataLayer;

namespace InsertLogoSandbox
{
   public sealed class BatchLogosSaver
   {
      internal static readonly IDictionary<string, string> ProjectNameLogoMap = new Dictionary<string, string>
      {
         {"G3D Content Pak", "Logos/3d-content-pak-logo.png"},
         {"Blender", "Logos/blender_logo.png"},
         {"CamStudio", "Logos/camstudio-logo.png"},
         {"Chromium", "Logos/chromium-logo.png"},
         {"Doom 3", "Logos/doom3-logo.png"},
         {"Eigen", "Logos/eigen-logo.png"},
         {"Ffdshow", "Logos/ffdshow-logo.png"},
         {"Geant4 software", "Logos/g4-logo.png"},
         {"Miranda IM", "Logos/miranda-logo.png"},
         {"MySQL", "Logos/mysql-logo.png"},
         {"NetXMS", "Logos/netxms_logo.png"},
         {"OpenCV", "Logos/opencv-logo.png"},
         {"OpenMW", "Logos/openmw-logo.png"},
         {"OpenSSL", "Logos/openssl-logo.png"},
         {"Qt", "Logos/qt-logo.png"},
         {"Quake-III-Arena", "Logos/quake3-logo.png"},
         {"ReactOS", "Logos/reactos-logo.png"},
         {"Scilab", "Logos/scilab_logo.png"},
         {"Tesseract", "Logos/tesseract.png"},
         {"VirtualDub", "Logos/virtualdub-logo.png"},
         {"Source Engine SDK", "Logos/sourceengine_logo.png"},
         {"Intel AMT SDK", "Logos/intel-logo.png"},
         {"SMHasher", "Logos/smhasher.png"},
         {"ABackup", "Logos/abackup-logo.png"},
         {"Xpdfwin", "Logos/xpdf-win-logo.png"}
      };

      private readonly DatabaseType _databaseType;

      public BatchLogosSaver(DatabaseType databaseType)
      {
         _databaseType = databaseType;
      }

      /// <summary>
      ///    Вставка логотипов проектов
      /// </summary>
      public int Go()
      {
         switch (_databaseType)
         {
            case DatabaseType.WatchfulEye:
               using (var eyeEntities = new WatchfulEyeEntities())
               {
                  foreach (var projectLogoPair in ProjectNameLogoMap)
                  {
                     var localPair = projectLogoPair;
                     var questions = from question in eyeEntities.Questions
                                     where question.ProjectName == localPair.Key
                                     select question;
                     if (!questions.Any())
                        continue;

                     foreach (var question in questions)
                     {
                        var anImagePath = projectLogoPair.Value;
                        byte[] imageData;
                        if (!string.IsNullOrEmpty(anImagePath) &&
                            (imageData = BitmapUtils.GetImageData(anImagePath)) != null)
                           question.ProjectLogo = imageData;
                     }
                  }

                  return eyeEntities.SaveChanges();
               }
            case DatabaseType.CppQuiz:
               using (var quizEntities = new QuizEntities())
               {
                  foreach (var projectLogoPair in ProjectNameLogoMap)
                  {
                     var localPair = projectLogoPair;
                     var questions = from question in quizEntities.QuizQuestions
                                     where question.ProjectName == localPair.Key
                                     select question;
                     if (!questions.Any())
                        continue;

                     foreach (var question in questions)
                     {
                        var anImagePath = projectLogoPair.Value;
                        byte[] imageData;
                        if (!string.IsNullOrEmpty(anImagePath) &&
                            (imageData = BitmapUtils.GetImageData(anImagePath)) != null)
                           question.ProjectLogo = imageData;
                     }
                  }

                  return quizEntities.SaveChanges();
               }
            default:
               throw new NotSupportedException(string.Format("Not supported database type {0}", _databaseType));
         }
      }
   }
}