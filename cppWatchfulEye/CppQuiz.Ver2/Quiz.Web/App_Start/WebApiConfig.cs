﻿using System;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.WebHost;

namespace Quiz.Web
{
   public static class WebApiConfig
   {
      public static void Register(HttpConfiguration config)
      {
         var httpControllerRouteHandler = typeof(HttpControllerRouteHandler).GetField("_instance",
            BindingFlags.Static | BindingFlags.NonPublic);
         if (httpControllerRouteHandler != null)
         {
            httpControllerRouteHandler.SetValue(null,
               new Lazy<HttpControllerRouteHandler>(() => new SessionHttpControllerRouteHandler(), true));
         }

         config.MapHttpAttributeRoutes();
         config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });
      }
   }
}