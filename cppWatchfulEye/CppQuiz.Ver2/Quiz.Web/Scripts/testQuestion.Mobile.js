﻿/// <reference path="jquery-2.1.3.js" />
var intervalValue = 1000;
var qId = 0;
var qNumber = 0;
var secondsLeft = 0;
var timeIsOut = false;
var timeout = null;
var answered = false;
var dataIndex = -1;
var isLastQuestion = false;
var dontKnowActionText = 'Don\'t know';
var resultsText = 'Results';
var nextQuestionText = 'Next';
var secondsLeftText = 'Seconds left: ';
var timeIsOutText = 'Time is out';
var testCompletePage = 'TestComplete.aspx';
var currentUserScore = 0;
var isCorrectCurrentAnswer = false;

// Обновление логотипа проекта
function getProjectLogo(questionId) {
    $('#projectLogo').empty();
    var imgLogo = new Image();
    imgLogo.src = 'GetLogoHandler.ashx?questionId=' + questionId;
    imgLogo.className = 'img-responsive';
    $('#projectLogo').append(imgLogo);
}

// Получение текущего вопроса
function goToNextQuestion() {
    $('#actionBtn').text(dontKnowActionText);
    $('#confirmAnswerBtn').attr('disabled', true);

    $.ajax({
        url: '/quiz/question',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            // Получаем компоненты вопроса.
            var questionNumber = data.questionNumber;
            var questionId = data.questionId;
            var codeFragment = data.codeFragment;
            var projectName = data.projectName;
            var explanatoryText = data.explanatoryText;
            var pvsStudioWarning = data.pvsStudioWarning;
            var qSecondsLeft = data.qSecondsLeft;
            var lastQuestion = data.isLastQuestion;
            var totalQuestionCount = data.totalQuestionCount;

            qId = questionId;
            qNumber = questionNumber;
            secondsLeft = qSecondsLeft;
            timeIsOut = secondsLeft <= 0;
            answered = false;
            isLastQuestion = lastQuestion;

            $('#currentQuestionNumber').html(questionNumber + ' Of ' + totalQuestionCount);
            $('#codeFragment').html(codeFragment);
            $('#pvsStudioWarning').html(pvsStudioWarning);
            $('#projectName').html('Project name: ' + projectName);
            $('#explanatoryText').html(explanatoryText);
            hideBlocks();
            
            if (timeIsOut) {
                $('#countdown').html(timeIsOutText).css('color', 'red');
            } else {                
                $('#countdown').html(secondsLeftText + secondsLeft).css('color', 'whitesmoke');
                if (timeout) {
                    clearTimeout(timeout);
                    timeout = null;
                }

                timeout = setInterval(function () {
                    if (secondsLeft === 0) {
                        executeUserAction('TimeIsUp');
                        $('#countdown').html(timeIsOutText).css('color', 'red');
                    } else {
                        secondsLeft--;
                        $('#countdown').html(secondsLeftText + secondsLeft).css('color', 'whitesmoke');
                    }
                }, intervalValue);
            }

            getProjectLogo(questionId);

            // Фиксация выбранного индекса при клике на фрагмент
            $('.highlight-fragment').bind('click', function () {
                $('.highlight-fragment').each(function (index, element) {
                    $(element).css('color', 'whitesmoke');                    
                });
                dataIndex = parseInt($(this).attr('data-index'), 10);
                $('#confirmAnswerBtn').attr('disabled', dataIndex === -1);
                $(this).css('color', 'seagreen');                
            });
        },
        error: function () {
            window.location.replace(testCompletePage);
        }
    });
}

function clearTimer() {
    timeIsOut = true;
    if (timeout) {
        clearTimeout(timeout);
        timeout = null;
    }
}

function setActionBtnText() {
    $('#actionBtn').text(isLastQuestion ? resultsText : nextQuestionText);
}

function executeUserAction(userAction) {
    if (timeIsOut || answered)
        return;

    setActionBtnText();
    $.ajax({
        data: {
            action: userAction,
            dataIndex: dataIndex,
            questionId: qNumber
        },
        datatype: 'http',
        type: 'POST',
        url: '/quiz/stat'
    }).done(function (responseResult) {
        if (!responseResult)
            return;

        var answerState = responseResult.AnswerState;
        var answerRes = '';
        var resColor = 'white';
        if (answerState === 'Right') {
            answerRes = 'RIGHT';
            resColor = 'red';
        }
        else if (answerState === 'Wrong') {
            answerRes = 'WRONG';
            resColor = 'green';
        }
        $('#resultText').text(answerRes).css('color', resColor);

        var resDataIndex = responseResult.DataIndex;
        var correctIndices = responseResult.CorrectIndices;
        currentUserScore = responseResult.CurrentCorrectAnswersNumber;
        $('.highlight-fragment').each(function (index, element) {
            var dataIndexValue = parseInt($(element).attr('data-index'), 10);
            if (dataIndexValue === resDataIndex) {
                $(element).css('font-weight', 'bolder').css('text-decoration', 'underline');
            }

            var isCorrectIndex = false;
            for (var i = 0; i < correctIndices.length; i++) {
                if (dataIndexValue === correctIndices[i]) {
                    isCorrectIndex = true;
                    break;
                }
            }

            if (isCorrectIndex) {
                $(element).css('color', 'red').css('font-weight', 'bolder');
            }
        });

        clearTimer();
        showBlocks();
        setActionBtnText();
        $('#currentScore').text('Score is: ' + currentUserScore);
    });

    answered = true;
}


function hideBlocks() {
    $('#exp, #warn, #currentScore, #resultText').hide();
    $('#noerrorsBtn, #confirmAnswerBtn').show();    
}

function showBlocks() {
    $('#exp, #warn, #currentScore, #resultText').show();
    $('#noerrorsBtn, #confirmAnswerBtn').hide();
}

$(document).bind('pageinit', function () {
    $('#actionBtn').text(dontKnowActionText);
    $('#noerrorsBtn').text('No Errors');
    $('#confirmAnswerBtn').text('Answer').attr('disabled', true);

    // Отправка ответа на вопрос
    $('#confirmAnswerBtn').click(function (e) {
        executeUserAction('TryToAnswer');
        if (e) {
            e.preventDefault();
        }
    });

    // Отправка ответа, что ошибок в коде нет
    $('#noerrorsBtn').click(function (e) {
        executeUserAction('NoErrors');
        if (e) {
            e.preventDefault();
        }
    });

    // Отправка действия при клике на кнопку                
    $('#actionBtn').click(function (e) {
        switch ($('#actionBtn').text()) {
            case dontKnowActionText:
                executeUserAction('DoesNotKnow');
                break;
            case nextQuestionText:
                goToNextQuestion();
                break;
            case resultsText:
                window.location.replace(testCompletePage + '?userScore=' + currentUserScore);
                break;
        }

        if (e) {
            e.preventDefault();
        }
    });    

    // Получаем текущий вопрос            
    goToNextQuestion();
});