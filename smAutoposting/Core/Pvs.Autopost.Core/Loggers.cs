﻿namespace Pvs.Autopost.Core
{
   /// <summary>
   /// Перечисление для доступных логгеров
   /// </summary>
   public enum Loggers
   {
      /// <summary>
      /// Имя лога для установки постов
      /// </summary>
      Autoposter,

      /// <summary>
      /// Имя лога для публикации постов
      /// </summary>
      Autoconsumer
   }
}