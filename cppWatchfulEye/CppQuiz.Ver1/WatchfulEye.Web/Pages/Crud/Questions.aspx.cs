﻿using System;
using System.Linq;
using System.Web.Routing;
using System.Web.UI;
using AjaxControlToolkit;
using WatchfulEye.DataLayer;
using WatchfulEye.Web.Model;
using WatchfulEye.Web.Pages.Helpers;

namespace WatchfulEye.Web.Pages.Crud
{
    public partial class Questions : Page
    {
        protected static string RootVirtualPath
        {
            get
            {
                VirtualPathData rootVirtualPathData =
                   RouteTable.Routes.GetVirtualPath(null, "RootRoute", new RouteValueDictionary());
                return rootVirtualPathData == null ? string.Empty : rootVirtualPathData.VirtualPath;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public IQueryable<Question> GetQuestions()
        {
            return WatchfulEyeRepository.GetQuestions().AsQueryable();
        }

        public void UpdateQuestion(int? questionId)
        {
            Question questionToUpdate =
               WatchfulEyeRepository.GetQuestions().FirstOrDefault(question => question.QuestionId == questionId);
            if (questionToUpdate != null && TryUpdateModel(questionToUpdate))
            {
                var binLogoData = CacheHelper.GetCurrentProjectLogo(Cache);
                if (binLogoData != null)
                {
                    if (binLogoData.Length > 0)
                    {
                        questionToUpdate.ProjectLogo = binLogoData;
                    }

                    CacheHelper.RemoveProjectLogo(Cache);
                }

                WatchfulEyeRepository.Save(questionToUpdate);
            }
        }

        public void InsertQuestion(Question question)
        {
            if (!TryUpdateModel(question))
                return;

            var binaryLogoData = CacheHelper.GetCurrentProjectLogo(Cache);
            if (binaryLogoData != null)
            {
                if (binaryLogoData.Length > 0)
                {
                    question.ProjectLogo = binaryLogoData;
                }

                CacheHelper.RemoveProjectLogo(Cache);
            }

            WatchfulEyeRepository.Save(question);
        }

        public void DeleteQuestion(Question question)
        {
            WatchfulEyeRepository.Delete(question);
        }

        protected void InsertLogoFileUploader_OnUploadComplete(object sender, AjaxFileUploadEventArgs e)
        {
            UploadCompleteAction(e);
        }

        protected void UpdateLogoFileUploader_OnUploadComplete(object sender, AjaxFileUploadEventArgs e)
        {
            UploadCompleteAction(e);
        }

        private void UploadCompleteAction(AjaxFileUploadEventArgs e)
        {
            if (e.State == AjaxFileUploadState.Success)
            {
                CacheHelper.AddProjectLogo(Cache, e.GetContents());
                e.DeleteTemporaryData();
            }
        }
    }
}