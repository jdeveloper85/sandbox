﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using Pvs.Autopost.Config;

namespace Pvs.Autopost.Core
{
   /// <summary>
   ///    Класс-эксперт по обработке исключений
   /// </summary>
   public class TrackHandler
   {
      private readonly IEnumerable<EmailRecipient> _emailRecipients = SocialKeysReader.Factory.EmailRecipients;
      private readonly SendEmailWrapper _emailWrapper = new SendEmailWrapper(SocialKeysReader.Factory.EmailConfig);
      private readonly ILog _log;
      private readonly bool _notifyByEmail;

      public TrackHandler(ILog log, bool notifyByEmail)
      {
         _log = log;
         _notifyByEmail = notifyByEmail;
      }

      public void Handle(Exception anException)
      {
         _log.Error(anException.Message, anException);
         Notify(anException);
      }

      public void Handle(params string[] messages)
      {
         foreach (string message in messages)
            _log.Info(message);
      }

      private void Notify(Exception anException)
      {
         if (_notifyByEmail && _emailRecipients.Any())
         {
            try
            {
               var message = new EmailMessage
               {
                  MessageSubject = anException.Message,
                  MessageBody = anException.StackTrace
               };
               _emailWrapper.Send(message, _emailRecipients.ToArray());
            }
            catch (Exception e)
            {
               _log.Error(e.Message, e);
            }
         }
      }
   }
}