﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using PastebinPosting;

namespace PastebinImport
{
   internal static class Program
   {
      private static void Main(string[] args)
      {
         if (args == null || args.Length != 1)
         {
            Console.WriteLine("Please pass full qualified directory name for pastebin files");
            return;
         }

         string directoryName = args[0];
         if (Directory.Exists(directoryName))
         {
            IEnumerable<string> pastebinFiles = Directory.EnumerateFiles(directoryName, "*.txt");
            List<Tuple<string, string>> subjectBodyTuples = pastebinFiles.Select(ParsePasteBinFile).ToList();
            subjectBodyTuples.Shuffle();
            int insertCount = BatchInsert(subjectBodyTuples);
            Console.WriteLine("Insert count: {0}", insertCount);
         }
      }

      private static int BatchInsert(IEnumerable<Tuple<string, string>> subjectBodyTuples)
      {
         using (var entities = new SocialAutopostEntities())
         {
            foreach (Pastebin pastebin in subjectBodyTuples.Select(subjectBodyTuple => new Pastebin
            {
               Subject = subjectBodyTuple.Item1,
               Body = subjectBodyTuple.Item2
            }))
            {
               entities.Pastebin.Add(pastebin);
            }

            return entities.SaveChanges();
         }
      }

      private static Tuple<string, string> ParsePasteBinFile(string pastebinFile)
      {
         string[] lines = File.ReadAllLines(pastebinFile);
         string subject = lines[0];
         var bodyBuilder = new StringBuilder();
         for (int i = 1; i < lines.Length; i++)
         {
            bodyBuilder.Append(lines[i]).Append(Environment.NewLine);
         }

         string body = bodyBuilder.ToString();
         return Tuple.Create(subject, body);
      }
   }

   internal static class ListExtensions
   {
      public static void Shuffle<T>(this IList<T> list)
      {
         var random = new Random();
         int count = list.Count;
         while (count > 1)
         {
            count--;
            int k = random.Next(count + 1);
            T value = list[k];
            list[k] = list[count];
            list[count] = value;
         }
      }
   }
}