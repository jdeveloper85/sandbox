[Code]

function GetVersion(Param: String): String;
begin
  Result := '1.00';
end;

function GetInstallDir(S: String): String;
begin
  Result := ExpandConstant('{app}');
end;

function NextButtonClick(CurPageID: Integer): Boolean; 
var
  ErrorCode: Integer;
  Text, CMDArgs: String;
begin
  Result := True;
  if (CurPageID = wpWelcome)
    then
    begin      
      if (not ShellExec('', '7z','', '', SW_HIDE, ewWaitUntilTerminated, ErrorCode)) then
      begin
       MsgBox('Error accessing 7z.exe! PATH environmental variable must contain 7z.exe installation directory', mbInformation, mb_Ok);
       Result:= False;
      end
    else
      begin
        Result := True;
      end;
    end
end;

{---------------------------------------------------------------------------------}

[Setup]
OutputBaseFilename=CSharpTester_Setup
AppName=CSharpTester
AppVerName=CSharpTester {code:GetVersion}
AppPublisher=OOO "Program Verification Systems"
AppCopyright=Copyright (c) 2008-2016 OOO "Program Verification Systems"
VersionInfoVersion=1.0.0.0
AppPublisherURL=http://www.viva64.com
AppSupportURL=http://www.viva64.com
AppUpdatesURL=http://www.viva64.com
DefaultDirName=D:\CSharpAnalyzerTester
DefaultGroupName=CSharpTester
SolidCompression=false
DirExistsWarning=auto
UserInfoPage=false
InternalCompressLevel=none
Compression=lzma
ShowLanguageDialog=no
DisableDirPage=false
AlwaysShowDirOnReadyPage=true
AlwaysShowGroupOnReadyPage=true
MinVersion=0,5.0.2195sp4
ArchitecturesInstallIn64BitMode=x64
PrivilegesRequired=none
;SignTool=Standard

[Messages]
FinishedLabel=Setup has finished installing [name] on your computer. The application may be launched from Visual Studio.

[Types]
Name: "custom"; Description: "Custom installation"; Flags: iscustom
Name: "compact"; Description: "Compact installation"
Name: "full"; Description: "Full installation"

[Components]
Name: "Core"; Description: "Core application files"; Types: full custom compact; Flags: disablenouninstallwarning;
Name: "etalons"; Description: "Etalon Logs"; Types: full custom; Flags: disablenouninstallwarning; 
Name: "DailyTests"; Description: "Test archive for daily launches (src-for-CSharpTester.7z)"; Types: full; ExtraDiskSpaceRequired: 955252736; Flags: disablenouninstallwarning;

[Files]
Source: ".\CSharpAnalyzerTester\bin\Release\CSharpAnalyzerTester.exe.config"; DestDir: {code:GetInstallDir}; Flags: onlyifdoesntexist; Components: core
Source: ".\CSharpAnalyzerTester\bin\Release\CSharpAnalyzerTester.exe"; DestDir: {code:GetInstallDir}; Flags: replacesameversion; Components: core
Source: ".\..\..\PVS-Studio Settings\Settings.xml.CSAnalyzerUnitTests"; DestDir: {code:GetInstallDir}; Flags: replacesameversion; Components: core
Source: ".\gen_etalons.bat"; DestDir: {code:GetInstallDir}; Flags: replacesameversion; Components: core
Source: ".\test_all.bat"; DestDir: {code:GetInstallDir}; Flags: replacesameversion; Components: core
Source: "{src}\SelfTester_setup\src-for-CSharpTester.7z"; DestDir: {code:GetInstallDir}; Flags: external replacesameversion; Components: DailyTests
Source: "{src}\SelfTester_setup\EtalonLogsCSharp\*"; DestDir: "{code:GetInstallDir}\Logs\EtalonLogs\"; Flags: external replacesameversion; Components: etalons

[Icons]
Name: {group}\{cm:UninstallProgram,CSharpTester}; Filename: {uninstallexe}; Flags: createonlyiffileexists; Components: core
Name: {group}\CSharpTester; Filename: {code:GetInstallDir}\CSharpAnalyzerTester.exe; WorkingDir: "{code:GetInstallDir}"; Components: core

[Run]
Filename: "{sys}\cmd.exe"; Parameters: "/c 7z x src-for-CSharpTester.7z -y"; WorkingDir:{code:GetInstallDir}; Flags: runhidden; StatusMsg: "Unpacking sources from src-for-CSharpTester.7z"; Components: DailyTests;
Filename: "{sys}\cmd.exe"; Parameters: "/c del /f /q src-for-CSharpTester.7z"; WorkingDir:{code:GetInstallDir}; Flags: runhidden; Components: DailyTests;

[UninstallDelete]
Type: filesandordirs; Name: "{code:GetInstallDir}\Src"
Type: dirifempty; Name: "{code:GetInstallDir}"
Type: files; Name: "{code:GetInstallDir}\Settings.xml.CSAnalyzerUnitTests"
Type: files; Name: "{code:GetInstallDir}\DevenvRuns.dat"
