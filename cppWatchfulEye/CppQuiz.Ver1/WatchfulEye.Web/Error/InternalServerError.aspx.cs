﻿using System;
using System.Web.UI;
using WatchfulEye.Web.Pages.Helpers;
#if DEBUG
using System.Text;
#endif

namespace WatchfulEye.Web.Error
{
    public partial class InternalServerError : Page
    {
#if DEBUG
private const int DefaultStringBuilderCapacity = 0x400;
#endif


        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            ErrorLabel.Text = "<h1>На сервере произошла ошибка</h1>";

#if DEBUG
            var lastServerEx = CacheHelper.GetLastServerError(Cache);
            if (lastServerEx != null)
            {
                var errorStringBuilder = new StringBuilder(DefaultStringBuilderCapacity);
                errorStringBuilder.Append("<p><h3>Краткое сообщение об ошибке:</h3>")
                    .AppendFormat("{0}</p>", lastServerEx.Message)
                    .Append("<p><h3>Трассировка стека:</h3>")
                    .Append(lastServerEx.StackTrace)
                    .Append("</p>");
                var innerLastServerEx = lastServerEx.InnerException;
                if (innerLastServerEx != null)
                {
                    errorStringBuilder.Append("<h3>Внутреннее исключение:</h3>")
                        .Append("Краткое сообщение о внутреннем исключении: ")
                        .AppendFormat("{0}<br/>", innerLastServerEx.Message)
                        .Append("<h3>Трассировка стека внутреннего исключения:</h3>")
                        .Append(innerLastServerEx.StackTrace);
                }

                ErrorLabel.Text = errorStringBuilder.ToString();
            }
#endif

            CacheHelper.RemoveLastServerError(Cache);
        }
    }
}