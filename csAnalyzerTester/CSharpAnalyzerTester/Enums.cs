﻿//  2006-2008 (c) Viva64.com Team
//  2008-2016 (c) OOO "Program Verification Systems"
using System;
using System.Linq;

namespace CSharpAnalyzerTester
{
    public enum TesterExitCode
    {
        Success = 0,
        IncorrectArgs = 1,
        BadSettings = 2,
        Differences = 3,
        Error = 4
    }

    public enum AnalysisStatus
    {
        [StatusHighlight(ConsoleColor.White)]
        None,

        [StatusHighlight(ConsoleColor.Green)]
        Ok,

        [StatusHighlight(ConsoleColor.Red)]
        Diff,

        [StatusHighlight(ConsoleColor.DarkYellow)]
        Fail,

        [StatusHighlight(ConsoleColor.DarkBlue)]
        Etalon,

        [StatusHighlight(ConsoleColor.Red)]
        Timeout
    }

    [Flags]
    public enum ErrorModes
    {
        SystemDefault = 0x0,
        SemFailCriticalErrors = 0x0001,
        SemNoAlignmentFaultExcept = 0x0004,
        SemNoGpFaultErrorBox = 0x0002,
        SemNoOpenFileErrorBox = 0x8000
    }

    public static class AnalysisStatusExts
    {
        private const ConsoleColor Default = ConsoleColor.White;

        public static ConsoleColor GetHighlight(this AnalysisStatus status)
        {
            var statusHighlightAttribute =
                status.GetType()
                    .GetField(status.ToString())
                    .GetCustomAttributes(typeof (StatusHighlightAttribute), false)
                    .Cast<StatusHighlightAttribute>()
                    .FirstOrDefault();
            return statusHighlightAttribute != null ? statusHighlightAttribute.Color : Default;            
        }
    }

    [AttributeUsage(AttributeTargets.Field)]
    public sealed class StatusHighlightAttribute : Attribute
    {
        public ConsoleColor Color { get; private set; }

        public StatusHighlightAttribute(ConsoleColor color)
        {
            Color = color;
        }
    }
}