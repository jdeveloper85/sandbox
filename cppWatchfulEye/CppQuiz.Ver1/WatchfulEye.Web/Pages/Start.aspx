﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Main.Master" AutoEventWireup="true"
CodeBehind="Start.aspx.cs" Inherits="WatchfulEye.Web.Pages.Start" %>

<%@ OutputCache Location="None" %>
<%@ Import Namespace="Pvs.Quiz.Common" %>
<%@ Import Namespace="System.Web.Routing" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div class="text-content">
        <div class="center">
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: left;">
                        <img src="../Images/dummy.png" alt=""/>
                    </td>
                    <td style="vertical-align: top;">
                        <h2 style="margin-top: 0; text-align: center;">C++ Quiz: are you a code guru?</h2>
                    </td>
                    <td style="text-align: right;">
                        <img src="../Images/level5.png" alt=""/>
                    </td>
                </tr>
            </table>
        </div>
        <div class="center">
            Do you think that it is impossible to make silly mistakes in your own code? Developers of
            <a class="standart" target="blank" href="http://www.viva64.com/en/pvs-studio/">PVS-Studio</a> C/C++ static analyzer are inclined to disagree!
        </div>
        <div class="center">
            Code analyzers work non-stop and can find many bugs which are otherwise hard to notice. Here we've gathered
            many fragments from well-known open source projects in which static analyzer had found real bugs.
        </div>
        <div class="center">
            We offer you to compete against
            <a class="standart" target="blank" href="http://www.viva64.com/en/pvs-studio/">PVS-Studio</a>
            static analyzer by locating bugs in <%= WebConfigReader.TestQuestionsNumber %> source code fragments
            randomly selected from our database of open source projects.
            You get a point for a correct answer if it was given within <%= WebConfigReader.QuestionTime %> seconds.
        </div>
        <div style="text-align: center; width: 100%;">
            <%
                var virtualPathData =
                    RouteTable.Routes.GetVirtualPath(null, "QuestionRoute", new RouteValueDictionary {{"number", 1}});
                if (virtualPathData != null)
                {
                    var path = virtualPathData.VirtualPath;
                    Response.Write(string.Format("<a href='{0}' class='actionButtons'>Begin Test</a>", path));
                }
            %>
        </div>
    </div>
</asp:Content>
<asp:Content ID="ResultContent" ContentPlaceHolderID="ResultContentPlaceHolder" runat="server">
</asp:Content>