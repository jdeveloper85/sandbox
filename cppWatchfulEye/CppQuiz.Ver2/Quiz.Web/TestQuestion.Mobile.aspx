﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Mobile.Master" AutoEventWireup="true" CodeBehind="TestQuestion.Mobile.aspx.cs" Inherits="Quiz.Web.TestQuestion_Mobile" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="Quiz.Web" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">        
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div class="ui-grid-a">
        <div id="currentQuestionNumber" class="ui-block-a"></div>
        <div id="countdown" class="ui-block-b"></div>
    </div>
    <div>
        <div id="currentScore"></div>
    </div>    
    <div data-role="collapsible" data-collapsed="false" data-content-theme="b">
        <h1>Code</h1>
        <div id="projectName"></div>
        <div id="codeFragment"></div>        
    </div>    
    <div data-role="collapsible">
        <h5>Project logo</h5>
        <div id="projectLogo"></div>
    </div>
    <div>
        <div id="resultText"></div>
    </div>
    <div data-role="controlgroup" data-theme="b">
        <button id="confirmAnswerBtn" type="button" data-role="button"></button>
        <button id="actionBtn" type="button" data-role="button"></button>
        <button id="noerrorsBtn" type="button" data-role="button"></button>
    </div>                    
    <div data-role="collapsible" id="warn">
        <h1>Warning</h1>    
        <div id="pvsStudioWarning"></div>
    </div>
    <div data-role="collapsible" id="exp">
        <h1>Explanation</h1>
        <div id="explanatoryText"></div>
    </div>
    <%: Scripts.Render(BundleConfig.TestQuestionMobile) %>
    <%: Styles.Render(BundleConfig.ProcessAnswer) %>      
    <div class="modal"></div>
</asp:Content>
