﻿//  2006-2008 (c) Viva64.com Team
//  2008-2016 (c) OOO "Program Verification Systems"
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using CSharpAnalyzerTester.Env;
using CSharpAnalyzerTester.Helpers;
using ProgramVerificationSystems.PVSStudio;
using Res = CSharpAnalyzerTester.Properties.Resources;

namespace CSharpAnalyzerTester
{
    internal static class Program
    {
        private const string OutputTxtSuffixLabel = "_Output.txt";
        private const string UnhandledErrorLabel = "Unhandled Error";

        private static readonly IEnv TestEnv = SingletonInjector<EnvImpl>.Instance.Default;
        private static readonly IConf TestConf = SingletonInjector<ConfImpl>.Instance;
        private static readonly TextWriter Out = Console.Out;
        private static readonly TextWriter Err = Console.Error;

        static Program()
        {
            AppDomain.CurrentDomain.UnhandledException +=
                (sender, args) =>
                    MessageBox.Show(args.ExceptionObject.ToString(), UnhandledErrorLabel, MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
        }

        private static int Main(string[] args)
        {            
            using (new ErrorModeManager(ErrorModes.SemNoGpFaultErrorBox))
            {
                return Launch(args);
            }
        }

        private static int Launch(string[] args)
        {
            Func<string, bool> existsCond = aFile => !string.IsNullOrEmpty(aFile) && File.Exists(aFile);
            var existsCustomImage = existsCond(TestConf.AnalyzerImagePath);
            var existsConfImage = existsCond(TestEnv.AnalyzerImagePath);
            if (!existsCustomImage && !existsConfImage)
            {
                Err.WriteLine("Analyzer image path not found: {0}", TestEnv.AnalyzerImagePath);
                return (int)TesterExitCode.Error;
            }

            if (existsCustomImage)
                TestEnv.AnalyzerImagePath = TestConf.AnalyzerImagePath;

            #region Внутренний режим тестера. Предназначен для управления настройками плагина при открытии лога

            if (args.Length == 3 &&
                args[0].TrimStart('-').Equals("run", StringComparison.InvariantCultureIgnoreCase))
            {
                return DiffМanager.OpenDiffInDevenv(args);
            }

            #endregion

            #region Режим запуска лога с изменениями в Visual Studio

            if (args.Length == 2 &&
                args[0].TrimStart('-').Equals("diff", StringComparison.InvariantCultureIgnoreCase))
            {
                DiffМanager.InitDevenv(args);
                return (int)TesterExitCode.Differences;
            }

            #endregion

            #region Режим генерации эталонных логов

            var genEtalon = args.Length > 0 &&
                            args[0].TrimStart('-').Equals("etalon", StringComparison.InvariantCultureIgnoreCase);

            #endregion

            bool success;
            string errorMessage;
            var projectList = LoadSettingsHelper.LoadSettings(TestEnv.SettingsPath, out success, out errorMessage);
            if (!success)
            {
                Out.WriteLine(errorMessage);
                return (int)TesterExitCode.BadSettings;
            }

            var applicationSettings = OutputHelper.LoadAnalyzerSettings(TestEnv.AnalyzerSettingsPath, Out) ??
                                      new ApplicationSettings { SourceTreeRoot = Environment.CurrentDirectory };

            OutputHelper.SaveAnalyzerSettings(TestEnv.AnalyzerSettingsPath, applicationSettings, Out);

            #region Режим запуска одного проекта

            if (args.Length == 3
                && args[0].TrimStart('-').Equals("runone", StringComparison.InvariantCultureIgnoreCase))
            {
                var solutionToRun = args[1];
                var sessionTargetFolder = args[2];

                if (!Directory.Exists(sessionTargetFolder))
                {
                    Out.WriteLine(Res.SessionFolderNotExists, sessionTargetFolder);
                    return (int)TesterExitCode.IncorrectArgs;
                }

                var projectToRun = projectList.FirstOrDefault(project => project.SolutionPath == solutionToRun);
                if (projectToRun == null)
                {
                    Out.WriteLine(Res.ProjectNotExistsInSettings, solutionToRun);
                    return (int)TesterExitCode.IncorrectArgs;
                }

                AutoStopwatch autoStopwatch;
                AnalysisStatus status;
                using (autoStopwatch = new AutoStopwatch())
                {
                    status = RecheckProject(projectToRun, sessionTargetFolder);
                }

                var elapsedMinutes = autoStopwatch.Elapsed.Minutes;
                var elapsedSeconds = autoStopwatch.Elapsed.Seconds;
                var elapsedStr = string.Format("{0,2}m {1,2}s", elapsedMinutes, elapsedSeconds);
                Out.WriteLine(Res.StatisticsTableFormatString, Path.GetFileName(projectToRun.SolutionPath),
                    elapsedStr, status);

                return (int)status;
            }

            #endregion            

            var openingTime = Stopwatch.StartNew();
            var summaries = CheckProjects(projectList.ToArray(), genEtalon, TestConf.ConcurrencyLevel, TestEnv, openingTime);            
            var okCnt = summaries.Count(summary => summary.Status == AnalysisStatus.Ok);
            var diffCnt = summaries.Count(summary => summary.Status == AnalysisStatus.Diff);
            var failCnt = summaries.Count(summary => summary.Status == AnalysisStatus.Fail);
            var haveErrors = failCnt > 0;
            var haveDiffs = diffCnt > 0;

            Out.WriteLine("{0}Oks: {1}. Diffs: {2}. Fails: {3}.", Environment.NewLine, okCnt, diffCnt, failCnt);
            //var htmlFileName = Path.Combine(TestEnv.LogsDirectory, "Summary.html");
            //OutputHelper.PrintHtml(htmlFileName, summaries, openingTime.Elapsed);
            Out.WriteLine(Res.OutputAnalysisTime, openingTime.Elapsed.Hours,
                openingTime.Elapsed.Minutes, openingTime.Elapsed.Seconds);
            Out.WriteLine();
            Out.WriteLine(Res.OutputLogDirectorty, TestEnv.LogDirFormattableName);

            // Если папка с diff'ами пуста, то удаляем её
            var diffs = Directory.GetFiles(TestEnv.LogsDiffDirectory, "*.*");
            if (diffs.Length == 0)
                Directory.Delete(TestEnv.LogsDiffDirectory, true);

            if (haveErrors)
                return (int)TesterExitCode.Error;

            if (haveDiffs)
                return (int)TesterExitCode.Differences;

            return (int)TesterExitCode.Success;
        }

        private static AnalysisStatus RecheckProject(TestProject aTestProject, string targetSessionFolder)
        {
            AnalysisStatus status;
            var solutionName = aTestProject.GetSolutionName();
            var etalonLog = solutionName.GetEtalonLog(TestEnv);
            var logFilename = solutionName.GetLogFileName(targetSessionFolder);
            var logFileNameDiffs = solutionName.GetLogFileNameDiffs(targetSessionFolder);
            var runResult = aTestProject.Run(TestEnv, logFilename);
            if (!runResult.Success || (runResult.ExitCode != 0 && runResult.ExitCode != 7) || !File.Exists(logFilename))
            {
                status = AnalysisStatus.Fail;
            }
            else
            {
                var pvsDiff = new PVSDiff();
                var diffSet = pvsDiff.GetDiff(etalonLog, logFilename);
                status = pvsDiff.Output.Succeeded ? AnalysisStatus.Ok : AnalysisStatus.Diff;
                if (status == AnalysisStatus.Diff)
                {
                    diffSet.WriteXml(logFileNameDiffs, XmlWriteMode.WriteSchema);
                }
            }

            return status;
        }

        private static IList<TestProjectSummary> CheckProjects(IReadOnlyList<TestProject> projectList, bool genEtalon,
            int concurrencyLevel, IEnv testEnv, Stopwatch openingTime)
        {
            using (
                IExecutable<IList<TestProjectSummary>> analysisManager =
                    new AnalysisManager(projectList, genEtalon, testEnv, concurrencyLevel))
            using (
                TextWriter htmlWriter =
                    new StreamWriter(new FileStream(Path.Combine(TestEnv.LogsDirectory, "Summary.html"), FileMode.Create,
                        FileAccess.Write, FileShare.Read)))
            {
                htmlWriter.Write(Res.HtmlHead);
                var collapsePanelId = 0;                

                analysisManager.Executed += (sender, args) =>
                {
                    using (new ConsoleHighlightWrapper(args.Status))
                    {
                        Out.WriteLine(Res.StatisticsTableFormatString, args.SolutionName, args.AnalysisTime, args.Status);
                    }
                    Interlocked.Increment(ref collapsePanelId);                    
                };
                analysisManager.Error += (sender, args) => Err.WriteLine(args.Error.ToString());
                analysisManager.Disposed += (sender, args) =>
                {
                    var projectSummary = args.ProjectSummary;
                    var solutionName = projectSummary.TestProject.GetSolutionName();
                    var noThrowMessage = string.Format(
                        "{0}{1}{2}", TestEnv.LogsDirectory, solutionName, OutputTxtSuffixLabel);
                    OutputHelper.WriteLogNoThrow(
                        noThrowMessage, args.Result.OutputData, args.Result.ErrorData, Out);

                    var accumulator = new StringBuilder(0x4000);
                    accumulator.AppendLine("<div class='dtable panel-group'><div class='panel panel-default'>");
                    {
                        var isOk = projectSummary.Status == AnalysisStatus.Ok;
                        accumulator.AppendFormat(
                            "<div class='panel-heading'><h4 class='panel-title' style='color: {0};'>",
                            isOk ? "black" : "red");
                        {
                            accumulator.AppendFormat(
                                "<a data-toggle='collapse' href='#{0}'>Checked solution: {1} - At {2} (<b>Elapsed: {3}</b>)</a>",
                                collapsePanelId, projectSummary.SolutionName, DateTime.Now.ToString("D"),
                                projectSummary.Elapsed.ToAbsoluteFormat());
                        }
                        accumulator.AppendLine("</h4></div>");

                        accumulator.AppendFormat(
                            "<div id='{0}' class='panel-collapse collapse'><div class='panel-body'>", collapsePanelId);
                        {
                            accumulator
                                .AppendLine(BuildRow("Solution path", projectSummary.TestProject.SolutionPath))
                                .AppendLine(BuildRow("Saved log path", projectSummary.SavedLogPath))
                                .AppendLine(BuildRow("Etalon log path", projectSummary.EtalonLogPath))
                                .AppendLine(BuildRow("Start time", projectSummary.StartTime.ToString("T")))
                                .AppendLine(BuildRow("End time", projectSummary.EndTime.ToString("T")))
                                .AppendLine(BuildRow("Total Elapsed", projectSummary.Elapsed.ToAbsoluteFormat()))
                                .AppendLine(BuildRow("Status", projectSummary.Status.ToString()));
                            accumulator
                                .AppendLine(BuildMessages(projectSummary.AdditionalMessages, "green"))
                                .AppendLine(BuildMessages(projectSummary.MissingMessages, "red"))
                                .AppendLine(BuildMessages(projectSummary.ModifiedMessages, "blue"));
                        }
                        accumulator.AppendLine("</div></div>");
                    }
                    accumulator.AppendLine("</div></div>");

                    lock (htmlWriter)
                    {
                        htmlWriter.Write(accumulator.ToString());
                        htmlWriter.Flush();
                    }
                };

                var summaries = analysisManager.Execute();
                var seed = summaries.Aggregate(TimeSpan.Zero, (current, summary) => current + summary.Elapsed);
                htmlWriter.Write("<hr />");
                htmlWriter.Write("<h2>Collected by projects time spent: {0}</h2>", seed.ToAbsoluteFormat());
                htmlWriter.Write("<h2>The total time spent: {0}</h2>", openingTime.Elapsed.ToAbsoluteFormat());
                htmlWriter.Write(Res.HtmlFoot);

                return summaries ?? Enumerable.Empty<TestProjectSummary>().ToList();
            }
        }

        private static string BuildRow(string cellTitle, string cellValue)
        {
            var contentBuilder = new StringBuilder(0x100);
            contentBuilder.Append("<div class='drow'>");
            contentBuilder.AppendFormat("<div class='dcell'>{0}:</div>", cellTitle);
            contentBuilder.AppendFormat("<div class='dcell'>{0}</div>", cellValue);
            contentBuilder.Append("</div>");

            return contentBuilder.ToString();
        }

        private static string BuildMessages(IEnumerable<string> messages, string color)
        {
            var contentBuilder = new StringBuilder(0x1000);
            if (messages == null)
                return string.Empty;

            var messageArray = messages.ToArray();
            if (messageArray.Length <= 0)
                return string.Empty;

            contentBuilder.AppendFormat("<div style='color: {0}; font-size: smaller;'>", color);
            contentBuilder.Append(
                messageArray.Aggregate(string.Empty,
                    (current, message) => current + string.Format("{0}<br />", SecurityElement.Escape(message))));
            contentBuilder.Append("</div>");

            return contentBuilder.ToString();
        }
    }
}