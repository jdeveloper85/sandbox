﻿using System.Collections.Generic;

namespace CppQuiz.Data
{
   public partial class QuizQuestion
   {
      public static QuizQuestion Empty
      {
         get
         {
            return new QuizQuestion
            {
               QuestionId = 0,
               CodeFragment = string.Empty,
               ProjectName = string.Empty,
               ProjectLogo = new byte[0],
               ExplanatoryText = string.Empty,
               PvsStudioWarning = string.Empty,
               QuizQuestionStats = new List<QuizQuestionStat>(0)
            };
         }
      }
   }
}