﻿using System.ServiceProcess;

namespace Pvs.Autoposting.ConsumerService
{
   internal static class Program
   {      
      private static void Main()
      {
         var servicesToRun = new ServiceBase[] { new AutopostConsumerService() };
         ServiceBase.Run(servicesToRun);
      }
   }
}