﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pvs.Quiz.Common
{
   /// <summary>
   ///     Класс для сохранение состояния ответов на вопросы
   /// </summary>
   public class AnswersKeeper
   {
      public static readonly int QuestionTime = WebConfigReader.QuestionTime;
      private readonly Dictionary<int, AnswerState> _answerStates = new Dictionary<int, AnswerState>();

      private readonly Dictionary<int, Tuple<DateTime, TimeSpan>> _timeStates =
          new Dictionary<int, Tuple<DateTime, TimeSpan>>();

      public int TotalRight
      {
         get { return _answerStates.Count(answerState => answerState.Value == AnswerState.Right); }
      }

      public int Count
      {
         get { return _answerStates.Count; }
      }

      /// <summary>
      ///     Установка состояния ответа на вопрос
      /// </summary>
      /// <param name="questionNumber">Номер вопроса</param>
      /// <param name="state">Состояние вопроса</param>
      /// <returns>true, если пользователя отвечает первый раз, false, если пытается ответить повторно</returns>
      public void SetQuestionState(int questionNumber, AnswerState state)
      {
         AnswerState answerState;
         if (_answerStates.TryGetValue(questionNumber, out answerState))
         {
            return;
         }

         _answerStates.Add(questionNumber, state);
      }

      public AnswerState GetQuestionState(int questionNumber)
      {
         AnswerState state;
         return _answerStates.TryGetValue(questionNumber, out state) ? state : default(AnswerState);
      }

      /// <summary>
      ///     Установка времени ответа на вопрос
      /// </summary>
      /// <param name="questionNumber">Номер вопроса</param>
      /// <param name="isFirstTime">Устанавливает true, если время еще не было установлено, false - в противном случае</param>
      /// <returns>Время, прошедшее с момента перехода на вопрос</returns>
      public TimeSpan SetTimeState(int questionNumber, out bool isFirstTime)
      {
         Tuple<DateTime, TimeSpan> timeSpent;
         TimeSpan spent;

         if (_timeStates.TryGetValue(questionNumber, out timeSpent))
         {
            var currentTimeState = _timeStates[questionNumber];
            var startTime = currentTimeState.Item1;
            spent = DateTime.Now.Subtract(startTime);
            _timeStates[questionNumber] = Tuple.Create(startTime, spent);
            isFirstTime = false;
         }
         else
         {
            var startTime = DateTime.Now;
            spent = DateTime.Now.Subtract(startTime);
            _timeStates.Add(questionNumber, Tuple.Create(startTime, spent));
            isFirstTime = true;
         }

         return spent;
      }

      public void Clear()
      {
         _answerStates.Clear();
         _timeStates.Clear();
      }
   }
}