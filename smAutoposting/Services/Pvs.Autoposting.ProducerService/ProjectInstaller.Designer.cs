﻿namespace Pvs.Autoposting.ProducerService
{
   partial class ProjectInstaller
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary> 
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Component Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.AutopostServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
         this.AutopostServiceInstaller = new System.ServiceProcess.ServiceInstaller();
         // 
         // AutopostServiceProcessInstaller
         // 
         this.AutopostServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
         this.AutopostServiceProcessInstaller.Password = null;
         this.AutopostServiceProcessInstaller.Username = null;
         // 
         // AutopostServiceInstaller
         // 
         this.AutopostServiceInstaller.Description = "Service for pushing messages to autopost";
         this.AutopostServiceInstaller.DisplayName = "Autopost Producer";
         this.AutopostServiceInstaller.ServiceName = "AutoPostProducerService";
         // 
         // ProjectInstaller
         // 
         this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.AutopostServiceProcessInstaller,
            this.AutopostServiceInstaller});

      }

      #endregion

      private System.ServiceProcess.ServiceProcessInstaller AutopostServiceProcessInstaller;
      private System.ServiceProcess.ServiceInstaller AutopostServiceInstaller;
   }
}