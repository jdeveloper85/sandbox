﻿using System;
using System.Web.Caching;
using CppQuiz.Data;

namespace Quiz.Web.Utils.Web
{
   public static class AppCacheHelper
   {
      #region Кэширование вопросов

      public static void AddQuestions(Cache cache, QuizQuestion[] quizQuestions)
      {
         InternalCacheHelper.Add(cache, AppCacheKey.FreshQuestions, quizQuestions);
      }

      public static QuizQuestion[] GetQuestions(Cache cache)
      {
         return InternalCacheHelper.Get<QuizQuestion[]>(cache, AppCacheKey.FreshQuestions);
      }

      public static void RemoveQuestions(Cache cache)
      {
         InternalCacheHelper.Remove(cache, AppCacheKey.FreshQuestions);
      }

      #endregion

      #region Вспомогательные типы

      private static class InternalCacheHelper
      {
         internal static void Add<T>(Cache cache, AppCacheKey cacheKey, T data)
         {
            var cacheKeyName = Enum.GetName(typeof(AppCacheKey), cacheKey);
            if (cacheKeyName != null)
            {
               cache.Insert(cacheKeyName, data);
            }
         }

         internal static T Get<T>(Cache cache, AppCacheKey cacheKey)
         {
            var cacheKeyName = Enum.GetName(typeof(AppCacheKey), cacheKey);
            if (cacheKeyName == null)
            {
               return default(T);
            }

            var cacheValue = cache[cacheKeyName];
            return cacheValue is T ? (T)cacheValue : default(T);
         }

         internal static void Remove(Cache cache, AppCacheKey cacheKey)
         {
            var cacheKeyName = Enum.GetName(typeof(AppCacheKey), cacheKey);
            if (cacheKeyName != null)
            {
               cache.Remove(cacheKeyName);
            }
         }
      }

      private enum AppCacheKey
      {
         FreshQuestions
      }

      #endregion
   }
}