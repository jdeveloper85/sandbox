﻿using TweetSharp;

namespace Pvs.Autopost.Core
{
   /// <summary>
   ///    Класс для публикации сообщений в Twitter'е
   /// </summary>
   public class TwitterPublishMessage : IPublishMessage
   {
      private readonly string _accessToken;
      private readonly string _accessTokenSecret;
      private readonly string _consumerKey;
      private readonly string _consumerSecret;

      public TwitterPublishMessage(string consumerKey, string consumerSecret, string accessToken,
         string accessTokenSecret)
      {
         _consumerKey = consumerKey;
         _consumerSecret = consumerSecret;
         _accessToken = accessToken;
         _accessTokenSecret = accessTokenSecret;
      }

      public bool Publish(string message)
      {
         var twitterService = new TwitterService(_consumerKey, _consumerSecret);
         twitterService.AuthenticateWith(_accessToken, _accessTokenSecret);
         var tweetOptions = new SendTweetOptions
         {
            Status = message
         };

         TwitterStatus twitterStatus = twitterService.SendTweet(tweetOptions);
         return twitterStatus != null;
      }
   }
}