﻿using System.Globalization;
using System.Web;
using System.Web.SessionState;
using Pvs.Quiz.Common;
using WatchfulEye.Web.Pages.Helpers;

namespace WatchfulEye.Web
{
    /// <summary>
    ///     HttpHandler для получения оставшегося времени на вопрос
    /// </summary>
    public class GetTimeSpentHandler : IHttpHandler, IRequiresSessionState
    {
        private const string QuestionIdQueryStringName = "QuestionId";

        public void ProcessRequest(HttpContext context)
        {
            var questionIdStr = context.Request.QueryString[QuestionIdQueryStringName];
            int questionId;

            const string timeIsUpMessage = "Time is up";
            if (int.TryParse(questionIdStr, out questionId) && context.Session != null)
            {
                bool isFirstTime;
                var answersKeeper = SessionHelper.GetAnswersKeeper(context.Session);
                var spent = answersKeeper.SetTimeState(questionId, out isFirstTime);
                if (isFirstTime)
                {
                    context.Response.Write(AnswersKeeper.QuestionTime.ToString(CultureInfo.InvariantCulture));
                    return;
                }

                var questionState = answersKeeper.GetQuestionState(questionId);
                var output = "Unknown question state";
                switch (questionState)
                {
                    case AnswerState.DontKnow:
                    case AnswerState.Right:
                    case AnswerState.Wrong:
                        output = "You've already answered this question";
                        break;
                    case AnswerState.TimeIsUp:
                        output = timeIsUpMessage;
                        break;
                }

                if (questionState != default(AnswerState))
                {
                    context.Response.Write(output);
                    return;
                }

                var deltaSpent = AnswersKeeper.QuestionTime - spent.Seconds;
                output = deltaSpent <= 0 || deltaSpent >= AnswersKeeper.QuestionTime
                    ? timeIsUpMessage
                    : deltaSpent.ToString(CultureInfo.InvariantCulture);
                context.Response.Write(output);
            }
            else
            {
                context.Response.Write(timeIsUpMessage);
            }
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}