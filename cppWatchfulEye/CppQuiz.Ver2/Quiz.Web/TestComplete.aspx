﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TestComplete.aspx.cs"
    Inherits="Quiz.Web.TestComplete"
    ViewStateMode="Inherit" %>

<%@OutputCache Duration="300" VaryByParam="none" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <h1>Test complete</h1>    
    <asp:Label runat="server" ID="TestResultLabel"/>
</asp:Content>
