﻿using System;

namespace Pvs.Pastebin.CodeCollectorServer
{
   public struct PastebinEntity
   {
      public string Lang { get; set; }

      public string PublishLink { get; set; }

      public string LinkValue { get; set; }

      public DateTime PublishDate { get; set; }

      public override string ToString()
      {
         return string.Format("Lang: {0}, PublishLink: {1}, LinkValue: {2}, PublishDate: {3}", Lang, PublishLink,
            LinkValue, PublishDate);
      }
   }
}