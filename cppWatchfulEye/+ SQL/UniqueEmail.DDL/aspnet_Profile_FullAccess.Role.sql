USE [1gb_x_email20b]
GO
/****** Object:  DatabaseRole [aspnet_Profile_FullAccess]    Script Date: 19.02.2015 15:21:39 ******/
CREATE ROLE [aspnet_Profile_FullAccess]
GO
ALTER ROLE [aspnet_Profile_ReportingAccess] ADD MEMBER [aspnet_Profile_FullAccess]
GO
ALTER ROLE [aspnet_Profile_BasicAccess] ADD MEMBER [aspnet_Profile_FullAccess]
GO
