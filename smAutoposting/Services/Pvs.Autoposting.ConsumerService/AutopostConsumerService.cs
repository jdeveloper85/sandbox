﻿using System.ServiceProcess;
using Pvs.Autopost.Core;
using Pvs.Autoposting.ConsumerServer;

namespace Pvs.Autoposting.ConsumerService
{
   public partial class AutopostConsumerService : ServiceBase
   {
      private AutopostConsumer _autopostConsumer;

      public AutopostConsumerService()
      {
         InitializeComponent();
      }

      protected override void OnStart(string[] args)
      {
         _autopostConsumer = new AutopostConsumer(AutopostLoggerWrapper.GetInstance(Loggers.Autoconsumer));
         _autopostConsumer.Start();
      }

      protected override void OnStop()
      {
         _autopostConsumer.Stop();
      }

      protected override void OnPause()
      {
         _autopostConsumer.Suspend();
      }

      protected override void OnContinue()
      {
         _autopostConsumer.Resume();
      }
   }
}