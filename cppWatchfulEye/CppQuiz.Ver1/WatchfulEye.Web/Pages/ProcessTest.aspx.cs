﻿using System;
using System.Web.UI;
using Pvs.Quiz.Common;
using WatchfulEye.Web.Pages.Helpers;

namespace WatchfulEye.Web.Pages
{
    public partial class ProcessTest : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int question = 0;
            if (!IsPostBack)
            {
                object questionNumber = Page.RouteData.Values["number"] ?? Request.QueryString["number"];
                if (questionNumber != null && !string.IsNullOrEmpty(questionNumber.ToString()) &&
                    int.TryParse(questionNumber.ToString(), out question))
                {
                    UserQuestion.QuestionNumber = question;
                }

                var urlReferrer = Request.UrlReferrer;
                if (urlReferrer != null)
                {
                    var segments = urlReferrer.Segments;
                    int questionNumberSegment;
                    if (segments.Length == 3 && int.TryParse(segments[2], out questionNumberSegment) &&
                        questionNumberSegment != question)
                    {
                        var answersKeeper = SessionHelper.GetAnswersKeeper(Session);
                        if ((answersKeeper.Count + 1) < question)   // NOTE: Пользователь решил "перепрыгнуть" через вопрос
                        {
                            Response.RedirectToRoute("StartRoute");
                            return;
                        }
                    }
                }
            }

            ProcessAnswer(question);
        }

        private void ProcessAnswer(int aQuestionNumber)
        {
            string answer = Request.Params["answerStatus"];
            AnswerState answerState;
            if (!string.IsNullOrEmpty(answer) && Enum.TryParse(answer, out answerState))
            {
                var answersKeeper = SessionHelper.GetAnswersKeeper(Session);
                answersKeeper.SetQuestionState(aQuestionNumber, answerState);
            }
        }
    }
}