﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using Pvs.Autopost.Config;
using Pvs.Autopost.Core;
using Pvs.Autoposting.Entities;

namespace Pvs.Autoposting.ConsumerServer
{
   /// <summary>
   ///    Класс, потребляющий сообщения для размещения из Msmq
   /// </summary>
   public class AutopostConsumer : IPostService
   {
      private readonly int _endPostingHour = SocialKeysReader.EndPostingHour;
      private readonly int _maxSeconds = SocialKeysReader.MaxTimeslotSeconds;
      private readonly int _minSeconds = SocialKeysReader.MinTimeslotSeconds;
      private readonly int _poolingInterval;
      private readonly IAutopostRepository _socialAutopostRepository;
      private readonly int _startPostingHour = SocialKeysReader.StartPostingHour;
      private readonly TrackHandler _trackHandler;
      private readonly TwitterConfiguration _twitterConfig;
      private CancellationToken _cancellationToken;
      private TimePublicationGenerator _timelineGenerator;
      private CancellationTokenSource _tokenSource;

      public AutopostConsumer(ILog autopostLogger)
      {
         _socialAutopostRepository = new SocialAutopostRepository(autopostLogger);
         _twitterConfig = SocialKeysReader.Factory.TwitterConfig;
         _trackHandler = new TrackHandler(autopostLogger, true);
         _poolingInterval = SocialKeysReader.PollingInterval;
      }

      public void Start()
      {
         _tokenSource = new CancellationTokenSource();
         _cancellationToken = _tokenSource.Token;
         _timelineGenerator = new TimePublicationGenerator(_minSeconds, _maxSeconds, _startPostingHour, _endPostingHour);
         Task.Factory.StartNew(
            PublishPostsAction, _cancellationToken, TaskCreationOptions.LongRunning, TaskScheduler.Default);
      }

      public void Stop()
      {
         _tokenSource.Cancel();
      }

      public void Suspend()
      {
         _tokenSource.Cancel();
      }

      public void Resume()
      {
         Start();
      }

      private void PublishPostsAction()
      {
         bool regenerated = false;

         while (!_cancellationToken.IsCancellationRequested)
         {
            DateTime nowDateTime = DateTime.Now;
            int nowHour = nowDateTime.Hour;

            if (nowHour < _timelineGenerator.StartPostingHour)
            {
               _trackHandler.Handle("Too early for the pushing publications.");
               regenerated = false;
            }
            else if (nowHour >= _timelineGenerator.StartPostingHour && nowHour <= _timelineGenerator.EndPostingHour)
            {
               if (!regenerated)
               {
                  _timelineGenerator = new TimePublicationGenerator(_minSeconds, _maxSeconds, nowHour, _endPostingHour);
                  _timelineGenerator.Regenerate();
                  regenerated = true;
               }

               if (!_timelineGenerator.IsEmpty())
               {
                  DateTime first;
                  if (_timelineGenerator.Peek(out first))
                  {
                     if (first <= nowDateTime && _timelineGenerator.Remove(first))
                     {
                        bool successPublish;
                        PostAction(out successPublish);
                        if (successPublish)
                        {
                           DateTime next;
                           string logMessage = _timelineGenerator.Peek(out next)
                              ? string.Format("The next publication will be through {0}", next)
                              : "Today publications will no longer";
                           _trackHandler.Handle(logMessage);
                        }
                     }
                     else
                     {
                        _trackHandler.Handle(string.Format("The time has not come yet for {0}", first));
                     }
                  }
               }
               else
               {
                  _trackHandler.Handle("Today publications will no longer");
               }
            }
            else
            {
               _trackHandler.Handle("Too early for the pushing publications");
               regenerated = false;
            }

            _trackHandler.Handle(string.Format("Next check will be in {0} seconds", _poolingInterval));
            Thread.Sleep(TimeSpan.FromSeconds(_poolingInterval));
         }
      }
      
      private void PostAction(out bool successOperation)
      {
         try
         {
            Post[] postsToPublish = _socialAutopostRepository.GetPostsToPublish();
            if (postsToPublish != null && postsToPublish.Length > 0)
            {
               IEnumerable<Uri> postUris = _socialAutopostRepository.GetPostUris(_socialAutopostRepository.GetPublishedPosts());
               Uri[] notDeferredPostUris = postUris as Uri[] ?? postUris.ToArray();
               IList<LinkStatistics> statLinks = AutopostUtils.GetLinkStatistics(notDeferredPostUris);

               Post firstSuitablePost = null;
               foreach (var post in postsToPublish.Where(post => post != null))
               {
                  string postedHost = new Uri(post.PureUri).Host;
                  if (!AutopostUtils.HasExceededDomainLimit(postedHost, SocialKeysReader.PercentDomainLimit, statLinks))
                  {
                     if (AutopostUtils.IsResourceAvailable(post.PureUri))
                     {
                        firstSuitablePost = post;
                        break;
                     }

                     _trackHandler.Handle(string.Format("Resource by URI {0} is not available", post.PureUri));
                     _socialAutopostRepository.SetPostUnavailable(post.PostId);
                  }
                  else
                  {
                     // TODO: Пометить пост как превышающий предел домена
                     _trackHandler.Handle(string.Format("Host {0} has exceeded domain limit", postedHost));
                  }
               }
               
               if (firstSuitablePost != null)
               {
                  IPublishMessage twitterPublishMessage =
                     new TwitterPublishMessage(_twitterConfig.ConsumerKey, _twitterConfig.ConsumerSecret,
                        _twitterConfig.AccessToken, _twitterConfig.AccessTokenSecret);
                  bool publishSuccess = twitterPublishMessage.Publish(firstSuitablePost.PostText);
                  if (publishSuccess)
                  {
                     _socialAutopostRepository.SetInPublish(firstSuitablePost.PostId);
                     _trackHandler.Handle(string.Format("{0} published successfully", firstSuitablePost.PureUri));
                  }
                  else
                  {
                     _trackHandler.Handle(string.Format("Twitter does not want to publish {0}", firstSuitablePost.PureUri));
                  }
               }
            }
         }
         catch (Exception ex)
         {
            _trackHandler.Handle(ex);
            successOperation = false;
            return;
         }

         successOperation = true;
      }
   }
}