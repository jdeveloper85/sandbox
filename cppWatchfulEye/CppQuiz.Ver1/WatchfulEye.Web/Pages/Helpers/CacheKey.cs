﻿namespace WatchfulEye.Web.Pages.Helpers
{
    public enum CacheKey
    {
        ProjectLogo,
        LastServerError
    }
}