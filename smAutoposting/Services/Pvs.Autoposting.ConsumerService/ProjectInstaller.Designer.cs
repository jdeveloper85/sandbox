﻿namespace Pvs.Autoposting.ConsumerService
{
   partial class ProjectInstaller
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary> 
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Component Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.AutopostConsumerServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
         this.AutopostConsumerServiceInstaller = new System.ServiceProcess.ServiceInstaller();
         // 
         // AutopostConsumerServiceProcessInstaller
         // 
         this.AutopostConsumerServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
         this.AutopostConsumerServiceProcessInstaller.Password = null;
         this.AutopostConsumerServiceProcessInstaller.Username = null;
         // 
         // AutopostConsumerServiceInstaller
         // 
         this.AutopostConsumerServiceInstaller.Description = "Service for consuming autoposted links to publish";
         this.AutopostConsumerServiceInstaller.DisplayName = "Autopost Consumer";
         this.AutopostConsumerServiceInstaller.ServiceName = "AutopostConsumerService";
         // 
         // ProjectInstaller
         // 
         this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.AutopostConsumerServiceProcessInstaller,
            this.AutopostConsumerServiceInstaller});

      }

      #endregion

      private System.ServiceProcess.ServiceProcessInstaller AutopostConsumerServiceProcessInstaller;
      private System.ServiceProcess.ServiceInstaller AutopostConsumerServiceInstaller;
   }
}