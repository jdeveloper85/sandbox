﻿using System.Collections.Generic;
using System.Linq;
using CppQuiz.Data;
using NUnit.Framework;

namespace Qiuz.Tests
{
   [TestFixture]
   public class SegmentPreprocessorTests
   {
      private string[] _codeFragments;

      [SetUp]
      public void Init()
      {
         using (var quizEntities = new CppQuizEntities())
         {
            _codeFragments = (from question in quizEntities.Questions
                              select question.CodeFragment).ToArray();
         }
      }

      [Test]
      public void DecorateTest()
      {
         var preprocessor = new SegmentPreprocessor();

         var decoratedCodeFragments = new List<string>();
         var rightIndexes = new List<List<int>>();

         foreach (var codeFragment in _codeFragments)
         {
            List<int> indexes;
            decoratedCodeFragments.Add(preprocessor.Decorate(codeFragment, out indexes));
            rightIndexes.Add(indexes);
         }

         Assert.AreEqual(decoratedCodeFragments.Count, rightIndexes.Count);
         foreach (var rightIndex in rightIndexes)
         {
            Assert.IsTrue(rightIndex.Count > 0);
         }
      }
   }
}