﻿using System;
using System.Web.UI;
using Pvs.Quiz.Common;

namespace WatchfulEye.Web.Pages.PostLevels
{
    public partial class Advanced : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Uri urlReferrer = Request.UrlReferrer;
            if (urlReferrer != null)
            {
                Response.RedirectToRoute("RootRoute");
                return;
            }

            ClientRedirectPlaceHolder.Visible = true;
        }

        protected static string GetDefinitionText()
        {
            return string.Format("PVS-Studio had measured my C++ skill as: {0}", CppUserLevel.Advanced.GetDefinition());
        }
    }
}