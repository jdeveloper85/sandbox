﻿namespace Pvs.Quiz.Common
{
   /// <summary>
   ///    Уровень пользователя
   /// </summary>
   public enum CppUserLevel
   {
      [LevelDefinition("Should not be allowed near C++")]
      [LevelImage("/Images/level1.png")]
      Dummy = 1,

      [LevelDefinition("Does not comprehend the wickedness of C++")]
      [LevelImage("/Images/level2.png")]
      Beginner = 2,

      [LevelDefinition("Knows something about C++ wickedness")]
      [LevelImage("/Images/level3.png")]
      Medium = 3,

      [LevelDefinition("Ready for real life C++")]
      [LevelImage("/Images/level4.png")]
      Advanced = 4,

      [LevelDefinition("Pedant of C++")]
      [LevelImage("/Images/level5.png")]
      Pro = 5,

      [LevelDefinition("Scored 100% by tricking the system")]
      [LevelImage("/Images/level6.png")]
      Haker = 6
   }
}