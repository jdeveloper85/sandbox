﻿/**
 * Запись всех контактов в постоянное хранилище
 */

using System.Collections.Generic;
using System.IO;
using System.Linq;
using Pvs.MailGenerator.Common;
using Pvs.MailGenerator.MailGeneratorEntities;

namespace Pvs.MailGenerator.Store
{
   internal static class EntryPoint
   {
      private const string ContactsFile = @"contacts.xlsx";

      private static void Main()
      {
         var contacts = GetContacts();
         using (var mailGenerator = new MailGeneratorContext())
         {
            foreach (
               var contact in
                  contacts.Where(contact => !string.IsNullOrEmpty(contact.Name) && !string.IsNullOrEmpty(contact.Mail)))
            {
               mailGenerator.Contacts.Add(new Contact
               {
                  Name = contact.Name,
                  Mail = contact.Mail,
                  Text = contact.LinkText,
                  Link = contact.LinkUrl
               });
            }

            mailGenerator.SaveChanges();
         }
      }

      private static IEnumerable<IContact> GetContacts()
      {
         IEnumerable<IContact> contacts;
         var fullContactFile = Path.GetFullPath(ContactsFile);
         using (var contactReader = new XlsxContactReader(fullContactFile))
         {
            contacts = contactReader.GetContacts();
         }

         return contacts;
      }
   }
}