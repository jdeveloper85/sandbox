﻿/**
 * Внешнее API загрузки документов для http://vk.com
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using VkApi.Library;

namespace VkApiSample
{
   internal static class VkPublishDocumentsEntryPoint
   {
      private static readonly AppSettingsReader AppSettingsReader = new AppSettingsReader();
      private static readonly Random IntervalRandom = new Random();
      private static readonly CancellationTokenSource TokenSource = new CancellationTokenSource();
      private static readonly VkSettings VkSettings;

      static VkPublishDocumentsEntryPoint()
      {
         var appId = GetByKey<int>("VkAppId");
         var login = GetByKey<string>("VkLogin");
         var pass = GetByKey<string>("VkPassword");
         var groupName = GetByKey<string>("VkGroupName");
         var groupId = GetByKey<int>("VkGroupId");
         var docLimit = GetByKey<int>("DocLimit");
         VkSettings = new VkSettings(appId, login, pass, groupName, groupId, docLimit);
      }

      private static void Main(string[] args)
      {
         if (args == null || args.Length != 3)
         {
            PrintUsage();
            return;
         }

         string errorMessage;
         CommandArguments arguments = ParseArguments(args, out errorMessage);
         if (string.IsNullOrEmpty(errorMessage))
         {
            Task.Factory.StartNew(VkUploadAction, arguments, TokenSource.Token);
            Console.WriteLine("Press any key to exit the program");
            Console.ReadKey();
            TokenSource.Cancel();
         }
         else
         {
            Console.WriteLine(errorMessage);
         }
      }

      private static void VkUploadAction(object stateObj)
      {
         var args = (CommandArguments)stateObj;
         const string searchPattern = "*.pdf";

         while (!TokenSource.IsCancellationRequested)
         {
            try
            {
               string[] pdfDocuments = Directory.GetFiles(args.DocumentFolder, searchPattern,
                  SearchOption.TopDirectoryOnly);
               string[] limitedDocuments = pdfDocuments.Length <= VkSettings.DocumentLimit
                  ? pdfDocuments
                  : pdfDocuments.Take(VkSettings.DocumentLimit).ToArray();

               var uploadExecutor = new VkApiCallWrapper(VkSettings);

               foreach (string pdfDocument in limitedDocuments)
               {
                  uploadExecutor.UploadDocument(pdfDocument);
                  File.Delete(pdfDocument);
                  checked
                  {
                     int sleepValue = IntervalRandom.Next((int)args.MinTimeslotSec, (int)args.MaxTimeslotSec);
                     Console.WriteLine("{0}: document {1} uploaded successfully", DateTime.Now.ToString("T"),
                        pdfDocument);
                     Thread.Sleep(TimeSpan.FromSeconds(sleepValue));
                  }
               }

               Environment.Exit(0);
            }
            catch (Exception ex)
            {
               Console.WriteLine(ex.Message);
               Console.Write("Press any key to exit the program");
               Console.ReadKey();
               Environment.Exit(1);
            }
         }
      }

      private static CommandArguments ParseArguments(IList<string> args, out string errorMessage)
      {
         string documentFolder;
         uint minTimeslotSec, maxTimeslotSec;
         errorMessage = string.Empty;

         if (!Directory.Exists(args[0]))
         {
            errorMessage = string.Format("Directory {0} is not exist", args[0]);
            documentFolder = string.Empty;
         }
         else
         {
            documentFolder = args[0];
         }

         if (!uint.TryParse(args[1], out minTimeslotSec))
         {
            errorMessage = "MinTimeslotSec must be int";
            minTimeslotSec = 0;
         }

         if (!uint.TryParse(args[2], out maxTimeslotSec))
         {
            errorMessage = "MaxTimeslotSec must be int";
            maxTimeslotSec = 0;
         }

         return new CommandArguments
         {
            DocumentFolder = documentFolder,
            MinTimeslotSec = minTimeslotSec,
            MaxTimeslotSec = maxTimeslotSec
         };
      }

      private static T GetByKey<T>(string key)
      {
         try
         {
            return (T)AppSettingsReader.GetValue(key, typeof(T));
         }
         catch (InvalidOperationException invalidOperationEx)
         {
            throw new Exception(invalidOperationEx.Message, invalidOperationEx);
         }
         catch (ArgumentNullException argNullEx)
         {
            throw new Exception(argNullEx.Message, argNullEx);
         }
      }

      private static void PrintUsage()
      {
         AssemblyName assemblyName = Assembly.GetExecutingAssembly().GetName();
         Console.WriteLine("Usage: {0} DocumentFolder MinTimeslotSec MaxTimeslotSec", assemblyName.Name);
         Console.WriteLine("\tDocumentFolder: Каталог, в котором находятся pdf-документы для публикации");
         Console.WriteLine("\tMinTimeslotSec: Минимальное значение времени между загрузками документов в секундах");
         Console.WriteLine("\tMaxTimeslotSec: Максимальное значение времени между загрузками документов в секундах");
      }

      #region Служебные типы

      private struct CommandArguments
      {
         public string DocumentFolder { get; set; }

         public uint MinTimeslotSec { get; set; }

         public uint MaxTimeslotSec { get; set; }
      }

      #endregion
   }
}