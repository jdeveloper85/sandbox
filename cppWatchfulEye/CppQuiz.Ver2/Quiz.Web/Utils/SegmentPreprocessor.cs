﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz.Web.Utils
{
   public sealed class SegmentPreprocessor
   {
      private const string DefaultTagName = "b";
      private const string DefaultAttributeName = "data-index";
      private const string DefaultClassName = "highlight-fragment";

      private const string CodeFragmentRePattern = @"(?<CodeSegment>(([a-zA-Z_\d])+|" +
                                                   @"([\d]+)|" +
                                                   @"([\(\)\[\]\{\};\,\?""']{1})|" +
                                                   @"(\+[+,=]?)|" +
                                                   @"(\-[-,>,=]?)|" +
                                                   @"(%[=]?)|" +
                                                   @"(\*[*,=]?)|" +
                                                   @"(/[=]?)|" +
                                                   @"(\:{1,2})|" +
                                                   @"((\![=]?){1})|" +
                                                   @"(={1,2})|" +
                                                   @"(<[=,<]?)|" +
                                                   @"(>[>,=]?)|" +
                                                   @"(&{1,2})|" +
                                                   @"(\|{1,2})))";

      private const char MarkedCorrectSymbol = '@';
      private const string CodesegmentGroupname = "CodeSegment";
      private readonly string _attributeName;
      private readonly string _className;
      private readonly string _tagName;

      public SegmentPreprocessor()
         : this(DefaultTagName, DefaultAttributeName, DefaultClassName)
      {
      }

      private SegmentPreprocessor(string tagName, string attributeName, string className)
      {
         _tagName = tagName;
         _attributeName = attributeName;
         _className = className;
      }

      public Task<Tuple<string, IList<int>>> DecorateAsync(string codeFragment)
      {
         return Task.Run(() => Decorate(codeFragment));
      }

      private Tuple<string, IList<int>> Decorate(string codeFragment)
      {
         var indexHolder = new FindIndexHolder();
         var decoratedCode = CodeFragmentRegex.Replace(codeFragment, match =>
         {
            var codeSegmentValue = match.Groups[CodesegmentGroupname].Value;
            var foundIndex = match.Index;
            indexHolder.CurrentIndex++;
            if (foundIndex > 0 && codeFragment[foundIndex - 1] == MarkedCorrectSymbol &&
                codeFragment[foundIndex + codeSegmentValue.Length] == MarkedCorrectSymbol)
            {
               indexHolder.RightIndexes.Add(indexHolder.CurrentIndex);
            }

            return string.Format("<{0} {1}=\"{2}\" class=\"{3}\">{4}</{0}>", _tagName, _attributeName,
               indexHolder.CurrentIndex, _className, codeSegmentValue);
         });

         IList<int> rightIndexes = indexHolder.RightIndexes;
         var decorated =
            new StringBuilder(decoratedCode).Replace(string.Format("{0}", MarkedCorrectSymbol), string.Empty).ToString();
         return Tuple.Create(decorated, rightIndexes);
      }

      private class FindIndexHolder
      {
         public int CurrentIndex;
         public readonly List<int> RightIndexes;

         public FindIndexHolder()
         {
            CurrentIndex = 0;
            RightIndexes = new List<int>();
         }
      }

      private static readonly Regex CodeFragmentRegex = new Regex(CodeFragmentRePattern, RegexOptions.Compiled);
   }
}