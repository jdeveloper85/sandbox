﻿using System.IO;
using System.Web;

namespace Quiz.Web
{
   /// <summary>
   ///    Модуль переадресации запросов на мобильные устройства
   /// </summary>
   public class MobileModule : IHttpModule
   {
      public void Init(HttpApplication context)
      {
         context.BeginRequest += (sender, args) =>
         {
            var requested = context.Request.Path;
            if (requested.ToLower().EndsWith(".aspx") && !requested.ToLower().EndsWith(".mobile.aspx")
                && context.Request.Browser.IsMobileDevice)
            {
               var pathElems = requested.Split('.');
               pathElems[pathElems.Length - 1] = "Mobile.aspx";
               var target = string.Join(".", pathElems);
               if (File.Exists(context.Request.MapPath(target)))
               {
                  context.Server.Transfer(target);
               }
            }
         };
      }

      public void Dispose()
      {
      }
   }
}