﻿/**
 * Генератор временных интервалов
 */

using System;
using Pvs.Autopost.Core;

namespace DateTimeGeneratedQueue
{
   internal class Program
   {
      private const int MinSecondValue = 300; // 5 минут
      private const int MaxSecondValue = 25200; // 7 часов
      private const int StartPostingHour = 8;
      private const int EndPostingHour = 23;

      private static void Main()
      {
         var publicationGenerator =
            new TimePublicationGenerator(MinSecondValue, MaxSecondValue, StartPostingHour, EndPostingHour);
         publicationGenerator.Regenerate();

         while (!publicationGenerator.IsEmpty())
         {
            DateTime first;
            if (publicationGenerator.Peek(out first))
            {
               Console.WriteLine(first);
               publicationGenerator.Remove(first);
            }
         }

         Console.WriteLine();
         publicationGenerator.Regenerate();

         while (!publicationGenerator.IsEmpty())
         {
            DateTime first;
            if (publicationGenerator.Peek(out first))
            {
               Console.WriteLine(first);
               publicationGenerator.Remove(first);
            }
         }
      }
   }
}