﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Crud/Admin.Master" AutoEventWireup="true"
    CodeBehind="Questions.aspx.cs" Inherits="WatchfulEye.Web.Pages.Crud.Questions"
    ValidateRequest="false" %>

<%@ Import Namespace="WatchfulEye.Web" %>

<asp:Content ID="QuestionsContent" ContentPlaceHolderID="CrudContentPlaceHolder" runat="server">
    <a href="../../QuestionSqlInsertDumpHandler.ashx" target="blank">C++ Quiz Database backup
    </a>
    <asp:UpdatePanel runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:ListView ID="QuestionsListView" runat="server" DataKeyNames="QuestionId"
                ItemType="WatchfulEye.DataLayer.Question" InsertItemPosition="LastItem"
                UpdateMethod="UpdateQuestion"
                InsertMethod="InsertQuestion"
                SelectMethod="GetQuestions"
                DeleteMethod="DeleteQuestion">
                <EditItemTemplate>
                    <li class="edit">
                        <table class="adminTable">
                            <tr>
                                <td class="error" colspan="2">
                                    <asp:ValidationSummary runat="server" ID="EditValidationSummary" DisplayMode="BulletList" />
                                </td>
                            </tr>
                            <tr>
                                <td>Code Fragment:</td>
                                <td>
                                    <textarea id="codeFragment" runat="server" rows="30" cols="80" value="<%# BindItem.CodeFragment %>" />
                                </td>
                            </tr>
                            <tr>
                                <td>Project Name:</td>
                                <td>
                                    <input type="text" runat="server" id="projectName" size="50" value="<%# BindItem.ProjectName %>" />
                                </td>
                            </tr>
                            <tr>
                                <td>Project Logo:</td>
                                <td style="width: 100%;">
                                    <ajaxToolkit:AjaxFileUpload runat="server" ID="UpdateLogoFileUploader"
                                        Mode="Client"
                                        MaximumNumberOfFiles="1"
                                        AllowedFileTypes="png,jpg,jpeg,gif"
                                        OnUploadComplete="UpdateLogoFileUploader_OnUploadComplete" />
                                </td>
                            </tr>
                            <tr>
                                <td>Explanatory Text:</td>
                                <td>
                                    <textarea id="explanatoryTextTextBox" runat="server" rows="15" cols="50" value="<%# BindItem.ExplanatoryText %>" />
                                </td>
                            </tr>
                            <tr>
                                <td>PVS-Studio Warning:</td>
                                <td>
                                    <textarea runat="server" id="pvsStudioWarning" rows="5" cols="50" value="<%# BindItem.PvsStudioWarning %>" />
                                </td>
                            </tr>
                        </table>
                    </li>
                    <div class="nav">
                        <asp:Button CssClass="actionButtons" ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
                        <asp:Button CssClass="actionButtons" ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" />
                        <asp:Button CssClass="actionButtons" ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                    </div>
                </EditItemTemplate>
                <EmptyDataTemplate>
                    No data was returned.
                </EmptyDataTemplate>
                <InsertItemTemplate>
                    <li class="insert">
                        <table class="adminTable">
                            <tr>
                                <td class="error" colspan="2">
                                    <asp:ValidationSummary CssClass="error-list" runat="server" ID="InsertValidationSummary" DisplayMode="BulletList" />
                                </td>
                            </tr>
                            <tr>
                                <td>Code Fragment:</td>
                                <td>
                                    <textarea id="codeFragment" runat="server" rows="30" cols="80" value="<%# BindItem.CodeFragment %>" />
                                </td>
                            </tr>
                            <tr>
                                <td>Project Name:</td>
                                <td>
                                    <input type="text" runat="server" id="projectName" size="50" value="<%# BindItem.ProjectName %>" />
                                </td>
                            </tr>
                            <tr>
                                <td>Project Logo:</td>
                                <td style="width: 100%;">
                                    <ajaxToolkit:AjaxFileUpload ID="InsertLogoFileUploader" runat="server"
                                        Mode="Client"
                                        MaximumNumberOfFiles="1"
                                        AllowedFileTypes="png,jpg,jpeg,gif"
                                        OnUploadComplete="InsertLogoFileUploader_OnUploadComplete" />
                                </td>
                            </tr>
                            <tr>
                                <td>Explanatory Text:</td>
                                <td>
                                    <textarea id="explanatoryText" runat="server" rows="15" cols="50" value="<%# BindItem.ExplanatoryText %>" />
                                </td>
                            </tr>
                            <tr>
                                <td>PVS-Studio Warning:</td>
                                <td>
                                    <textarea runat="server" id="pvsStudioWarning" rows="5" cols="50" value="<%# BindItem.PvsStudioWarning %>" />
                                </td>
                            </tr>
                        </table>
                    </li>
                    <div class="nav">
                        <asp:Button CssClass="actionButtons" ID="InsertButton" runat="server" CommandName="Insert" Text="Insert" />
                        <asp:Button CssClass="actionButtons" ID="CancelButton" runat="server" CommandName="Cancel" Text="Clear" />
                    </div>
                </InsertItemTemplate>
                <ItemSeparatorTemplate>
                    <hr class="item-separator" />
                </ItemSeparatorTemplate>
                <ItemTemplate>
                    <li class="item-template">
                        <table class="adminTable">
                            <tr>
                                <td>Code Fragment:</td>
                                <td>
                                    <pre><%# Item.CodeFragment %></pre>
                                </td>
                            </tr>
                            <tr>
                                <td>Project Name:</td>
                                <td><%# Item.ProjectName %></td>
                            </tr>
                            <tr>
                                <td>Project Logo:</td>
                                <td>
                                    <img src="<%# RootVirtualPath %>GetImageHandler.ashx?<%# GetImageHandler.QuestionIdQueryStringName %>=<%# Item.QuestionId %>" alt="" /></td>
                            </tr>
                            <tr>
                                <td>Explanatory Text:</td>
                                <td><%# Item.ExplanatoryText %></td>
                            </tr>
                            <tr>
                                <td>PVS-Studio Warning:</td>
                                <td><%# Item.PvsStudioWarning %></td>
                            </tr>
                        </table>
                    </li>
                    <div class="nav">
                        <asp:Button CssClass="actionButtons" ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                    </div>
                </ItemTemplate>
                <LayoutTemplate>
                    <ul id="itemPlaceholderContainer" runat="server" class="layout-template">
                        <li runat="server" id="itemPlaceholder" />
                    </ul>
                    <div class="layout-template-pager">
                        <asp:DataPager ID="BottomDataPager" PageSize="10" runat="server">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonCssClass="pager" ButtonType="Button" ShowFirstPageButton="True" ShowNextPageButton="False" ShowPreviousPageButton="False" NextPageText=" " />
                                <asp:NumericPagerField NumericButtonCssClass="pager" />
                                <asp:NextPreviousPagerField ButtonCssClass="pager" ButtonType="Button" ShowLastPageButton="True" ShowNextPageButton="False" ShowPreviousPageButton="False" />
                            </Fields>
                        </asp:DataPager>
                    </div>
                </LayoutTemplate>
            </asp:ListView>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
