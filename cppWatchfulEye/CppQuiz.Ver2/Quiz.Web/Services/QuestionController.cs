﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using CppQuiz.Data;
using Pvs.Quiz.Common;
using Quiz.Web.Infrastructure.Abstract;
using Quiz.Web.Infrastructure.Concrete;
using Quiz.Web.Model;
using Quiz.Web.Utils;
using Quiz.Web.Utils.Web;

namespace Quiz.Web.Services
{
   [RoutePrefix("quiz")]
   public class QuestionController : ApiController
   {
      private readonly IQuizRepository<QuizQuestion> _quizRepository = new AppCachedRepository(new QuizEntityRepository())
      {
         QuizQuestions = AppCacheHelper.GetQuestions(HttpContext.Current.Cache)
      };

      [Route("question")]
      public async Task<dynamic> Get()
      {
         if (HttpContext.Current == null || HttpContext.Current.Session == null)
            return null;

         var session = SessionHelper.GetOrCreate(HttpContext.Current.Session);
         if (session.QuestionIds.Count == 0)
            return GenerateRedirectResponse("TestComplete.aspx");

         var questionId = session.QuestionIds.Peek();
         var currentQuestionNumber = session.CurrentQuestionNumber;
         if (currentQuestionNumber > WebConfigReader.TestQuestionsNumber)
            return GenerateRedirectResponse("TestComplete.aspx");

         var maxQuestionTime = WebConfigReader.QuestionTime;
         var timeRemaining = session.GetTimeRemaining(questionId);
         var remainingMinutes = timeRemaining.Minutes;
         var remainingSeconds = timeRemaining.Seconds;
         var secondsLeft = remainingMinutes >= 1
            ? 0
            : (remainingSeconds >= maxQuestionTime ? 0 : maxQuestionTime - remainingSeconds);

         var question = await _quizRepository.GetQuestionAsync(questionId);
         var fragmentResult = await TsSingleton<SegmentPreprocessor>.Instance.DecorateAsync(question.CodeFragment);
         session.CurrentCorrectIndices = fragmentResult.Item2;

         return new
         {
            questionNumber = currentQuestionNumber,
            questionId = question.QuestionId,
            qSecondsLeft = secondsLeft,
            codeFragment = fragmentResult.Item1,
            explanatoryText = question.ExplanatoryText,
            projectName = question.ProjectName,
            pvsStudioWarning = question.PvsStudioWarning,
            isLastQuestion = currentQuestionNumber == WebConfigReader.TestQuestionsNumber,
            totalQuestionCount = WebConfigReader.TestQuestionsNumber
         };
      }

      [Route("stat")]
      [HttpPost]
      [ResponseType(typeof(AnswerResponse))]
      public async Task<IHttpActionResult> Post(FormDataCollection form)
      {
         AnswerRequest answerRequest;
         string errorMessage;

         if (!QuestionApiControllerFormValueParser.ParseFormValues(form, out answerRequest, out errorMessage))
            return BadRequest(errorMessage);

         var session = SessionHelper.GetOrCreate(HttpContext.Current.Session);
         session.CurrentQuestionNumber++;
         if (session.QuestionIds.Count == 0)
            return BadRequest("Your test session is complete");

         var questionId = session.QuestionIds.Dequeue();
         var currentCorrectIndices = session.CurrentCorrectIndices;

         AnswerState answerState;
         switch (answerRequest.AnswerRequestKind)
         {
            case AnswerRequestKind.DoesNotKnow:
               answerState = AnswerState.DontKnow;
               break;

            case AnswerRequestKind.TimeIsUp:
               answerState = AnswerState.TimeIsUp;
               break;

            case AnswerRequestKind.TryToAnswer:
               answerState = currentCorrectIndices.Any(correctIndex => answerRequest.DataIndex == correctIndex)
                  ? AnswerState.Right
                  : AnswerState.Wrong;
               break;

            case AnswerRequestKind.NoErrors:
               answerState = currentCorrectIndices.Count == 0 ? AnswerState.Right : AnswerState.Wrong;
               break;

            default:
               return BadRequest(string.Format("Detecting unknown action: {0}", answerRequest.AnswerRequestKind));
         }

         if (session.AnswerStat.ContainsKey(answerState))
         {
            session.AnswerStat[answerState]++;
            var successUpdate = await _quizRepository.UpdateQuestionStatAsync(questionId, answerState);
            if (successUpdate)
            {
               return Ok(new AnswerResponse
               {
                  AnswerState = answerState.ToString(),
                  DataIndex = answerRequest.DataIndex,
                  CorrectIndices = currentCorrectIndices.ToArray(),
                  CurrentCorrectAnswersNumber = session.AnswerStat[AnswerState.Right]
               });
            }

            return BadRequest("Detected unresolved update");
         }

         return BadRequest(string.Format("Detecting bad answer state value {0}", answerState));
      }

      protected override void Dispose(bool disposing)
      {
         if (disposing)
         {
            _quizRepository.Dispose();
         }

         base.Dispose(disposing);
      }

      private HttpResponseMessage GenerateRedirectResponse(string redirectPage)
      {
         var redirectResponse = Request.CreateResponse(HttpStatusCode.Moved);
         redirectResponse.Headers.Location = new Uri(string.Format("{0}/{1}", Request.Headers.Host, redirectPage));
         return redirectResponse;
      }
   }
}