using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Microsoft.Office.Interop.Excel;
using Pvs.MailGenerator.Common;

namespace Pvs.MailGenerator.Store
{
   public sealed class XlsxContactReader : IDisposable
   {
      private Application _contactApp;
      private readonly string _contactFile;

      public XlsxContactReader(string contactFile)
      {
         _contactFile = contactFile;
      }

      public void Dispose()
      {
         if (_contactApp != null)
         {
            _contactApp.Quit();
         }
      }

      public IEnumerable<IContact> GetContacts()
      {
         _contactApp = new Application { Visible = false };
         var contactsWorkbook = _contactApp.Workbooks.Open(_contactFile);
         var contactsSheet = (Worksheet)contactsWorkbook.Sheets[1];
         var lastRowIndex = contactsSheet.Cells.SpecialCells(XlCellType.xlCellTypeLastCell).Row;
         IList<IContact> contacts = new List<IContact>();
         for (var i = 1; i <= lastRowIndex; i++)
         {
            dynamic values = contactsSheet.Range[string.Format("A{0}", i), string.Format("C{0}", i)].Cells.Value2;
            var contactName = values.GetValue(1, 1) as string;
            var contactEmail = values.GetValue(1, 2) as string;
            var linkText = values.GetValue(1, 3) as string;
            var hyperlinks = contactsSheet.Range[string.Format("C{0}", i), string.Format("C{0}", i)].Cells.Hyperlinks;
            var linkUrl = Utils.GetLinkUrl(hyperlinks);

            if (string.IsNullOrEmpty(linkUrl))
            {
               var dividedText = Utils.DivideTextByLink(linkText);
               linkText = dividedText.Item1;
               linkUrl = dividedText.Item2;
            }
            
            contacts.Add(new ContactImpl
            {
               Name = !string.IsNullOrWhiteSpace(contactName) ? contactName.Trim() : contactName,
               Mail = !string.IsNullOrWhiteSpace(contactEmail) ? contactEmail.Trim() : contactEmail,
               LinkText = linkText,
               LinkUrl = linkUrl
            });
         }

         return contacts;
      }

      #region �������� �������

      private static class Utils
      {
         private static readonly Regex HyperlinkRegex =
            new Regex(@"(http|https|ftp)://([\w+?\.\w+])+([a-zA-Z0-9\~\!\@\#\$\%\^\&\*\(\)_\-\=\+\\\/\?\.\:\;\'\,]*)?",
               RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

         /// <summary>
         ///    ������� ������ �� ����� � ������
         /// </summary>
         /// <param name="textToSplit">������� ������</param>
         /// <returns>������ [�����, ������]</returns>
         public static Tuple<string, string> DivideTextByLink(string textToSplit)
         {
            if (string.IsNullOrWhiteSpace(textToSplit))
               return Tuple.Create(string.Empty, string.Empty);

            textToSplit = textToSplit.Trim();
            Tuple<string, string> retValue;

            var linkMatches = HyperlinkRegex.Matches(textToSplit);
            if (linkMatches.Count > 0)
            {
               var linkMatch = linkMatches[0];
               var resultLink = linkMatch.ToString();
               var restText = textToSplit.Substring(0, linkMatch.Index).TrimEnd();
               retValue = Tuple.Create(restText, resultLink);
            }
            else
            {
               retValue = Tuple.Create(textToSplit, String.Empty);
            }

            return retValue;
         }

         public static string GetLinkUrl(Hyperlinks hyperlinks)
         {
            var linkUrl = String.Empty;
            if (hyperlinks == null || hyperlinks.Count <= 0)
               return linkUrl;

            foreach (Hyperlink hyperlink in hyperlinks)
            {
               if (hyperlink != null && !String.IsNullOrWhiteSpace(hyperlink.Address))
               {
                  var address = hyperlink.Address;
                  var subAddress = hyperlink.SubAddress;
                  if (!String.IsNullOrEmpty(address))
                  {
                     linkUrl = address;
                     if (!String.IsNullOrEmpty(subAddress))
                     {
                        linkUrl = String.Format("{0}#{1}", linkUrl, subAddress);
                     }
                  }

                  break;
               }
            }

            return linkUrl;
         }
      }

      #endregion
   }
}