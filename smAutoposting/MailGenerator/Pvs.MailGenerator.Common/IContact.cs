namespace Pvs.MailGenerator.Common
{
   public interface IContact
   {
      string Name { get; set; }

      string Mail { get; set; }

      string LinkText { get; set; }

      string LinkUrl { get; set; }

      string BitlyLink { get; set; }
   }
}