﻿using System.Configuration;

namespace Pvs.Autopost.Config
{
   public sealed class RecipientElement : ConfigurationElement
   {
      [ConfigurationProperty("displayName", DefaultValue = "", IsKey = false, IsRequired = true)]
      public string DisplayName
      {
         get { return (string)base["displayName"]; }
      }

      [ConfigurationProperty("address", DefaultValue = "", IsKey = true, IsRequired = true)]
      public string Address
      {
         get { return (string)base["address"]; }
      }
   }
}