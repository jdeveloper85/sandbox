using System;
using System.Collections.Generic;

namespace Pvs.Autoposting.Entities
{
   /// <summary>
   ///    ��������� ��� ������-������ ������ � �������
   /// </summary>
   public interface IAutopostRepository
   {
      /// <summary>
      ///    ��������� ����� ������ ��� ����������
      /// </summary>
      /// <param name="takeLimit">���-�� ������</param>
      /// <returns>����� �����</returns>
      IEnumerable<Post> GetNewPosts(int takeLimit);

      /// <summary>
      ///    ��������� ���� ������
      /// </summary>
      /// <returns>��� �����</returns>
      IEnumerable<Post> GetPublishedPosts();

      /// <summary>
      ///    ��������� ������ � ����� ���������� ��� ����������
      /// </summary>
      /// <param name="postIds">�������������� ������ ��� ����������</param>
      /// <returns>���-�� ����������� ���������</returns>
      void SetInPending(IEnumerable<int> postIds);

      /// <summary>
      ///    ��������� URI ��� ������
      /// </summary>
      /// <param name="posts">�����</param>
      /// <returns>����� URI ��� ������</returns>
      IEnumerable<Uri> GetPostUris(IEnumerable<Post> posts);

      /// <summary>
      ///    ��������� ����� ��� �����������
      /// </summary>
      /// <param name="postId">Id-�����</param>
      /// <returns>true, ���� ���� ������� �������� � ���� ��� �����������, false - � ��������� ������</returns>      
      void SetInPublish(int postId);

      /// <summary>
      ///    �������� ������ ����, ��������� ��� ����������
      /// </summary>
      /// <returns>������ ����, ��������� ��� ����������</returns>
      Post[] GetPostsToPublish();

      /// <summary>
      ///    ��������� ���� ������
      /// </summary>
      /// <returns>��� �����</returns>
      IEnumerable<Post> GetPosts();

      /// <summary>
      /// ���������� �����
      /// </summary>
      /// <param name="post">����</param>
      void Save(Post post);

      /// <summary>
      /// �������� �����
      /// </summary>
      /// <param name="aPost">���� ��� ��������</param>
      void Delete(Post aPost);

      /// <summary>
      /// ��������� ���������� ��� �����������
      /// </summary>
      /// <param name="postId">Id-�����</param>
      void SetPostUnavailable(int postId);
   }
}