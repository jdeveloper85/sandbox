﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pvs.Autoposting.Entities
{
   /// <summary>
   ///    Утилиты проверки URI для валидатора привязки модели
   /// </summary>
   public static class PureUriValidator
   {
      /// <summary>
      ///    Проверка URI на корректность и уникальность
      /// </summary>
      /// <param name="pureUriString">Строка URI</param>
      /// <param name="errorMessage">Сообщение об ошибке</param>
      /// <param name="excludeCurrent">Без учета уникальности текущего проверяемого URI</param>
      /// <returns>true, если проверка прошла успешно, false в противном случае</returns>
      public static bool CheckPureUri(string pureUriString, out string errorMessage, bool excludeCurrent = false)
      {
         try
         {
            var pureUri = new Uri(pureUriString);
            IAutopostRepository repository = new SocialAutopostRepository();
            IEnumerable<Post> allPosts = excludeCurrent
               ? repository.GetPosts().Where(post => post.PureUri != pureUriString)
               : repository.GetPosts();

            var postUris = new HashSet<string>();
            foreach (Post post in allPosts)
            {
               postUris.Add(post.PureUri);
            }

            if (postUris.Contains(pureUri.AbsoluteUri))
            {
               errorMessage = string.Format("A link {0} already exists for posting", pureUriString);
               return false;
            }

            errorMessage = string.Empty;
            return true;
         }
         catch (UriFormatException)
         {
            errorMessage = string.Format("{0} is not a valid URI", pureUriString);
            return false;
         }
      }
   }
}