﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Haker.aspx.cs" Inherits="WatchfulEye.Web.Pages.PostLevels.Haker" %>

<%@ Import Namespace="WatchfulEye.Web.Model" %>
<%@ Import Namespace="Pvs.Quiz.Common" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><%= GetDefinitionText() %></title>
</head>
<body>
    <%= GetDefinitionText() %>
    <img src="<%= CppUserLevel.Haker.GetLevelImage() %>" alt="<%= GetDefinitionText() %>" />
    <asp:PlaceHolder runat="server" ID="ClientRedirectPlaceHolder" Visible="False">
        <script type="text/javascript">
            window.location.replace('http://q.viva64.com');
        </script>
    </asp:PlaceHolder>
</body>
</html>
