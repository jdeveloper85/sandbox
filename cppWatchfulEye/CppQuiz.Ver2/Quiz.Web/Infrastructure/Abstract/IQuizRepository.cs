﻿using System;
using System.Threading.Tasks;
using Pvs.Quiz.Common;

namespace Quiz.Web.Infrastructure.Abstract
{
   public interface IQuizRepository<T> : IDisposable
   {      
      Task<T> GetQuestionAsync(int questionId);
      Task<byte[]> GetProjectLogoAsync(int questionId);
      Task<bool> UpdateQuestionStatAsync(int questionId, AnswerState answerState);
   }
}