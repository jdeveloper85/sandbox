﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pvs.Autopost.Config;
using Pvs.Autopost.Core;
using Pvs.Autoposting.Entities;

/**
 * NOTE: Предполагается, что ссылки вставлены в БД в правильном виде и соотв.
 * конструктор Uri(string) не должен выбрасывать исключений null и ошибок формата
 */

namespace FilterLinksByDomainSample
{
   internal static class Program
   {
      private static readonly double PercentDomainLimit = SocialKeysReader.PercentDomainLimit;
      private const string VivaHost = "www.viva64.com";

      private static void Main()
      {
         // Получаем все опубликованные ссылки
         IAutopostRepository repository = new SocialAutopostRepository();
         IEnumerable<Post> publishedPosts = repository.GetPublishedPosts();
         IEnumerable<Uri> postUris = repository.GetPostUris(publishedPosts);

         // Группируем опубликованные ссылки по доменам
         var notDeferredPostUris = postUris as Uri[] ?? postUris.ToArray();
         var statLinks = AutopostUtils.GetLinkStatistics(notDeferredPostUris);

         foreach (var pair in statLinks.Take(20))
         {
            Console.WriteLine(pair);
         }

         double checksum = statLinks.Sum(statLink => statLink.Percent);
         Console.WriteLine(checksum);

         bool exceeded = AutopostUtils.HasExceededDomainLimit(VivaHost, PercentDomainLimit, statLinks);
         Console.WriteLine(exceeded);
      }
   }
}