﻿//  2006-2008 (c) Viva64.com Team
//  2008-2016 (c) OOO "Program Verification Systems"
using System;
using System.Runtime.Serialization;

namespace CSharpAnalyzerTester
{
    [Serializable]
    public class InvalidSettingsException : Exception
    {
        public InvalidSettingsException()
        {
        }

        public InvalidSettingsException(string message)
            : base(message)
        {
        }

        public InvalidSettingsException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected InvalidSettingsException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}