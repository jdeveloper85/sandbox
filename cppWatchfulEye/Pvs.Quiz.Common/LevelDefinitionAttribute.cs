﻿using System;

namespace Pvs.Quiz.Common
{
   [AttributeUsage(AttributeTargets.All, Inherited = false)]
   public sealed class LevelDefinitionAttribute : Attribute
   {
      public LevelDefinitionAttribute(string definition)
      {
         Definition = definition;
      }

      public string Definition { get; private set; }
   }
}