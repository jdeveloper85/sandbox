﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Quiz.Web.Infrastructure.Abstract
{   
   public interface ICrudQuestionRepository<T> : IDisposable
   {
      Task<IEnumerable<T>> GetAllAsync();
      Task<T> GetAsync(int questionId);
      Task<T> AddAsync(T question);
      Task<bool> UpdateAsync(T question);
      Task<bool> DeleteAsync(int questionId);
   }
}