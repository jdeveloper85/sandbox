﻿using System.ServiceProcess;

namespace Pvs.Autoposting.ProducerService
{
   internal static class Program
   {
      /// <summary>
      ///    Windows-сервис, производящий сообщения для msmq
      /// </summary>
      private static void Main()
      {
         ServiceBase[] servicesToRun = { new AutoPostProducerService() };
         ServiceBase.Run(servicesToRun);
      }
   }
}