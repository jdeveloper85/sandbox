﻿/// <reference path="jquery-2.1.3.js" />
var intervalValue = 1000;
var qId = 0;
var qNumber = 0;
var secondsLeft = 0;
var timeIsOut = false;
var timeout = null;
var answered = false;
var dataIndex = -1;
var isLastQuestion = false;
var dontKnowActionText = 'Don\'t know';
var resultsText = 'Results';
var nextQuestionText = 'Next Question';
var testCompletePage = 'TestComplete.aspx';
var currentUserScore = 0;
var isCorrectCurrentAnswer = false;

// Обновление логотипа проекта
function getProjectLogo(questionId) {
    $('#projectLogo').empty();
    var imgLogo = new Image();
    imgLogo.src = 'GetLogoHandler.ashx?questionId=' + questionId;
    imgLogo.className = 'img-responsive';
    $('#projectLogo').append(imgLogo);
}

// Получение текущего вопроса
function goToNextQuestion() {
    $('#actionBtn').text(dontKnowActionText);
    $.ajax({
        url: '/quiz/question',
        type: 'GET',
        dataType: 'json',
        success: function (data) {            
            // Получаем компоненты вопроса.
            var questionNumber = data.questionNumber;
            var questionId = data.questionId;
            var codeFragment = data.codeFragment;
            var projectName = data.projectName;
            var explanatoryText = data.explanatoryText;
            var pvsStudioWarning = data.pvsStudioWarning;
            var qSecondsLeft = data.qSecondsLeft;
            var lastQuestion = data.isLastQuestion;
            var totalQuestionCount = data.totalQuestionCount;

            qId = questionId;
            qNumber = questionNumber;
            secondsLeft = qSecondsLeft;
            timeIsOut = secondsLeft <= 0;
            answered = false;
            isLastQuestion = lastQuestion;

            $('#currentQuestionNumber').html(questionNumber + ' Of ' + totalQuestionCount + ' questions');
            $('#codeFragment').html(codeFragment);
            $('#pvsStudioWarning').html(pvsStudioWarning);
            $('#projectName').html('Project name: ' + projectName);
            $('#explanatoryText').html(explanatoryText);
            hideBlocks();

            if (timeIsOut) {
                $('#countdown').html('Time is out').css('color', 'red');
            } else {
                $('#countdown').html('Seconds left: ' + secondsLeft).css('color', 'black');
                if (timeout) {
                    clearTimeout(timeout);
                    timeout = null;
                }

                timeout = setInterval(function () {
                    if (secondsLeft === 0) {
                        executeUserAction('TimeIsUp');
                        $('#countdown').html('Time is out').css('color', 'red');
                    } else {
                        secondsLeft--;
                        $('#countdown').html('Seconds left: ' + secondsLeft).css('color', 'black');
                    }
                }, intervalValue);
            }

            getProjectLogo(questionId);

            // Отправка действия при клике на фрагмент
            $('.highlight-fragment').bind('click', function () {
                dataIndex = parseInt($(this).attr('data-index'), 10);                
                executeUserAction('TryToAnswer');                
            });
        },        
        error: function () {
            window.location.replace(testCompletePage);
        }
    });
}

function clearTimer() {
    timeIsOut = true;
    if (timeout) {
        clearTimeout(timeout);
        timeout = null;
    }
}

function setActionBtnText() {
    $('#actionBtn').text(isLastQuestion ? resultsText : nextQuestionText);
}

function executeUserAction(userAction) {
    if (timeIsOut || answered)
        return;

    setActionBtnText();
    $.ajax({
        data: {
            action: userAction,
            dataIndex: dataIndex,
            questionId: qNumber
        },
        datatype: 'http',
        type: 'POST',
        url: '/quiz/stat'
    }).done(function (responseResult) {
        if (!responseResult)
            return;

        var answerState = responseResult.AnswerState;
        var answerRes = '';
        var resColor = 'white';
        if (answerState === 'Right') {
            answerRes = 'RIGHT';
            resColor = 'red';
        }
        else if (answerState === 'Wrong') {
            answerRes = 'WRONG';
            resColor = 'green';
        }
        $('#resultText').text(answerRes).css('color', resColor);

        var resDataIndex = responseResult.DataIndex;
        var correctIndices = responseResult.CorrectIndices;
        currentUserScore = responseResult.CurrentCorrectAnswersNumber;
        $('.highlight-fragment').each(function (index, element) {
            var dataIndexValue = parseInt($(element).attr('data-index'), 10);
            if (dataIndexValue === resDataIndex) {
                $(element).css('font-weight', 'bolder').css('text-decoration', 'underline');                
            }

            var isCorrectIndex = false;
            for (var i = 0; i < correctIndices.length; i++) {
                if (dataIndexValue === correctIndices[i]) {
                    isCorrectIndex = true;
                    break;
                }
            }

            if (isCorrectIndex) {
                $(element).css('color', 'red').css('font-weight', 'bolder');
            }
        });

        clearTimer();
        showBlocks();
        setActionBtnText();
        $('#currentScore').text('Current score is: ' + currentUserScore);
    });

    answered = true;
}

function hideBlocks() {
    $('#explanatoryText, #pvsStudioWarning, #currentScore, #resultText').hide();
    $('#noerrorsBtn').show();
}

function showBlocks() {
    $('#explanatoryText, #pvsStudioWarning, #currentScore, #resultText, #noerrorsBtn').show();
    $('#noerrorsBtn').hide();
}

$(document).ready(function () {
    $('#actionBtn').text(dontKnowActionText);
    $('#noerrorsBtn').text('No Errors');

    // Добавляем визуализацию при асинхронных загрузках
    var jqBody = $('body');
    $(document).on({
        ajaxStart: function () {
            jqBody.addClass('loading');
        },
        ajaxStop: function () {
            jqBody.removeClass('loading');
        }
    });

    // Получаем текущий вопрос            
    goToNextQuestion();

    // Отправка действия при клике на кнопку                
    $('#actionBtn').click(function (e) {
        switch ($('#actionBtn').text()) {
            case dontKnowActionText:
                executeUserAction('DoesNotKnow');
                break;
            case nextQuestionText:                                
                goToNextQuestion();                
                break;
            case resultsText:
                window.location.replace(testCompletePage);
                break;
        }

        if (e) {
            e.preventDefault();
        }
    });

    $('#noerrorsBtn').click(function (e) {
        executeUserAction('NoErrors');
        if (e) {
            e.preventDefault();
        }
    });
});