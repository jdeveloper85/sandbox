﻿using System.Collections.Generic;
using Pvs.Quiz.Common;

namespace WatchfulEye.Web.Model
{
    public class QuestionsRandomOrder
    {
        private readonly WatchfulEyeRepository _eyeRepository;
        private readonly Dictionary<int, int> _orderToQuestionIdMap;
        private readonly int _testQuestionsNumber;

        public QuestionsRandomOrder()
        {
            _testQuestionsNumber = WebConfigReader.TestQuestionsNumber;
            _eyeRepository = new WatchfulEyeRepository();
            _orderToQuestionIdMap = new Dictionary<int, int>(_testQuestionsNumber);
        }

        public int this[int orderIndex]
        {
            get
            {
                int idx;
                return _orderToQuestionIdMap.TryGetValue(orderIndex, out idx) ? idx : -1;
            }
        }

        public void Reinit()
        {
            var randomQuestionIds = _eyeRepository.RandomQuestionIds(_testQuestionsNumber);
            if (_orderToQuestionIdMap.Count > 0)
                _orderToQuestionIdMap.Clear();
            for (var i = 0; i < randomQuestionIds.Length; i++)
                _orderToQuestionIdMap[i + 1] = randomQuestionIds[i];
        }
    }
}