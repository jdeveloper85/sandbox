﻿using System.Linq;

namespace Pvs.Quiz.Common
{
   public static class CppUserLevelExtensions
   {
      public static string GetDefinition(this CppUserLevel aUserLevel)
      {
         return
            aUserLevel.GetType()
               .GetField(aUserLevel.ToString())
               .GetCustomAttributes(typeof(LevelDefinitionAttribute), false)
               .Cast<LevelDefinitionAttribute>()
               .First()
               .Definition;
      }

      public static string GetLevelImage(this CppUserLevel aUserLevel)
      {
         return
            aUserLevel.GetType()
               .GetField(aUserLevel.ToString())
               .GetCustomAttributes(typeof(LevelImageAttribute), false)
               .Cast<LevelImageAttribute>()
               .First()
               .ImagePath;
      }
   }
}