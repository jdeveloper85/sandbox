﻿namespace Pvs.Autopost.Config
{
   /// <summary>
   /// Сообщение электронной почты
   /// </summary>
   public struct EmailMessage
   {
      public string MessageSubject { get; set; }

      public string MessageBody { get; set; }
   }
}