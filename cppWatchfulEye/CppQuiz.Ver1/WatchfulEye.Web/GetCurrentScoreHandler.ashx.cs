﻿using System.Web;
using System.Web.SessionState;
using WatchfulEye.Web.Pages.Helpers;

namespace WatchfulEye.Web
{
    /// <summary>
    ///    HttpHandler для получения текущего результата теста
    /// </summary>
    public class GetCurrentScoreHandler : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (context.Session == null)
            {
                context.Response.Write("0");
                return;
            }

            var answersKeeper = SessionHelper.GetAnswersKeeper(context.Session);
            context.Response.Write(answersKeeper.TotalRight);
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}