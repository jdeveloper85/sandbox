﻿using System;
using System.Web.UI;
using Pvs.Quiz.Common;
using WatchfulEye.Web.Pages.Helpers;

namespace WatchfulEye.Web.Pages
{
    public partial class Start : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AnswersKeeper answersKeeper = SessionHelper.GetAnswersKeeper(Session);
            var randomOrder = SessionHelper.GetQuestionsRandomOrder(Session);
            randomOrder.Reinit();
            answersKeeper.Clear();
        }
    }
}