namespace Pvs.MailGenerator.Common
{
   public class ContactImpl : IContact
   {
      public string Name { get; set; }
      public string Mail { get; set; }
      public string LinkText { get; set; }
      public string LinkUrl { get; set; }
      public string BitlyLink { get; set; }

      public override string ToString()
      {
         return string.Format("Name: {0}, Mail: {1}, LinkText: {2}, LinkUrl: {3}", Name, Mail, LinkText, LinkUrl);
      }
   }
}