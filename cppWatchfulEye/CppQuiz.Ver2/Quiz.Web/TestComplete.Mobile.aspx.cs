﻿using System;
using System.Web.UI;

namespace Quiz.Web
{
   public partial class TestComplete_Mobile : Page
   {
      protected void Page_Load(object sender, EventArgs e)
      {
         if (IsPostBack)
            return;

         var userScoreParam = Request.QueryString["userScore"];
         int userScore;
         if (!string.IsNullOrEmpty(userScoreParam) && int.TryParse(userScoreParam, out userScore))
         {
            TestResultLabel.Text = string.Format("Number of correct answers - {0}", userScore);
         }
      }
   }
}