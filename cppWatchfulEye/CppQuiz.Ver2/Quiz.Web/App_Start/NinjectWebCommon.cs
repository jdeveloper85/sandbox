using System;
using System.Web;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Ninject.WebApi.DependencyResolver;
using Quiz.Web;
using WebActivator;
using System.Web.Http;

[assembly: WebActivator.PreApplicationStartMethod(typeof (NinjectWebCommon), "Start")]
[assembly: ApplicationShutdownMethod(typeof (NinjectWebCommon), "Stop")]

namespace Quiz.Web
{
   public static class NinjectWebCommon
   {
      private static readonly Bootstrapper DefaultBootstrapper = new Bootstrapper();

      public static void Start()
      {
         DynamicModuleUtility.RegisterModule(typeof (OnePerRequestHttpModule));
         DynamicModuleUtility.RegisterModule(typeof (NinjectHttpModule));
         DefaultBootstrapper.Initialize(CreateKernel);
      }

      public static void Stop()
      {
         DefaultBootstrapper.ShutDown();
      }

      private static IKernel CreateKernel()
      {
         var kernel = new StandardKernel();
         try
         {
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
            GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);

            RegisterServices(kernel);
            return kernel;
         }
         catch
         {
            kernel.Dispose();
            throw;
         }
      }

      private static void RegisterServices(IKernel kernel)
      {
         IoCConfiguration.SetupDi(kernel);
      }
   }
}