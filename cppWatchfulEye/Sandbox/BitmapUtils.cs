﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace InsertLogoSandbox
{
    public static class BitmapUtils
    {
        private const int DefaultMemoryStreamCapacity = 0x400000;

        public static byte[] GetImageData(string anImagePath)
        {
            if (string.IsNullOrEmpty(anImagePath) || !File.Exists(anImagePath))
                return null;

            try
            {
                using (Stream imageStream = File.OpenRead(anImagePath))
                using (var imageBitMap = new Bitmap(imageStream))
                using (var imageMemoryStream = new MemoryStream(DefaultMemoryStreamCapacity))
                {
                    imageBitMap.Save(imageMemoryStream, ImageFormat.Png);
                    return imageMemoryStream.ToArray();
                }
            }
            catch (ArgumentException)
            {
                return null;
            }
        }
    }
}