﻿using System.ComponentModel.DataAnnotations;

namespace WatchfulEye.DataLayer
{
    [MetadataType(typeof(QuestionMetadata))]
    public partial class Question
    {
        public static Question EmptyQuestion = new Question
        {
            CodeFragment = "No Fragment",
            ExplanatoryText = "No Explanatory Text",
            ProjectLogo = null,
            ProjectName = "No Project Name",
            PvsStudioWarning = "No warnings"
        };
    }
}