﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Neutral.aspx.cs" Inherits="WatchfulEye.Web.Pages.PostLevels.Neutral" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Test your C++ skills - find bugs in popular open-source projects!</title>
</head>
<body>
    Test your C++ skills - find bugs in popular open-source projects!
    <img src="../../Images/unicorn.png" alt="Test your C++ skills - find bugs in popular open-source projects!" />
    <asp:PlaceHolder runat="server" ID="ClientRedirectPlaceHolder" Visible="False">
        <script type="text/javascript">
            window.location.replace('http://q.viva64.com');
        </script>
    </asp:PlaceHolder>
</body>
</html>
