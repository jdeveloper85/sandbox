﻿using System.ServiceProcess;

namespace Pvs.Pastebin.CollectorService
{
   internal static class Program
   {      
      private static void Main()
      {
         var servicesToRun = new ServiceBase[] { new PastebinCollectorService() };
         ServiceBase.Run(servicesToRun);
      }
   }
}