USE [SocialAutopost]
GO
 
IF EXISTS(
       SELECT *
       FROM   INFORMATION_SCHEMA.TABLES
       WHERE  TABLE_NAME = 'Post'
   )
    DROP TABLE Post
GO
    
CREATE TABLE Post
(
	PostId                  INT IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	PureUri                 VARCHAR(256) NOT NULL,
	PostText                VARCHAR(4096) NOT NULL,
	CreatedDate             DATETIME NOT NULL,
	SocialBits              TINYINT NOT NULL DEFAULT 0,
	IsPriority              BIT NOT NULL DEFAULT 0,
	IsPending               BIT NOT NULL DEFAULT 0,
	IsPosted                BIT NOT NULL DEFAULT 0,
	IsResourceAvailable     BIT NULL,
	PostedDate              DATETIME NULL,
	ErrorMessage            VARCHAR(1024) NULL
)
GO

ALTER TABLE Post
ADD CONSTRAINT Post_PureUri_Unique UNIQUE(PureUri)
GO

/* ALTER TABLE Post
ADD CONSTRAINT Post_Default_Now_Date
DEFAULT GETDATE() FOR CreatedDate
GO */