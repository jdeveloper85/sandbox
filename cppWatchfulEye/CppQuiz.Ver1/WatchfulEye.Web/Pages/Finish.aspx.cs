﻿using System;
using System.Web;
using System.Web.UI;
using Pvs.Quiz.Common;
using WatchfulEye.Web.Pages.Helpers;

namespace WatchfulEye.Web.Pages
{
    public partial class Finish : Page
    {
        protected UserLevelGradation Gradation
        {
            get { return (UserLevelGradation)ViewState["Gradation"]; }
            private set { ViewState["Gradation"] = value; }
        }

        protected string GetPostLevelUrl
        {
            get
            {
                HttpRequest currentRequest = HttpContext.Current.Request;
                string rootUrl = currentRequest.Url.AbsoluteUri.Replace(currentRequest.Url.AbsolutePath, string.Empty);
                string gradationName = Enum.GetName(typeof(CppUserLevel), Gradation.Level);
                return string.Format("{0}/Pages/PostLevels/{1}.aspx", rootUrl, gradationName);
            }
        }

        protected string Definition
        {
            get { return string.Format("PVS-Studio had measured my C++ skill as: {0}", Gradation.Level.GetDefinition()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            AnswersKeeper answersKeeper = SessionHelper.GetAnswersKeeper(Session);
            int totalRight = answersKeeper.TotalRight;
            int total = WebConfigReader.TestQuestionsNumber;
            SetLevel(totalRight, total);
            TestResultLabel.Text = string.Format("{0} error{1} out of {2}", totalRight,
               totalRight == 1 ? string.Empty : "s", total);
            if (Master != null)
            {
                Control topSocialContent = Master.FindControl("SocialContent");
                if (topSocialContent != null)
                    topSocialContent.Visible = false;
            }

            if (answersKeeper.Count != WebConfigReader.TestQuestionsNumber) // Пользователь не отвечал на все вопросы        
                Response.RedirectToRoute("StartRoute");
        }

        private void SetLevel(int totalRight, int total)
        {
            var rightPercentage = (int)(Math.Ceiling((totalRight / (double)total) * 100));
            Gradation = new UserLevelGradation(rightPercentage);
        }

        protected static string GetDefinition(CppUserLevel userLevel)
        {
            return string.Format("{0}. {1}", (int)userLevel, userLevel.GetDefinition());
        }
    }
}