namespace VkApi.Library
{
   public struct VkSettings
   {
      public VkSettings(int appId, string login, string password, string groupName, int groupId, int docLimit)
         : this()
      {
         AppId = appId;
         Login = login;
         Password = password;
         GroupName = groupName;
         GroupId = groupId;
         DocumentLimit = docLimit;
      }

      public int AppId { get; private set; }

      public string Login { get; private set; }

      public string Password { get; private set; }

      public string GroupName { get; private set; }

      public int GroupId { get; private set; }

      public int DocumentLimit { get; private set; }
   }
}