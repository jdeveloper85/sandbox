﻿namespace Pvs.Autopost.Config
{
   public struct PastebinConfiguration
   {
      public string WebSiteUrl { get; set; }

      public string DefaultEncoding { get; set; }

      public int MinimumPastedDataLength { get; set; }
   }
}