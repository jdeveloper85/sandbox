﻿using System.ComponentModel.DataAnnotations;

namespace WatchfulEye.DataLayer
{
    /// <summary>
    /// Метаданные для вопроса теста
    /// </summary>
    public class QuestionMetadata
    {
        [Required(ErrorMessage = "Code fragment must be filled")]
        public string CodeFragment { get; set; }

        [Required(ErrorMessage = "Project name must be filled")]
        public string ProjectName { get; set; }

        [Required(ErrorMessage = "Explanatory text must be filled")]
        public string ExplanatoryText { get; set; }

        [Required(ErrorMessage = "PVS-Studio Warning must be filled")]
        public string PvsStudioWarning { get; set; }
    }
}