﻿using System;
using Pvs.Autopost.Config;
using TweetSharp;

namespace TwitterSample
{
   internal static class Program
   {
      private static void Main()
      {
         string consumerKey = SocialKeysReader.Factory.TwitterConfig.ConsumerKey;
         string consumerSecret = SocialKeysReader.Factory.TwitterConfig.ConsumerSecret;
         string accessToken = SocialKeysReader.Factory.TwitterConfig.AccessToken;
         string accessTokenSecret = SocialKeysReader.Factory.TwitterConfig.AccessTokenSecret;

         var twitterService = new TwitterService(consumerKey, consumerSecret);
         twitterService.AuthenticateWith(accessToken, accessTokenSecret);
         var tweetOptions = new SendTweetOptions
         {
            Status = "Tweeting with http://www.viva64.com/ru/pvs-studio TweetSharp"
         };

         TwitterStatus twitterStatus = twitterService.SendTweet(tweetOptions);
         if (twitterStatus != null)
         {
            Console.WriteLine(twitterStatus.Author);
         }

         Console.ReadKey();
      }
   }
}