﻿using System.Net;
using System.Net.Mail;

namespace Pvs.Autopost.Config
{
   /// <summary>
   ///    Оболочка для отправки писем
   /// </summary>
   public sealed class SendEmailWrapper
   {
      private readonly EmailConfiguration _emailConfig;

      /// <summary>
      /// К-тор отправки писем
      /// </summary>
      /// <param name="emailConfig">Конфигурация почты</param>
      public SendEmailWrapper(EmailConfiguration emailConfig)
      {
         _emailConfig = emailConfig;
      }

      /// <summary>
      /// Отправка письма по почте
      /// </summary>
      /// <param name="emailMessage">Сообщение</param>
      /// <param name="recipients">Список получателей</param>
      public void Send(EmailMessage emailMessage, params EmailRecipient[] recipients)
      {
         foreach (EmailRecipient recipient in recipients)
         {
            InternalSendEmail(emailMessage, recipient);
         }
      }

      private void InternalSendEmail(EmailMessage emailMessage, EmailRecipient recipient)
      {
         var fromAddress = new MailAddress(_emailConfig.FromAddress, _emailConfig.FromDisplayName);
         var toAddress = new MailAddress(recipient.ToAddress, recipient.ToDisplayName);
         var smtpClient = new SmtpClient
         {
            Host = _emailConfig.SmtpServerName,
            Port = _emailConfig.SmtpPort,
            EnableSsl = false,
            DeliveryMethod = SmtpDeliveryMethod.Network,
            UseDefaultCredentials = false,
            Credentials = new NetworkCredential(_emailConfig.MailLogin, _emailConfig.MailPassword)
         };

         using (
            var message = new MailMessage(fromAddress, toAddress)
            {
               Subject = emailMessage.MessageSubject,
               Body = emailMessage.MessageBody
            })
         {
            smtpClient.Send(message);
         }
      }
   }
}