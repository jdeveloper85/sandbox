﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternalServerError.aspx.cs"
Inherits="WatchfulEye.Web.Error.InternalServerError" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
<form id="errorForm" runat="server">
    На сервере произошла ошибка.<br/>
    <fieldset>
        <legend>Информация об ошибке</legend>
        <pre>
            <asp:Label ID="ErrorLabel" Text="" runat="server" />
        </pre>
    </fieldset>
</form>
</body>
</html>