﻿using System;

namespace Pvs.Quiz.Common
{
   [Serializable]
   public class UserLevelGradation
   {
      public UserLevelGradation(int level)
      {
         if (level <= 20) Level = CppUserLevel.Dummy;
         else if (level > 20 && level <= 40) Level = CppUserLevel.Beginner;
         else if (level > 40 && level <= 60) Level = CppUserLevel.Medium;
         else if (level > 60 && level <= 80) Level = CppUserLevel.Advanced;
         else if (level > 80 && level < 100) Level = CppUserLevel.Pro;
         else Level = CppUserLevel.Haker;
      }

      public CppUserLevel Level { get; private set; }
   }
}