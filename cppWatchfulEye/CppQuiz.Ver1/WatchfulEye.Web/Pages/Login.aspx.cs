﻿using System;
using System.Web.Security;
using System.Web.UI;

namespace WatchfulEye.Web.Pages
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                string userName = Request.Form["name"];
                string userPass = Request.Form["password"];
#pragma warning disable 618
                if (userName != null && userPass != null && FormsAuthentication.Authenticate(userName, userPass))
#pragma warning restore 618
                {
                    FormsAuthentication.SetAuthCookie(userName, false);
                    Response.Redirect(Request["ReturnUrl"] ?? "/");
                }
                else
                {
                    ModelState.AddModelError("Fail", "Login failed. Please try again");
                }
            }
        }
    }
}