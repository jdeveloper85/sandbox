﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using Pvs.Autopost.Core;

namespace Pvs.Autoposting.Entities
{
   /// <summary>
   ///    Реализация интерфейса над хранилищем для ORM ADO.NET EF 5
   /// </summary>
   public class SocialAutopostRepository : IAutopostRepository
   {
      private readonly ILog _autopostLogger;

      public SocialAutopostRepository()
      {
         _autopostLogger = null;
      }

      /// <summary>
      ///    Конструктор хранилища
      /// </summary>
      /// <param name="autopostLogger">Логгер</param>
      public SocialAutopostRepository(ILog autopostLogger)
      {
         _autopostLogger = autopostLogger;
      }

      /// <summary>
      ///    Получение новых постов для размещения
      /// </summary>
      /// <param name="takeLimit">Кол-во постов</param>
      /// <returns>Новые посты</returns>
      public IEnumerable<Post> GetNewPosts(int takeLimit)
      {
         using (var autopostEntities = new SocialAutopostEntities())
         {
            Post[] newPosts = (from post in autopostEntities.Posts
                               where !post.IsPending && !post.IsPosted && post.IsResourceAvailable == null
                               orderby post.IsPriority descending, post.CreatedDate
                               select post).ToArray();

            return newPosts.Take(takeLimit);
         }
      }

      /// <summary>
      ///    Получение всех постов
      /// </summary>
      /// <returns>Все посты</returns>
      public IEnumerable<Post> GetPublishedPosts()
      {
         using (var autopostEntities = new SocialAutopostEntities())
         {
            return (from post in autopostEntities.Posts
                    where post.IsPending && post.IsPosted
                    select post).ToArray();
         }
      }

      /// <summary>
      ///    Установка постов в режим готовности для размещения
      /// </summary>
      /// <param name="postIds">Идентификаторы постов для размещения</param>
      /// <returns>Кол-во сохраненных изменений</returns>
      public void SetInPending(IEnumerable<int> postIds)
      {
         using (var autopostEntities = new SocialAutopostEntities())
         {
            try
            {
               foreach (int postId in postIds)
               {
                  Post post = autopostEntities.Posts.Find(postId);
                  if (post != null && !post.IsPending)
                  {
                     post.IsPending = true;
                     post.IsResourceAvailable = true;
                  }
               }

               autopostEntities.SaveChanges();
            }
            catch (InvalidOperationException invalidOpEx)
            {
               if (_autopostLogger != null)
               {
                  _autopostLogger.Error(invalidOpEx.Message, invalidOpEx);
               }
            }
         }
      }

      /// <summary>
      ///    Получение URI для постов
      /// </summary>
      /// <param name="posts">Посты</param>
      /// <returns>Набор URI для постов</returns>
      public IEnumerable<Uri> GetPostUris(IEnumerable<Post> posts)
      {
         var uris = new List<Uri>();
         foreach (
            string pureUri in posts.Where(post => !string.IsNullOrEmpty(post.PureUri)).Select(post => post.PureUri))
         {
            try
            {
               var postUri = new Uri(pureUri);
               uris.Add(postUri);
            }
            catch (UriFormatException uriFormatEx)
            {
               if (_autopostLogger != null)
               {
                  _autopostLogger.Error(string.Format("{0} is in bad format for URI", pureUri), uriFormatEx);
               }
            }
         }

         return uris;
      }

      /// <summary>
      ///    Установка поста как размещенный
      /// </summary>
      /// <param name="postId">Id-поста</param>
      /// <returns>true, если пост успешно сохранен в базе как размещенный, false - в противном случае</returns>
      /// <exception cref="PostFailException">Если пост не удалось пометить как размещенный</exception>
      public void SetInPublish(int postId)
      {
         using (var autopostEntities = new SocialAutopostEntities())
         {
            try
            {
               Post post = autopostEntities.Posts.Find(postId);
               if (post != null && post.IsPending && !post.IsPosted)
               {
                  post.IsPosted = true;
                  post.PostedDate = DateTime.Now;
                  autopostEntities.SaveChanges();
               }
            }
            catch (InvalidOperationException invalidOpEx)
            {
               throw new PostFailException(string.Format("Post {0} failed", postId), invalidOpEx);
            }
         }
      }

      /// <summary>
      ///    Получить первый пост, пригодный для размещения
      /// </summary>
      /// <returns>Первый пост, пригодный для размещения</returns>
      public Post[] GetPostsToPublish()
      {
         using (var autopostEntities = new SocialAutopostEntities())
         {
            return (from post in autopostEntities.Posts
                    where post.IsPending && !post.IsPosted && post.ErrorMessage == null && post.IsResourceAvailable == true
                    orderby post.IsPriority descending, post.CreatedDate
                    select post).ToArray();
         }
      }

      /// <summary>
      ///    Получение всех постов
      /// </summary>
      public IEnumerable<Post> GetPosts()
      {
         using (var autopostEntities = new SocialAutopostEntities())
         {
            return (from post in autopostEntities.Posts
                    select post).ToArray();
         }
      }

      /// <summary>
      ///    Вставка или обновление поста
      /// </summary>
      /// <param name="post">Пост</param>
      /// <exception cref="InvalidOperationException">При ненормальных манипуляциях с СУБД</exception>
      public void Save(Post post)
      {
         using (var autopostEntities = new SocialAutopostEntities())
         {
            if (post.PostId == 0)
            {
               autopostEntities.Posts.Add(post);
            }
            else
            {
               Post foundPost = autopostEntities.Posts.Find(post.PostId);
               if (foundPost != null)
               {
                  if (string.Compare(foundPost.PureUri, post.PureUri, StringComparison.CurrentCultureIgnoreCase) != 0)
                     foundPost.PureUri = post.PureUri;

                  foundPost.PostText = post.PostText;
                  foundPost.CreatedDate = post.CreatedDate;
                  foundPost.SocialBits = post.SocialBits;
                  foundPost.IsPriority = post.IsPriority;
                  foundPost.IsPending = post.IsPending;
                  foundPost.IsPosted = post.IsPosted;
                  foundPost.IsResourceAvailable = post.IsResourceAvailable;
                  foundPost.PostedDate = post.PostedDate;
                  foundPost.ErrorMessage = post.ErrorMessage;
               }
            }

            autopostEntities.SaveChanges();
         }
      }

      /// <summary>
      ///    Удаление поста
      /// </summary>
      /// <param name="aPost">Пост для удаления</param>
      public void Delete(Post aPost)
      {
         using (var autopostEntities = new SocialAutopostEntities())
         {
            Post postToDelete = autopostEntities.Posts.Find(aPost.PostId);
            if (postToDelete != null)
               autopostEntities.Posts.Remove(postToDelete);

            autopostEntities.SaveChanges();
         }
      }

      /// <summary>
      /// Установка ресурса для публикации как недоступный
      /// </summary>
      /// <param name="postId">PostId</param>
      public void SetPostUnavailable(int postId)
      {
         using (var autopostEntities = new SocialAutopostEntities())
         {
            try
            {
               Post post = autopostEntities.Posts.Find(postId);
               if (post != null)
               {
                  post.IsResourceAvailable = false;
                  autopostEntities.SaveChanges();
               }
            }
            catch (InvalidOperationException invalidOperationEx)
            {
               throw new PostFailException(string.Format("Post {0} failed", postId), invalidOperationEx);
            }
         }
      }
   }
}