USE [1gb_x_email20b]
GO
/****** Object:  DatabaseRole [aspnet_Personalization_FullAccess]    Script Date: 19.02.2015 15:21:39 ******/
CREATE ROLE [aspnet_Personalization_FullAccess]
GO
ALTER ROLE [aspnet_Personalization_ReportingAccess] ADD MEMBER [aspnet_Personalization_FullAccess]
GO
ALTER ROLE [aspnet_Personalization_BasicAccess] ADD MEMBER [aspnet_Personalization_FullAccess]
GO
