﻿namespace WatchfulEye.Web.Pages.Helpers
{
    /// <summary>
    /// Ключи сеанса
    /// </summary>
    public enum SessionKey
    {
        /// <summary>
        /// Ключ для хранения состояния ответов на вопросы
        /// </summary>
        AnswerKeeper,

        /// <summary>
        /// Ключ для хранения состояния случайного порядка вопросов
        /// </summary>
        QuestionsRandomOrder
    }
}