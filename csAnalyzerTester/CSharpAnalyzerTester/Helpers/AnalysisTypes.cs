﻿//  2006-2008 (c) Viva64.com Team
//  2008-2016 (c) OOO "Program Verification Systems"
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CSharpAnalyzerTester.Env;
using Diff = ProgramVerificationSystems.PVSStudio.PVSDiff;
using Res = CSharpAnalyzerTester.Properties.Resources;
using ProgramVerificationSystems.PVSStudio.CommonTypes;

namespace CSharpAnalyzerTester.Helpers
{
    public sealed class AnalysisManager : IExecutable<IList<TestProjectSummary>>
    {
        private const string BatchOpenBatLabel = "Batch_Open.bat";
        private const string BatchApproveBatLabel = "Batch_Approve.bat";
        private const string ApproveDiffsSuffix = "_Approve_Diffs";
        private const string BatExt = ".bat";
        private const string OpenDiffsSuffix = "_Open_Diffs";
        private readonly int _concurrencyLevel;
        private readonly IEnv _env;
        private readonly bool _genEtalon;
        private readonly IReadOnlyList<TestProject> _projects;
        private IList<TestProjectSummary> _summaries;

        public AnalysisManager(IReadOnlyList<TestProject> projects, bool genEtalon, IEnv testEnv, int concurrencyLevel)
        {
            _projects = projects;
            _genEtalon = genEtalon;
            _concurrencyLevel = concurrencyLevel;
            _env = testEnv;
            InitTestSession();
        }

        public IList<TestProjectSummary> Execute()
        {
            var summaries = new ConcurrentQueue<TestProjectSummary>();
            var factory = new TaskFactory(new LimitedConcurrencyLevelTaskScheduler(_concurrencyLevel));

            try
            {
                Task.WaitAll(_projects.Select(project => factory.StartNew(() =>
                {
                    TestProjectSummary summary;
                    using (IExecutable<TestProjectSummary> summaryItem = new AnalysisWorkItem(_env, project, _genEtalon))
                    {
                        summary = AnalyzeItem(summaryItem);
                    }

                    if (summary.Status == AnalysisStatus.Diff)
                        CreateBatFiles(project, _env);

                    summaries.Enqueue(summary);
                })).ToArray());
            }
            catch (AggregateException aggrEx)
            {
                OnError(new ErrorEventArgs(aggrEx.GetBaseException()));
            }

            _summaries = summaries.ToArray();
            return _summaries;
        }

        private static void CreateBatFiles(TestProject project, IEnv anEnv)
        {
            var solutionName = project.GetSolutionName();
            var approveDiffBat = Path.Combine(
               anEnv.ApproveDiffsDir, string.Format("{0}{1}{2}", solutionName, ApproveDiffsSuffix, BatExt));
            var openDiffBat = Path.Combine(
               anEnv.LogsDiffDirectory, string.Format("{0}{1}{2}", solutionName, OpenDiffsSuffix, BatExt));
            BatchHelper.WriteBatForApproveEtalon(approveDiffBat, solutionName.GetLogFileName(anEnv));
            BatchHelper.WriteBatForRunDevenv(openDiffBat, solutionName.GetLogFileNameDiffs(anEnv));
        }

        public void Dispose()
        {
            if (_summaries == null)
                return;

            var diffProjects =
                (from summary in _summaries where summary.Status == AnalysisStatus.Diff select summary.TestProject)
                    .ToList();

            var approveBatchCommands = diffProjects.Aggregate(string.Empty,
                (current, project) =>
                {
                    var solutionName = project.GetSolutionName();
                    var approveDiffBat = Path.Combine(
                        _env.ApproveDiffsDir, string.Format("{0}{1}{2}", solutionName, ApproveDiffsSuffix, BatExt));
                    var approveLines = File.ReadAllLines(approveDiffBat);
                    var line = approveLines.Aggregate(string.Empty,
                        (currentCommand, batCommand) => currentCommand + (batCommand + Environment.NewLine));
                    return current + line;
                });
            var openBatchCommands = diffProjects.Aggregate(string.Empty,
                (current, project) =>
                {
                    var solutionName = project.GetSolutionName();
                    var openDiffBat = Path.Combine(
                        _env.LogsDiffDirectory, string.Format("{0}{1}{2}", solutionName, OpenDiffsSuffix, BatExt));
                    var openLines = File.ReadAllLines(openDiffBat);
                    var line = openLines.Aggregate(string.Empty,
                        (currentCommand, batCommand) => currentCommand + (batCommand + Environment.NewLine));
                    return current + line;
                });

            if (!string.IsNullOrEmpty(approveBatchCommands))
                File.WriteAllText(
                    Path.Combine(_env.ApproveDiffsDir, BatchApproveBatLabel), approveBatchCommands);

            if (!string.IsNullOrEmpty(openBatchCommands))
                File.WriteAllText(
                    Path.Combine(_env.LogsDiffDirectory, BatchOpenBatLabel), openBatchCommands);
        }

        public event EventHandler<ExecutedEventArgs> Executed;
        public event EventHandler<ErrorEventArgs> Error;
        public event EventHandler<DisposeEventArgs> Disposed;

        private TestProjectSummary AnalyzeItem(IExecutable<TestProjectSummary> workItem)
        {
            workItem.Executed += (sender, args) => OnExecuted(args);
            workItem.Error += (sender, args) => OnError(args);
            workItem.Disposed += (sender, args) => OnDisposed(args);
            var summary = workItem.Execute();

            return summary;
        }

        private void InitTestSession()
        {
            if (!Directory.Exists(_env.LogsDiffDirectory))
                Directory.CreateDirectory(_env.LogsDiffDirectory);
            if (!Directory.Exists(_env.ApproveDiffsDir))
                Directory.CreateDirectory(_env.ApproveDiffsDir);            
        }

        private void OnExecuted(ExecutedEventArgs e)
        {
            e.Raise(this, ref Executed);
        }

        private void OnError(ErrorEventArgs e)
        {
            e.Raise(this, ref Error);
        }

        private void OnDisposed(DisposeEventArgs e)
        {
            e.Raise(this, ref Disposed);
        }        
    }

    public sealed class DisposeEventArgs : EventArgs
    {
        public TestProjectSummary ProjectSummary { get; private set; }
        public RunResult Result { get; private set; }

        public DisposeEventArgs(TestProjectSummary projectSummary, RunResult result)
        {
            ProjectSummary = projectSummary;
            Result = result;
        }
    }

    public sealed class AnalysisWorkItem : IExecutable<TestProjectSummary>
    {
        private readonly IEnv _env;
        private readonly bool _genEtalon;
        private readonly TestProject _project;
        private TestProjectSummary _projectSummary;
        private RunResult _result;

        public AnalysisWorkItem(IEnv env, TestProject project, bool genEtalon)
        {
            _env = env;
            _project = project;
            _genEtalon = genEtalon;
        }

        public void Dispose()
        {
            OnDisposed(new DisposeEventArgs(_projectSummary, _result));
        }

        public TestProjectSummary Execute()
        {            
            try
            {
                var solutionName = _project.GetSolutionName();
                var etalonLog = solutionName.GetEtalonLog(_env);
                var logFileName = solutionName.GetLogFileName(_env);
                var logFileNameDiffs = solutionName.GetLogFileNameDiffs(_env);

                _projectSummary = new TestProjectSummary
                {
                    TestProject = _project,
                    SolutionName = solutionName,
                    SavedLogPath = logFileName,
                    EtalonLogPath = etalonLog
                };

                TryDelete(etalonLog);
                AutoStopwatch autoStopwatch;
                var targetLog = _genEtalon ? etalonLog : logFileName;
                using (autoStopwatch = new AutoStopwatch())
                {
                    _result = _project.Run(_env, targetLog);
                }

                BatchHelper.CreateRestartBatScript(_project, solutionName, _env.LogsDirectory);
                var elapsed = _projectSummary.Elapsed = autoStopwatch.Elapsed;
                var analysisTime = elapsed.ToAbsoluteFormat();
                _projectSummary.StartTime = autoStopwatch.StartTime;
                _projectSummary.EndTime = autoStopwatch.EndTime;
                if (!_result.Success)
                {
                    _result.OutputData.AppendLine(Res.AnalysisProcessFail);
                    _projectSummary.Status = _result.IsTimeout ? AnalysisStatus.Timeout : AnalysisStatus.Fail;
                    return _projectSummary;
                }

                if (_result.ExitCode != 0 && _result.ExitCode != 7)
                {
                    var logMessage = string.Format(Res.AnalyzerCrashed, _result.ExitCode);
                    _result.OutputData.Append(logMessage);
                    _projectSummary.Status = _result.IsTimeout ? AnalysisStatus.Timeout : AnalysisStatus.Fail;
                    OnExecuted(
                        new ExecutedEventArgs(solutionName, analysisTime, _projectSummary.Status));
                    return _projectSummary;
                }

                var analysisPlogNotFound = Res.AnalysisPlogNotFound;
                if (!File.Exists(targetLog))
                {
                    _projectSummary.Status = AnalysisStatus.Fail;
                    var message = string.Format(analysisPlogNotFound, _result.ExitCode, targetLog);
                    _result.OutputData.AppendLine(message);
                    OnExecuted(
                        new ExecutedEventArgs(solutionName, analysisTime, _projectSummary.Status));
                    return _projectSummary;
                }

                var comparerReports = new Diff();
                if (!_genEtalon)
                {
                    if (!File.Exists(etalonLog))
                        etalonLog = Diff.CreateDummyEtalon(logFileName);

                    var diff = comparerReports.GetDiff(etalonLog, logFileName);
                    if (!comparerReports.Output.Succeeded)
                    {
                        FetchMessages(_projectSummary, comparerReports);
                        diff.WriteXml(logFileNameDiffs, XmlWriteMode.WriteSchema);
                    }

                    _projectSummary.Status = comparerReports.Output.Succeeded
                        ? AnalysisStatus.Ok
                        : AnalysisStatus.Diff;
                }
                else
                {
                    _projectSummary.Status = AnalysisStatus.Etalon;
                }

                OnExecuted(
                    new ExecutedEventArgs(solutionName, analysisTime, _projectSummary.Status));
            }
            catch (Exception ex)
            {
                OnError(new ErrorEventArgs(ex));
            }

            return _projectSummary;
        }

        public event EventHandler<ExecutedEventArgs> Executed;
        public event EventHandler<ErrorEventArgs> Error;
        public event EventHandler<DisposeEventArgs> Disposed;

        private void TryDelete(string etalonLog)
        {
            if (!_genEtalon)
                return;

            try
            {
                File.Delete(etalonLog);
            }
            catch
            {
                // note: ignored
            }
        }

        private static void FetchMessages(TestProjectSummary projectSummary, Diff comparerReports)
        {
            projectSummary.MissingMessages =
                comparerReports.Output.Missings.Rows.Cast<DataRow>().Select(row => row.ToMessage());
            projectSummary.AdditionalMessages =
                comparerReports.Output.Additionals.Rows.Cast<DataRow>().Select(row => row.ToMessage());
            projectSummary.ModifiedMessages =
                comparerReports.Output.Modifies.Rows.Cast<DataRow>().Select(row => row.ToMessage());
        }

        private void OnExecuted(ExecutedEventArgs e)
        {
            e.Raise(this, ref Executed);
        }

        private void OnError(ErrorEventArgs e)
        {
            e.Raise(this, ref Error);            
        }

        private void OnDisposed(DisposeEventArgs e)
        {
            e.Raise(this, ref Disposed);            
        }        
    }

    public sealed class ExecutedEventArgs : EventArgs
    {
        public ExecutedEventArgs(string solutionName, string analysisTime, AnalysisStatus status)
        {
            SolutionName = solutionName;
            AnalysisTime = analysisTime;
            Status = status;
        }

        public string SolutionName { get; private set; }
        public string AnalysisTime { get; private set; }
        public AnalysisStatus Status { get; private set; }
    }

    public sealed class ErrorEventArgs : EventArgs
    {
        public ErrorEventArgs(Exception error)
        {
            Error = error;
        }

        public Exception Error { get; private set; }
    }

    public interface IExecutable<out T> : IDisposable
    {
        T Execute();
        event EventHandler<ExecutedEventArgs> Executed;
        event EventHandler<ErrorEventArgs> Error;
        event EventHandler<DisposeEventArgs> Disposed;
    }

    public static class StringFilenamesExtensions
    {
        private const string DefaultPlogExt = ".plog";
        private const string DiffsSuffix = "_Diffs";

        public static string GetEtalonLog(this string aSolutionName, IEnv anEnv)
        {
            return string.Format("{0}{1}{2}", anEnv.EtalonLogDirectory, aSolutionName, DefaultPlogExt);
        }

        public static string GetLogFileName(this string aSolutionName, IEnv anEnv)
        {
            return string.Format("{0}{1}{2}", anEnv.LogsDirectory, aSolutionName, DefaultPlogExt);
        }

        public static string GetLogFileName(this string aSolutionName, string aTargetSessionFolder)
        {
            return Path.Combine(aTargetSessionFolder, string.Format("{0}{1}", aSolutionName, DefaultPlogExt));
        }

        public static string GetLogFileNameDiffs(this string aSolutionName, IEnv anEnv)
        {
            return string.Format("{0}{1}{2}{3}", anEnv.LogsDirectory, aSolutionName, DiffsSuffix, DefaultPlogExt);
        }

        public static string GetLogFileNameDiffs(this string aSolutionName, string aTargetSessionFolder)
        {
            return Path.Combine(aTargetSessionFolder,
                string.Format("{0}{1}{2}", aSolutionName, DiffsSuffix, DefaultPlogExt));
        }
    }

    public static class EventArgsExtensions
    {
        public static void Raise<TEventArgs>(this TEventArgs e, object sender, ref EventHandler<TEventArgs> handler)
            where TEventArgs : EventArgs
        {
            var tmpHandler = Interlocked.CompareExchange(ref handler, null, null);
            if (tmpHandler != null)
                tmpHandler(sender, e);
        }
    }

    public sealed class ConsoleHighlightWrapper : IDisposable
    {
        private readonly ConsoleColor _sourceColor;
        private static readonly object ForegroundMonitorObj = new object();

        public ConsoleHighlightWrapper(AnalysisStatus status)
        {
            Monitor.Enter(ForegroundMonitorObj);
            _sourceColor = Console.ForegroundColor;
            Console.ForegroundColor = status.GetHighlight();
        }

        public void Dispose()
        {
            Console.ForegroundColor = _sourceColor;
            Monitor.Exit(ForegroundMonitorObj);
        }
    }
}