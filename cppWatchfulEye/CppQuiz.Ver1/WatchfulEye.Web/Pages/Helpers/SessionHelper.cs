﻿using System;
using System.Web.SessionState;
using Pvs.Quiz.Common;
using WatchfulEye.Web.Model;

namespace WatchfulEye.Web.Pages.Helpers
{
    /// <summary>
    ///    "Прослойка" для строгой типизации сеанса
    /// </summary>
    public static class SessionHelper
    {
        public static AnswersKeeper GetAnswersKeeper(HttpSessionState session)
        {
            var answersKeeper = Get<AnswersKeeper>(session, SessionKey.AnswerKeeper);
            if (answersKeeper != null) return answersKeeper;
            answersKeeper = new AnswersKeeper();
            Set(session, SessionKey.AnswerKeeper, answersKeeper);

            return answersKeeper;
        }

        public static QuestionsRandomOrder GetQuestionsRandomOrder(HttpSessionState session)
        {
            var randomOrder = Get<QuestionsRandomOrder>(session, SessionKey.QuestionsRandomOrder);
            if (randomOrder != null) return randomOrder;
            randomOrder = new QuestionsRandomOrder();
            Set(session, SessionKey.QuestionsRandomOrder, randomOrder);

            return randomOrder;
        }

        #region Private Helpers

        private static void Set(HttpSessionState session, SessionKey sessionKey, object value)
        {
            session[Enum.GetName(typeof(SessionKey), sessionKey)] = value;
        }

        private static T Get<T>(HttpSessionState session, SessionKey key)
        {
            object dataValue = session[Enum.GetName(typeof(SessionKey), key)];
            return dataValue is T ? (T)dataValue : default(T);
        }

        #endregion
    }
}