﻿using System.Web.Optimization;

namespace Quiz.Web
{
   public static class BundleConfig
   {
      public const string JQueryMobile = "~/bundle/jquerymobile";
      public const string JQuery = "~/bundle/jquery";
      public const string BootstrapJs = "~/bundle/bootstrapjs";
      public const string JQueryMobileCss = "~/bundle/jquerymobileCSS";
      public const string BootstrapCss = "~/bundle/bootstrap";
      //public const string MailCss = "~/bundles/mailstyles";
      //public const string JQueryValidation = "~/bundles/validation";
      public const string LessCssDesktop = "~/Less";
      public const string LessCssMobile = "~/LessMobile";
      public const string TestQuestionDesc = "~/TestQuestionDesc";
      public const string TestQuestionMobile = "~/TestQuestionMobile";
      public const string ProcessAnswer = "~/ProcessAnswer";
      public const string Modernizer = "~/modernizr";
      public const string KnockoutJs = "~/knockoutjs";

      public static void RegisterBundles(BundleCollection bundles)
      {
         var jqMobile = new ScriptBundle(JQueryMobile).Include(
            "~/Scripts/jquery-2.1.3.min.js",
            "~/Scripts/jquery.mobile-1.4.5.min.js");

         var jqSingle = new ScriptBundle(JQuery).Include(
            "~/Scripts/jquery-2.1.3.min.js");

         var bootstrapJs = new ScriptBundle(BootstrapJs).Include(
            "~/Scripts/bootstrap.min.js");

         var jqMobileCss = new StyleBundle(JQueryMobileCss).Include(
            "~/Content/jquery.mobile-1.4.5.min.css");

         var bootstrapCss = new StyleBundle(BootstrapCss).Include(
            "~/Content/bootstrap.min.css",
            "~/Content/bootstrap-theme.min.css");

         //var mailcssBundle = new StyleBundle(MailCss).Include(
         //   "~/Content/styles/mailstyles.css");

         //var validationBundle = new ScriptBundle(JQueryValidation).Include(
         //   "~/Scripts/jquery-2.1.3.min.js",
         //   "~/Scripts/jquery.validate.unobtrusive.min.js");

         var lessBundle = new Bundle(LessCssDesktop).Include(
            "~/Styles/Main.less");
         lessBundle.Transforms.Add(new LessTransform());
         lessBundle.Transforms.Add(new CssMinify());

         var lessMobileBundle = new Bundle(LessCssMobile).Include(
            "~/Styles/Main.Mobile.less");
         lessMobileBundle.Transforms.Add(new LessTransform());
         lessMobileBundle.Transforms.Add(new CssMinify());

         var processAnswerBundle = new Bundle(ProcessAnswer).Include(
            "~/Styles/ProcessAnswer.less");
         processAnswerBundle.Transforms.Add(new LessTransform());
         processAnswerBundle.Transforms.Add(new CssMinify());

         var testQuestionBundle = new Bundle(TestQuestionDesc).Include(
            "~/Scripts/testQuestion.js");

         var testQuestionMobileBundle = new Bundle(TestQuestionMobile).Include(
            "~/Scripts/testQuestion.Mobile.js");

         var modernizrBundle = new Bundle(Modernizer).Include(
            "~/Scripts/modernizr-2.8.3.js");

         var knockoutBundle = new Bundle(KnockoutJs).Include(
            "~/Scripts/knockout-3.3.0.js");

         //bundles.Add(validationBundle);
         //bundles.Add(mailcssBundle);
         bundles.Add(jqMobile);
         bundles.Add(jqSingle);
         bundles.Add(bootstrapJs);
         bundles.Add(jqMobileCss);
         bundles.Add(bootstrapCss);
         bundles.Add(lessBundle);
         bundles.Add(lessMobileBundle);
         bundles.Add(processAnswerBundle);
         bundles.Add(testQuestionBundle);
         bundles.Add(testQuestionMobileBundle);
         bundles.Add(modernizrBundle);
         bundles.Add(knockoutBundle);
      }
   }
}