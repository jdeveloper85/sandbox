﻿using System;
using System.Web.UI;

namespace WatchfulEye.Web.Pages.PostLevels
{
    public partial class Neutral : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Uri urlReferrer = Request.UrlReferrer;
            if (urlReferrer != null)
            {
                Response.RedirectToRoute("RootRoute");
                return;
            }

            ClientRedirectPlaceHolder.Visible = true;
        }
    }
}