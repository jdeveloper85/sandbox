﻿<%@ Page Title="C++ Test" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="TestQuestion.aspx.cs" Inherits="Quiz.Web.TestQuestion" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="Quiz.Web" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContentPlaceHolder">
    <%: Scripts.Render(BundleConfig.TestQuestionDesc) %>
    <%: Styles.Render(BundleConfig.ProcessAnswer) %>      
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div class="row">
        <div class="col-lg-7">
            <div class="panel">
                <div>
                    <div class="pull-left" id="currentQuestionNumber"></div>
                    <div class="pull-right" id="currentScore"></div>
                </div>
            </div>
            <div class="panel">
                <pre id="codeFragment"></pre>
                <div class="h2" id="resultText"></div>
            </div>
            <div class="panel">
                <button id="actionBtn" type="button" class="btn btn-primary btn-lg"></button>
                <button id="noerrorsBtn" type="button" class="btn btn-primary btn-lg"></button>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="panel" id="countdown"></div>
            <div class="panel">
                <span id="projectName"></span>
                <span id="projectLogo"></span>
            </div>
            <div class="panel" id="pvsStudioWarning"></div>
            <div class="panel" id="explanatoryText"></div>
        </div>
    </div>    
    <div class="modal"></div>
</asp:Content>
