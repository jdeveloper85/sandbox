﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Main.Master" AutoEventWireup="true"
    CodeBehind="ProcessTest.aspx.cs" Inherits="WatchfulEye.Web.Pages.ProcessTest" %>

<%@ OutputCache Location="None" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">    
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <Eye:Question ID="UserQuestion" QuestionResponseResult="None" runat="server" />    
</asp:Content>
<asp:Content ID="ResultContent" ContentPlaceHolderID="ResultContentPlaceHolder" runat="server">
    <Eye:Answer ID="UserAnswer" QuestionResponseResult="None" runat="server" />
</asp:Content>
