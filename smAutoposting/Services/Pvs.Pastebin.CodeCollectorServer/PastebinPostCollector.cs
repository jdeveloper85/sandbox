﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Pvs.Autopost.Config;
using Pvs.Autopost.Core;

namespace Pvs.Pastebin.CodeCollectorServer
{
   public sealed class PastebinPostCollector : IPostService
   {
      private const string FirstCaptureBlock =
         "<textarea id=\"paste_code\" class=\"paste_code\" name=\"paste_code\" onkeydown=\"return catchTab(this,event)\">";

      private const string LastCaptureBlock = "</textarea>";
      private const string DefaultLang = "C++";
      private const string EventSourceName = "PastebinCollectorService";
      private const string ApplicationLogName = "Application";

      private readonly PastebinConfiguration _config;
      private readonly string _pastebinCollectorDir;
      private CancellationToken _token;
      private CancellationTokenSource _tokenSource;

      public PastebinPostCollector(string pastebinCollectorDir)
      {
         _pastebinCollectorDir = pastebinCollectorDir;
         _config = SocialKeysReader.Factory.PastebinConfig;
      }

      public void Start()
      {
         _tokenSource = new CancellationTokenSource();
         _token = _tokenSource.Token;
         Task.Factory.StartNew(PastebinCollectorAction, _token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
      }

      public void Stop()
      {
         _tokenSource.Cancel();
      }

      public void Suspend()
      {
         _tokenSource.Cancel();
      }

      public void Resume()
      {
         Start();
      }
      
      private void PastebinCollectorAction()
      {
         while (!_token.IsCancellationRequested)
         {
            try
            {
               IEnumerable<PastebinEntity> pasteEntities = GetPublishPasteEntities(_config.WebSiteUrl);

               foreach (
                  PastebinEntity pastebinEntity in
                     pasteEntities.Where(
                        entity => string.Equals(entity.Lang, DefaultLang, StringComparison.CurrentCultureIgnoreCase))
                        .OrderBy(entity => entity.PublishDate))
               {
                  string rawPasteData = GetRawPasteData(pastebinEntity.PublishLink);
                  if (rawPasteData != string.Empty && rawPasteData.Length > _config.MinimumPastedDataLength)
                  {
                     WriteToCppSourceFile(pastebinEntity, rawPasteData);
                  }
               }
            }
            catch (Exception ex)
            {
               if (EventLog.SourceExists(EventSourceName))
               {
                  using (var svcEventLog = new EventLog(ApplicationLogName, ".", EventSourceName))
                  {
                     svcEventLog.WriteEntry(ex.Message, EventLogEntryType.Warning);
                  }
               }
            }

            Thread.Sleep(TimeSpan.FromSeconds(30));
         }
      }

      private static string GetRawPasteData(string publishLink)
      {
         string resultCode = string.Empty;

         using (var client = new WebClient())
         {
            byte[] responseBytes = client.DownloadData(publishLink);
            string sourceHtml =
               WebUtility.HtmlDecode(Encoding.GetEncoding("utf-8")
                  .GetString(responseBytes, 0, responseBytes.Length));

            if (!string.IsNullOrWhiteSpace(sourceHtml))
            {
               int firstPosition = sourceHtml.IndexOf(FirstCaptureBlock, StringComparison.InvariantCultureIgnoreCase) +
                                   FirstCaptureBlock.Length;
               int lastPosition = sourceHtml.IndexOf(LastCaptureBlock, StringComparison.CurrentCulture);
               resultCode = sourceHtml.Substring(firstPosition, lastPosition - firstPosition);
            }
         }

         return resultCode;
      }

      private IEnumerable<PastebinEntity> GetPublishPasteEntities(string website)
      {
         var pastebinEntities = new List<PastebinEntity>();

         using (var webClient = new WebClient())
         {
            byte[] response = webClient.DownloadData(website);
            string sourceHtml =
               WebUtility.HtmlDecode(Encoding.GetEncoding(_config.DefaultEncoding)
                  .GetString(response, 0, response.Length - 1));
            var document = new HtmlDocument();
            document.LoadHtml(sourceHtml);

            List<HtmlNode> htmlNodes = document.DocumentNode.Descendants()
               .Where(
                  node =>
                     node.Name == "div" && node.Attributes["id"] != null &&
                     node.Attributes["id"].Value.Contains("menu_2"))
               .ToList();

            if (htmlNodes.Count != 1)
            {
               return pastebinEntities;
            }

            List<HtmlNode> liNodes = htmlNodes[0].Descendants("li").ToList();
            foreach (HtmlNode liNode in liNodes)
            {
               string linkValue = liNode.Descendants("a").ToList()[0].GetAttributeValue("href", null);
               string publishLink = website + linkValue;

               string spanContent = liNode.Descendants("span").ToList()[0].InnerText;
               if (!spanContent.Contains("|"))
               {
                  continue;
               }

               string[] splitContent = spanContent.Split(new[] {"|"}, StringSplitOptions.RemoveEmptyEntries);
               if (splitContent.Length != 2)
               {
                  continue;
               }

               string timeValue = splitContent[1];
               if (string.IsNullOrWhiteSpace(timeValue))
               {
                  continue;
               }

               timeValue = timeValue.Trim();
               string[] timeValues = timeValue.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
               if (timeValues.Length < 2)
               {
                  continue;
               }

               string timeString = timeValues[0].Trim();
               int timePass;
               if (!int.TryParse(timeString, out timePass) || string.IsNullOrWhiteSpace(timeValues[1]))
               {
                  continue;
               }

               string timeTypeStr = timeValues[1].Trim();
               TimeParserValue timeType;
               int deltaSeconds = 0;
               if (!Enum.TryParse(timeTypeStr, true, out timeType))
               {
                  continue;
               }

               switch (timeType)
               {
                  case TimeParserValue.Sec:
                     deltaSeconds = timePass;
                     break;

                  case TimeParserValue.Min:
                     deltaSeconds = 60*timePass;
                     break;
               }

               if (deltaSeconds <= 0)
               {
                  continue;
               }

               DateTime publishDate = DateTime.Now.AddSeconds(deltaSeconds);
               string lang = splitContent[0];
               if (string.IsNullOrWhiteSpace(lang))
               {
                  continue;
               }

               pastebinEntities.Add(new PastebinEntity
               {
                  Lang = lang.Trim(),
                  PublishDate = publishDate,
                  PublishLink = publishLink,
                  LinkValue = linkValue.Substring(1, linkValue.Length - 1)
               });
            }
         }

         return pastebinEntities;
      }

      private void WriteToCppSourceFile(PastebinEntity pastebinEntity, string rawPasteData)
      {
         var srcContentBuilder = new StringBuilder();
         srcContentBuilder.AppendLine(string.Format("// {0}", pastebinEntity.PublishLink));
         srcContentBuilder.Append(rawPasteData);

         string fileName = string.Format("{0}{1}{2}.cpp", _pastebinCollectorDir, Path.DirectorySeparatorChar,
            pastebinEntity.LinkValue);
         if (!File.Exists(fileName))
         {
            File.WriteAllText(fileName, srcContentBuilder.ToString(), Encoding.UTF8);
         }
      }
   }
}