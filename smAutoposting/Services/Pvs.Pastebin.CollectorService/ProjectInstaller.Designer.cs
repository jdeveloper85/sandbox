﻿namespace Pvs.Pastebin.CollectorService
{
   partial class ProjectInstaller
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary> 
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Component Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.PastebinCollectorServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
         this.PastebinCollectorServiceInstaller = new System.ServiceProcess.ServiceInstaller();
         // 
         // PastebinCollectorServiceProcessInstaller
         // 
         this.PastebinCollectorServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
         this.PastebinCollectorServiceProcessInstaller.Password = null;
         this.PastebinCollectorServiceProcessInstaller.Username = null;
         // 
         // PastebinCollectorServiceInstaller
         // 
         this.PastebinCollectorServiceInstaller.Description = "Collector of cpp-pastes from pastebin.com";
         this.PastebinCollectorServiceInstaller.DisplayName = "Pastebin collector";
         this.PastebinCollectorServiceInstaller.ServiceName = "PastebinCollectorService";
         this.PastebinCollectorServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
         // 
         // ProjectInstaller
         // 
         this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.PastebinCollectorServiceProcessInstaller,
            this.PastebinCollectorServiceInstaller});

      }

      #endregion

      private System.ServiceProcess.ServiceProcessInstaller PastebinCollectorServiceProcessInstaller;
      private System.ServiceProcess.ServiceInstaller PastebinCollectorServiceInstaller;
   }
}