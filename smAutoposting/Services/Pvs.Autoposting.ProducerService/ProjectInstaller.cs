﻿using System.ComponentModel;
using System.Configuration.Install;

namespace Pvs.Autoposting.ProducerService
{
   [RunInstaller(true)]
   public partial class ProjectInstaller : Installer
   {
      public ProjectInstaller()
      {
         InitializeComponent();
      }
   }
}