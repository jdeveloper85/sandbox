﻿using System;
using System.IO;
using System.Net;
using System.Xml;

namespace Pvs.MailGenerator.Starter
{
   /// <summary>
   ///    Класс-оболочка над вызовами методов Bitly-сервисов
   /// </summary>
   public class BitlyApiWrapper
   {
      private const int ServiceTimeout = 10000;
      private const string ResponseElementName = "response";
      private const string StatusCodeElementName = "status_code";
      private const string DataElementName = "data";
      private const string UrlElementName = "url";
      private readonly string _bitlyApiKey;
      private readonly string _bitlyLogin;

      /// <summary>
      ///    Конструктор
      /// </summary>
      /// <param name="bitlyLogin">Логин для bit.ly</param>
      /// <param name="bitlyApiKey">API-ключ для bit.ly</param>
      public BitlyApiWrapper(string bitlyLogin, string bitlyApiKey)
      {
         _bitlyLogin = bitlyLogin;
         _bitlyApiKey = bitlyApiKey;
      }

      /// <summary>
      ///    Метод укорачивания ссылок
      /// </summary>
      /// <param name="url">Url для укорачивания</param>
      /// <param name="xml">Формат вызова (xml, txt).
      ///    <remarks>xml по-умолчанию</remarks>
      /// </param>
      /// <returns>Короткую ссылку</returns>
      /// <exception cref="WebException">Если вернулся не 200 HTTP-статус код</exception>
      public string Shorten(string url, bool xml = true)
      {
         var escapedUri = Uri.EscapeUriString(url);
         var targetUrl = string.Format("http://api.bit.ly/v3/shorten?login={0}&apiKey={1}&format={2}&longUrl={3}",
            _bitlyLogin, _bitlyApiKey, xml ? "xml" : "txt", escapedUri);
         var request = WebRequest.Create(targetUrl) as HttpWebRequest;

         if (request == null)
            return string.Empty;

         request.Timeout = ServiceTimeout;
         var responseStream = request.GetResponse().GetResponseStream();

         if (responseStream == null)
            return string.Empty;

         if (xml)
         {
            var doc = new XmlDocument();
            doc.Load(responseStream);
            var responseDocument = doc[ResponseElementName];
            if (responseDocument == null || responseDocument[StatusCodeElementName] == null)
               return string.Empty;

            if (responseDocument[StatusCodeElementName].InnerText != "200")
            {
               throw new WebException(responseDocument[StatusCodeElementName].InnerText);
            }

            return doc[ResponseElementName] != null && doc[ResponseElementName][DataElementName] != null &&
                   doc[ResponseElementName][DataElementName][UrlElementName] != null
               ? doc[ResponseElementName][DataElementName][UrlElementName].InnerText
               : string.Empty;
         }

         using (var reader = new StreamReader(responseStream))
         {
            return reader.ReadLine();
         }
      }
   }
}