﻿namespace Quiz.Web.Model
{
   /// <summary>
   ///    Вид запроса при POST-обращении к серверу
   /// </summary>
   public enum AnswerRequestKind
   {
      /// <summary>
      ///    Неопределенный вид запроса
      /// </summary>
      None,

      /// <summary>
      ///    Не знает ответ
      /// </summary>
      DoesNotKnow,

      /// <summary>
      ///    Попытался ответить
      /// </summary>
      TryToAnswer,

      /// <summary>
      ///    Время вышло
      /// </summary>
      TimeIsUp,

      /// <summary>
      ///    Ошибок не было
      /// </summary>
      NoErrors
   }
}