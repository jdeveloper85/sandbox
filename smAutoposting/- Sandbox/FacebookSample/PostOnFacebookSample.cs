﻿using System;
using System.Collections.Specialized;
using System.Dynamic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using Facebook;

namespace FacebookSample
{
   internal static class PostOnFacebookSample
   {
      private const string FacebookAppId = "1523955557819484";
      private const string FacebookAppSecret = "f724df6950fdacf59c01788f510f7055";
      private const string GroupId = "726347477459266";

      private const string AuthUrlFormat =
         "https://graph.facebook.com/oauth/access_token?client_id={0}&client_secret={1}&grant_type=client_credentials&scope=manage_pages,offline_access,publish_stream";

      private static void Main()
      {
         try
         {
            string accessToken = GetAccessToken(FacebookAppId, FacebookAppSecret);
            PostMessage(accessToken, "My test message");
         }
         catch (FacebookOAuthException facebookOAuthEx)
         {
            Console.WriteLine(facebookOAuthEx);
         }
         catch (Exception ex)
         {
            Console.WriteLine(ex.Message);
         }
      }

      private static string GetAccessToken(string apiId, string apiSecret)
      {
         string accessToken = string.Empty;
         string url = string.Format(AuthUrlFormat, apiId, apiSecret);

         WebRequest request = WebRequest.Create(url);
         using (WebResponse response = request.GetResponse())
         using (Stream responseStream = response.GetResponseStream())
         {
            if (responseStream != null)
            {
               string responseString;
               using (var reader = new StreamReader(responseStream, Encoding.UTF8))
               {
                  responseString = reader.ReadToEnd();
               }

               NameValueCollection query = HttpUtility.ParseQueryString(responseString);
               accessToken = query["access_token"];
            }
         }

         if (string.IsNullOrWhiteSpace(accessToken))
            throw new Exception("There is no Access Token");

         return accessToken;
      }

      private static void PostMessage(string accessToken, string message)
      {
         var facebookClient = new FacebookClient(accessToken);
         dynamic messagePost = new ExpandoObject();
         messagePost.access_token = accessToken;
         //messagePost.req_perms = "publish_stream";
         messagePost.message = message;

         //messagePost.picture = "[A_PICTURE]";
         //messagePost.link = "[SOME_LINK]";
         messagePost.name = "Denis Vinevcev";
         //messagePost.caption = "my caption";
         //messagePost.description = "my description";
         /*dynamic result = facebookClient.Post(string.Format("/{0}/feed", GroupId), messagePost);*/
         facebookClient.Post(string.Format("/groups/{0}", GroupId), messagePost);
      }
   }
}