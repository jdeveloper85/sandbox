﻿namespace Pvs.Autopost.Core
{
   /// <summary>
   /// Интерфейс для служб остановки/возобновления
   /// </summary>
   public interface IPostService
   {
      /// <summary>
      /// Запуск
      /// </summary>
      void Start();

      /// <summary>
      /// Остановка
      /// </summary>
      void Stop();

      /// <summary>
      /// Приостановка
      /// </summary>
      void Suspend();

      /// <summary>
      /// Возобновление
      /// </summary>
      void Resume();
   }
}