﻿using System;
using System.Web.Routing;
using System.Web.UI;
using Pvs.Quiz.Common;

namespace WatchfulEye.Web.Controls
{
    /// <summary>
    ///     Web-элемент управления для визуализации состояния ответа
    /// </summary>
    public partial class AnswerStatus : UserControl
    {
        private readonly int _totalQuestionCount = WebConfigReader.TestQuestionsNumber;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected string GetNextQuestionUrl()
        {
            var questionNumber = (string)Page.RouteData.Values["number"] ?? Request.QueryString["number"];
            int question;
            if (int.TryParse(questionNumber, out question))
            {
                var currentRoute = question >= _totalQuestionCount
                    ? RouteTable.Routes.GetVirtualPath(null, "FinishRoute", new RouteValueDictionary())
                    : RouteTable.Routes.GetVirtualPath(null, "QuestionRoute",
                        new RouteValueDictionary { { "number", (question + 1) } });

                return currentRoute != null ? currentRoute.VirtualPath : string.Empty;
            }

            return string.Empty;
        }

        protected string GetQuestionText()
        {
            var questionNumber = (string)Page.RouteData.Values["number"] ?? Request.QueryString["number"];
            int question;
            return int.TryParse(questionNumber, out question)
                ? (question >= _totalQuestionCount ? "Finish" : "Next Question")
                : string.Empty;
        }
    }
}