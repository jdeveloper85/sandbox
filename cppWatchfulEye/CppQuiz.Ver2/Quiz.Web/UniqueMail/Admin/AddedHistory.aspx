﻿<%@ Page Title="История добавления" Language="C#" MasterPageFile="~/UniqueMail/Auth.Master" AutoEventWireup="true"
CodeBehind="AddedHistory.aspx.cs" Inherits="Quiz.Web.UniqueMail.Admin.AddedHistory" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    <style type="text/css">
        .pageHolderClass {
            font-size: inherit;            
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            $('table tr.pageHolderClass td a').addClass('btn btn-default').css('margin', '3px');
        });
    </script>
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <div class="panel-heading">
        <h3><%= Title %></h3>
    </div>    
    <div class="panel-body">
        <%--EnableSortingAndPagingCallbacks="True"--%>
        <asp:GridView CssClass="table table-hover"
                      ID="AddHistoryGridView" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                      DataSourceID="GbSqlDataSource"
                      DataKeyNames="EmailId">
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            <PagerStyle HorizontalAlign="Center" CssClass="pageHolderClass" />                                       
            <Columns>
                <asp:CommandField ShowDeleteButton="True"/>
                <asp:BoundField DataField="EmailId" Visible="False" HeaderText="EmailId" SortExpression="EmailId"/>
                <asp:BoundField DataField="UserName" HeaderText="Пользователь" SortExpression="UserName"/>
                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email"/>
                <asp:BoundField DataField="AddDate" HeaderText="Дата добавления" SortExpression="AddDate"/>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="GbSqlDataSource" runat="server"
                           ConnectionString="<%$ ConnectionStrings:FreelanceIdentityConnection %>"
                           SelectCommand="SELECT [EmailId], [UserName], [Email], [AddDate] FROM [AddHistoryView]"
                           DeleteCommand="DELETE FROM UniqueEmail WHERE EmailId = @EmailId">
            <DeleteParameters>
                <asp:Parameter Name="EmailId"/>
            </DeleteParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>