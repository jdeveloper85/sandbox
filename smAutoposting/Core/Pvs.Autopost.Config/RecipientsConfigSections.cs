﻿using System.Configuration;

namespace Pvs.Autopost.Config
{
   public sealed class RecipientsConfigSections : ConfigurationSection
   {
      [ConfigurationProperty("Recipients")]
      public RecipientCollection Recipients
      {
         get { return (RecipientCollection)base["Recipients"]; }
      }
   }
}