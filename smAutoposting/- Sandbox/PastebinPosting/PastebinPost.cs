﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Text;

namespace PastebinPosting
{
   public sealed class PastebinPost
   {
      public static string PastebinLoginUrl;
      public static string PastebinPostUrl;
      private readonly string _userKey;

      public PastebinPost(string userName, string password, string developerApiKey, string loginUrl)
      {
         UserName = userName;
         Password = password;
         DeveloperApiKey = developerApiKey;
         LoginUrl = loginUrl;
         _userKey = GetUserKey();
      }

      private string UserName { get; set; }

      private string Password { get; set; }

      private string DeveloperApiKey { get; set; }

      private string LoginUrl { get; set; }

      public void Send(string body, string subject, string format, string postUrl)
      {
         if (String.IsNullOrWhiteSpace(body) || String.IsNullOrWhiteSpace(subject))
            return;

         var sendQuery = new NameValueCollection
         {
            {"api_dev_key", DeveloperApiKey},
            {"api_option", "paste"},
            {"api_paste_code", body},
            {"api_paste_private", "0"},
            {"api_paste_name", subject},
            {"api_paste_expire_date", "N"},
            {"api_paste_format", format},
            {"api_user_key", _userKey}
         };

         using (var client = new WebClient())
         {
            string response = Encoding.UTF8.GetString(client.UploadValues(postUrl, sendQuery));
            Uri isValidUri;
            if (!Uri.TryCreate(response, UriKind.Absolute, out isValidUri))
            {
               throw new WebException("Paste Error", WebExceptionStatus.SendFailure);
            }
         }
      }

      private string GetUserKey()
      {
         var loginQuery = new NameValueCollection
         {
            {"api_dev_key", DeveloperApiKey},
            {"api_user_name", UserName},
            {"api_user_password", Password}
         };

         using (var webClient = new WebClient())
         {
            byte[] responseBytes = webClient.UploadValues(LoginUrl, loginQuery);
            string response = Encoding.UTF8.GetString(responseBytes);

            if (response.Contains("Bad API request"))
            {
               throw new WebException("Bad Request", WebExceptionStatus.SendFailure);
            }

            return response;
         }
      }
   }
}