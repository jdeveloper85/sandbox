﻿//  2006-2008 (c) Viva64.com Team
//  2008-2016 (c) OOO "Program Verification Systems"
using System.Collections.Generic;
using CommandLine;
using CommandLine.Text;

namespace ProgramVerificationSystems.PlogConverter
{
    /// <summary>
    ///     Type for command line options
    /// </summary>
    public class CmdConverterOptions
    {
        /// <summary>
        ///     Absolute path to plog-file
        /// </summary>
        /// <example>--plog=c:\example\your_plog.plog</example>
        [Option('p', "plog", Required = true, HelpText = "Path to plog-file")]
        public string PlogPath { get; set; }

        /// <summary>
        ///     Destination directory for output files
        /// </summary>
        /// <example>--outputDir=c:\dest</example>
        [Option('o', "outputDir", Required = false, HelpText = "Output directory for the generated files")]
        public string OutputPath { get; set; }

        /// <summary>
        ///     Root path for source files
        /// </summary>
        /// <example>--srcRoot=c:\projects\solutionfolder</example>
        [Option('r', "srcRoot", Required = false, HelpText = "Root path for your source files")]
        public string SrcRoot { get; set; }

        /// <summary>
        ///     Filter by analyzer type with a list of levels
        /// </summary>
        /// <example>--analyzer=GA:1,2;64:1,2,3</example>
        [OptionList('a', "analyzer", Separator = ';', Required = false,
            HelpText = "Specifies analyzer(s) and level(s) to be used for filtering, i.e. GA:1,2;64:1;OP:1,2,3")]
        public IList<string> AnalyzerLevelFilter { get; set; }

        /// <summary>
        ///     Render types
        /// </summary>
        /// <example>--renderTypes=Html,Totals,Txt,Csv</example>
        [OptionList('t', "renderTypes", Separator = ',', Required = false,
            HelpText = "Render types for output. i.e. Html,Totals,Txt,Csv")]
        public IList<string> PlogRenderTypes { get; set; }

        /// <summary>
        ///     Error codes to disable
        /// </summary>
        /// <example>--excludedCodes=V101,V102,...V200</example>
        [OptionList('d', "excludedCodes", Separator = ',', Required = false, HelpText = "Error codes to disable, i.e. V101,V102,V103")]
        public IList<string> DisabledErrorCodes { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            var helper = HelpText.AutoBuild(this);
            return helper;
        }
    }
}