﻿using System;
using Pvs.Autopost.Core;

namespace ResourceAvailableSample
{
   internal static class Program
   {
      private static readonly string[] WebResources =
      {
         "https://twitter.com/bboytronik",
         "https://mail.ru/",
         "https://www.google.ru/",
         "http://q.viva64.com/",
         "http://q.viva64.com/notexists",
         "http://www.google.com/notexists"         
      };
      
      private static void Main()
      {
         foreach (var webResource in WebResources)
         {
            bool available = AutopostUtils.IsResourceAvailable(webResource);
            Console.WriteLine("{0} availability is {1}", webResource, available);
         }
      }
   }
}