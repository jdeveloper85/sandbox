﻿namespace Pvs.Autopost.Config
{
   /// <summary>
   /// Параметры конфигурации для Twitter'а
   /// </summary>
   public struct TwitterConfiguration
   {
      public string ConsumerKey { get; set; }

      public string ConsumerSecret { get; set; }

      public string AccessToken { get; set; }

      public string AccessTokenSecret { get; set; }
   }
}