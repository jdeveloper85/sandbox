﻿using System.Web;
using WatchfulEye.Web.Model;

namespace WatchfulEye.Web
{
    /// <summary>
    ///     HttpHandler для извлечения изображения
    /// </summary>
    public class GetImageHandler : IHttpHandler
    {
        public const string QuestionIdQueryStringName = "QuestionId";

        public void ProcessRequest(HttpContext context)
        {
            var questionIdStr = context.Request.QueryString[QuestionIdQueryStringName];
            int questionId;
            byte[] imageLogo;
            if (int.TryParse(questionIdStr, out questionId) &&
                (imageLogo = WatchfulEyeRepository.GetImageLogo(questionId)) != null)
            {
                context.Response.ContentType = "image/png";
                context.Response.BinaryWrite(imageLogo);
            }
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}