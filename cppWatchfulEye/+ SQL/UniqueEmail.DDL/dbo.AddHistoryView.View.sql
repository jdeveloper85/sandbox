USE [1gb_x_email20b]
GO
/****** Object:  View [dbo].[AddHistoryView]    Script Date: 19.02.2015 15:21:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[AddHistoryView] as
select e.EmailId, u.UserName, e.Email, e.AddDate from aspnet_Users u
inner join UniqueEmail e on e.UserId = u.UserId
GO
