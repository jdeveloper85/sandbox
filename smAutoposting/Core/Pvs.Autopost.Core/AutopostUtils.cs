﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace Pvs.Autopost.Core
{
   /// <summary>
   ///    Утилиты ядра автоматического размещения
   /// </summary>
   public static class AutopostUtils
   {
      private const string LinksRegexpPattern =
            @"(http|https)://([\w+?\.\w+])+([a-zA-Z0-9\~\!\@\#\$\%\^\&\*\(\)_\-\=\+\\\/\?\.\:\;\'\,]*)?";

      private static readonly Regex LinksRegex = new Regex(LinksRegexpPattern, RegexOptions.Compiled);

      /// <summary>
      ///    Группировка URI по доменам
      /// </summary>
      /// <param name="uris">Набор URI</param>
      /// <returns>Словарь Хост => Набор URI</returns>
      private static IDictionary<string, IList<Uri>> GroupLinksByHost(IEnumerable<Uri> uris)
      {
         var linksByHost = new Dictionary<string, IList<Uri>>();
         foreach (Uri uri in uris)
         {
            string host = uri.Host;
            if (!linksByHost.ContainsKey(host))
            {
               linksByHost[host] = new List<Uri>();
            }

            linksByHost[host].Add(uri);
         }

         return linksByHost;
      }

      /// <summary>
      ///    Получение сущностей для статистики по ссылкам
      /// </summary>
      /// <param name="linksByHost">Словарь [домен, Набор ссылок для домена]</param>
      /// <returns>Cущности для статистики по ссылкам</returns>
      private static IList<LinkStatistics> GetLinkStatistics(IDictionary<string, IList<Uri>> linksByHost)
      {
         int totalCount = linksByHost.Keys.Sum(host => linksByHost[host].Count);
         return (from host in linksByHost.Keys
                 let linkPercent = (linksByHost[host].Count / (double)totalCount) * 100
                 select new LinkStatistics
                 {
                    Host = host,
                    Percent = linkPercent,
                    Links = linksByHost[host]
                 }).ToList();
      }

      /// <summary>
      ///    Получение сущностей для статистики по ссылкам
      /// </summary>
      /// <param name="postUris">Набор URI</param>
      /// <returns>Cущности для статистики по ссылкам</returns>
      public static IList<LinkStatistics> GetLinkStatistics(IEnumerable<Uri> postUris)
      {
         IDictionary<string, IList<Uri>> publishedLinksByDomain = GroupLinksByHost(postUris);
         IList<LinkStatistics> statLinks = GetLinkStatistics(publishedLinksByDomain);
         return statLinks;
      }

      /// <summary>
      /// Превысился ли предел размещения для домена
      /// </summary>
      /// <param name="hostName">Имя хоста</param>
      /// <param name="percentDomainLimit">Допустимый предел размещения</param>
      /// <param name="statLinks">Набор статистик по ссылкам</param>
      /// <returns>true, если превысился, false - в противном случае</returns>
      public static bool HasExceededDomainLimit(string hostName, double percentDomainLimit,
         IEnumerable<LinkStatistics> statLinks)
      {
         return (from statLink in statLinks
                 where statLink.Host.Equals(hostName, StringComparison.CurrentCultureIgnoreCase)
                 select statLink.Percent into percent
                 select percent >= percentDomainLimit).FirstOrDefault();
      }

      /// <summary>
      /// Проверяет доступность ресурса по строке URI
      /// </summary>
      /// <param name="pureUriString">Строка URI</param>
      /// <returns>true, если ресурс доступен по URI, false - в противном случае</returns>
      public static bool IsResourceAvailable(string pureUriString)
      {
         try
         {
            var request = WebRequest.Create(pureUriString) as HttpWebRequest;
            if (request == null)
               return false;
            
            var response = request.GetResponse() as HttpWebResponse;
            if (response != null)            
               return response.StatusCode == HttpStatusCode.OK;            

            return false;
         }
         catch (Exception)
         {
            return false;
         }
      }

      /// <summary>
      /// Преобразование всех найденных Url в действительные Html-ссылки
      /// </summary>
      /// <param name="source">Исходная строка</param>
      /// <returns>Строка, в которой подсвечены Url Html-ссылками</returns>
      public static string HighlightLinks(string source)
      {
         return LinksRegex.Replace(source, @"<a href='$&'>$&</a>");
      }
   }
}