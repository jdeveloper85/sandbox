﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using Pvs.Autopost.Core;
using Pvs.Autoposting.Entities;

namespace Pvs.Autoposting.ProducerServer
{
   /// <summary>
   ///    Класс, извлекающий запросы для размещения и помещающий их в MSMQ
   /// </summary>
   public class AutopostProducer : IPostService
   {
      private readonly int _intervalInSeconds;
      private readonly IAutopostRepository _socialAutopostRepository;
      private readonly int _takePostLimit;
      private readonly TrackHandler _trackHandler;
      private CancellationToken _cancellationToken;
      private CancellationTokenSource _tokenSource;

      /// <summary>
      /// Конструктор производителя сообщений для размещения
      /// </summary>
      /// <param name="takePostLimit">Предельное кол-во постов для размещения на один запрос</param>
      /// <param name="intervalInSeconds">Интервал в секундах между обращениями</param>
      /// <param name="logger">Логгер</param>
      public AutopostProducer(int takePostLimit, int intervalInSeconds, ILog logger)
      {
         _takePostLimit = takePostLimit;
         _intervalInSeconds = intervalInSeconds;
         _socialAutopostRepository = new SocialAutopostRepository(logger);
         _trackHandler = new TrackHandler(logger, true);
      }

      public void Start()
      {
         _tokenSource = new CancellationTokenSource();
         _cancellationToken = _tokenSource.Token;
         Task.Factory.StartNew(PushPostsAction, _cancellationToken, TaskCreationOptions.LongRunning,
            TaskScheduler.Default);
      }

      public void Stop()
      {
         _tokenSource.Cancel();
      }

      public void Suspend()
      {
         _tokenSource.Cancel();
      }

      public void Resume()
      {
         Start();
      }

      private void PushPostsAction()
      {
         while (!_cancellationToken.IsCancellationRequested)
         {
            try
            {
               IEnumerable<Post> newPosts = _socialAutopostRepository.GetNewPosts(_takePostLimit);
               IEnumerable<Post> notDeferredPosts = newPosts as Post[] ?? newPosts.ToArray();
               var availablePosts = new List<Post>();

               foreach (var newPost in notDeferredPosts)
               {
                  if (AutopostUtils.IsResourceAvailable(newPost.PureUri))
                  {
                     availablePosts.Add(newPost);
                  }
                  else
                  {
                     _trackHandler.Handle(string.Format("Resource is not available by address {0}", newPost.PureUri));
                     _socialAutopostRepository.SetPostUnavailable(newPost.PostId);
                  }
               }

               if (availablePosts.Any())
               {
                  _socialAutopostRepository.SetInPending(availablePosts.Select(post => post.PostId).ToArray());
                  foreach (var availablePost in availablePosts)
                     _trackHandler.Handle(
                        string.Format("'{0}' was set in pending for publish", availablePost.PostText));
               }
               else
               {
                  _trackHandler.Handle("There is currently no available publications");
               }
            }
            catch (Exception ex)
            {
               _trackHandler.Handle(ex);
            }

            _trackHandler.Handle(
               string.Format("The following publications will be considered at the planning through {0}",
                  _intervalInSeconds));
            Thread.Sleep(TimeSpan.FromSeconds(_intervalInSeconds));
         }
      }
   }
}