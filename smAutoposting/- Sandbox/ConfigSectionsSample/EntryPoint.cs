﻿/**
 * Создание своих собственных разделов конфигурации
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Pvs.Autopost.Config;

namespace ConfigSectionsSample
{
   internal static class EntryPoint
   {
      private static void Main()
      {
         IEnumerable<EmailRecipient> recipients = SocialKeysReader.Factory.EmailRecipients;
         Array.ForEach(recipients.ToArray(), recipient => Console.WriteLine(recipient));         
      }
   }
}