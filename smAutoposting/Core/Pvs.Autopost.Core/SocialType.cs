﻿using System;

namespace Pvs.Autopost.Core
{
   /// <summary>
   /// Тип социальной сети
   /// </summary>
   [Flags]
   public enum SocialType : byte
   {      
      /// <summary>
      /// Twitter
      /// </summary>
      [PostLimit(PostLimit = 140, LinkTruncationLength = 22)]
      Twitter = 0x1,

      /// <summary>
      /// Facebook
      /// </summary>
      [PostLimit]
      Facebook = 0x2,

      /// <summary>
      /// GooglePlus
      /// </summary>
      [PostLimit]
      GooglePlus = 0x4,

      /// <summary>
      /// Reddit
      /// </summary>
      [PostLimit]
      Reddit = 0x8
   }
}