USE [1gb_x_email20b]
GO

SELECT u.UserName         Freelancer,
       COUNT(m.Email)     MailCount
FROM   UniqueEmail m
       INNER JOIN aspnet_Users u
            ON  m.UserId = u.UserId
GROUP BY
       u.UserName
ORDER BY
       MailCount          DESC