﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using CppQuiz.Data;

namespace Quiz.Web.Utils
{
   /// <summary>
   ///    "Сырые" методы для таблиц БД Quiz
   /// </summary>
   public static class RawQuizSqlUtils
   {
      /// <summary>
      ///    Получение случайных идентификаторов вопросов
      /// </summary>
      /// <param name="topLimit">Максимальное кол-во записей</param>
      /// <param name="quizConnectionString">Строка соединения</param>
      /// <returns>Случайные идентификаторы вопросов</returns>
      public static IEnumerable<int> GetRandomQuestionIds(int topLimit, string quizConnectionString)
      {
         var randomIds = new List<int>();

         using (var sqlConnection = new SqlConnection(quizConnectionString))
         {
            sqlConnection.Open();
            using (
               var randomIdsCommand =
                  new SqlCommand(
                     String.Format("SELECT TOP {0} QuestionId FROM QuizQuestion ORDER BY NEWID()", topLimit),
                     sqlConnection))
            {
               var sqlDataReader = randomIdsCommand.ExecuteReader();
               while (sqlDataReader.Read())
               {
                  randomIds.Add((int)sqlDataReader[0]);
               }
            }

            return randomIds.ToArray();
         }
      }

      /// <summary>
      /// Получение всех вопросов
      /// </summary>
      /// <returns>Все вопросы</returns>
      public static QuizQuestion[] GetQuestions()
      {
         using (var ctx = new QuizEntities())
         {
            return (from q in ctx.QuizQuestions
                    select q).ToArray();
         }
      }
   }
}