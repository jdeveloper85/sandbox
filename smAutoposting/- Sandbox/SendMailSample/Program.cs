﻿/**
 * Пример отправки письма
 */

using System;
using Pvs.Autopost.Config;

namespace SendMailSample
{
   internal static class Program
   {
      private static void Main()
      {
         try
         {
            var emailWrapper = new SendEmailWrapper(SocialKeysReader.Factory.EmailConfig);

            var recipient = new EmailRecipient
            {
               ToAddress = "vinevcev@viva64.com",
               ToDisplayName = "Denis Vinevcev"
            };

            var message = new EmailMessage
            {
               MessageBody = "Autoposting Error",
               MessageSubject = "Error message occured"
            };

            emailWrapper.Send(message, recipient);
         }
         catch (Exception ex)
         {
            Console.WriteLine(ex);
         }
      }
   }
}