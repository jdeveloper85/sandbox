﻿//  2006-2008 (c) Viva64.com Team
//  2008-2016 (c) OOO "Program Verification Systems"
namespace CSharpAnalyzerTester.Env
{
    public interface IEnv
    {
        string LogDirFormattableName { get; }
        string SettingsPath { get; }
        string EtalonLogDirectory { get; }
        string LogsDirectory { get; }
        string LogsDiffDirectory { get; }
        string ApproveDiffsDir { get; }
        string AnalyzerImagePath { get; set; }
        string AnalyzerSettingsPath { get; }
        bool EnableBenchmark { get; set; }
        IEnv Default { get; }        
    }

    public sealed class EnvImpl : IEnv
    {
        public string LogDirFormattableName { get; private set; }
        public string SettingsPath { get; private set; }
        public string EtalonLogDirectory { get; private set; }
        public string LogsDirectory { get; private set; }
        public string LogsDiffDirectory { get; private set; }
        public string ApproveDiffsDir { get; private set; }
        public string AnalyzerImagePath { get; set; }
        public string AnalyzerSettingsPath { get; private set; }
        public bool EnableBenchmark { get; set; }

        public IEnv Default
        {
            get
            {
                return new EnvImpl
                {
                    LogDirFormattableName = EnvValueSet.LogDirFormattableName,
                    SettingsPath = EnvValueSet.SettingsPath,
                    EtalonLogDirectory = EnvValueSet.EtalonLogDirectory,
                    LogsDirectory = EnvValueSet.LogsDirectory,
                    LogsDiffDirectory = EnvValueSet.LogsDiffDirectory,
                    ApproveDiffsDir = EnvValueSet.ApproveDiffsDir,
                    AnalyzerImagePath = EnvValueSet.AnalyzerImagePath,
                    AnalyzerSettingsPath = EnvValueSet.AnalyzerSettingsPath,
                    EnableBenchmark = EnvValueSet.EnableBenchmark
                };
            }
        }
    }
}