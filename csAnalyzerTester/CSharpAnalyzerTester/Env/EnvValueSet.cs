﻿//  2006-2008 (c) Viva64.com Team
//  2008-2016 (c) OOO "Program Verification Systems"
using System;
using System.IO;
using CSharpAnalyzerTester.Helpers;

namespace CSharpAnalyzerTester.Env
{
    internal static class EnvValueSet
    {
        #region Константы

        private const string TestSuitesLabel = "TestSuites";
        private const string SettingsXmlLabel = "Settings.xml";
        private const string LogsLabel = "Logs";
        private const string EtalonLogsLabel = "EtalonLogs";
        private const string Viva64LogsLabel = "Viva64Logs";
        private const string DiffsLabel = "Diffs";
        private const string ApprovesLabel = "Approves";
        private const string PvsStudioCsExecutableName = "PVS-Studio_Cs.exe";
        private const string PvsStudioCsXmlConfig = "Settings.xml.CSAnalyzerUnitTests";

        #endregion

        #region Поля

        private static readonly string LogDirectoryFormat =
            string.Format("{0}@{1}", Environment.MachineName, DateTime.Now.ToString("s").Replace('T', '#'))
                .Replace(':', '_');

        private static readonly string Settings = Path.Combine(Environment.CurrentDirectory, TestSuitesLabel,
            SettingsXmlLabel);

        private static readonly string EtalonLogDir = Path.Combine(Environment.CurrentDirectory, LogsLabel,
            string.Format("{0}{1}", EtalonLogsLabel, Path.DirectorySeparatorChar));

        private static readonly string LogsDir = Path.Combine(Environment.CurrentDirectory, LogsLabel, Viva64LogsLabel,
            string.Format("{0}{1}", LogDirFormattableName, Path.DirectorySeparatorChar));

        private static readonly string LogsDiffDir = string.Format("{0}{1}{2}", LogsDirectory, DiffsLabel,
            Path.DirectorySeparatorChar);

        private static readonly string ApproveDir = Path.Combine(LogsDiffDirectory, ApprovesLabel);

        private static string _analyzerImagePath = Path.Combine(Environment.CurrentDirectory, PvsStudioCsExecutableName);

        private static readonly string AnalyzerSettings = Path.Combine(Environment.CurrentDirectory,
            PvsStudioCsXmlConfig);

        #endregion

        #region Свойства

        public static string LogDirFormattableName
        {
            get { return LogDirectoryFormat; }
        }

        public static string SettingsPath
        {
            get { return Settings; }
        }

        public static string EtalonLogDirectory
        {
            get { return EtalonLogDir; }
        }

        public static string LogsDirectory
        {
            get { return LogsDir; }
        }

        public static string LogsDiffDirectory
        {
            get { return LogsDiffDir; }
        }

        public static string ApproveDiffsDir
        {
            get { return ApproveDir; }
        }

        public static string AnalyzerImagePath
        {
            get { return _analyzerImagePath; }
            set { _analyzerImagePath = value; }
        }

        public static string AnalyzerSettingsPath
        {
            get { return AnalyzerSettings; }
        }

        public static bool EnableBenchmark
        {
            get { return SingletonInjector<ConfImpl>.Instance.EnableBenchmark; }
        }

        #endregion
    }
}