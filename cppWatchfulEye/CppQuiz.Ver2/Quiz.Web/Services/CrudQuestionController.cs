﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using CppQuiz.Data;
using Quiz.Web.Infrastructure.Abstract;

namespace Quiz.Web.Services
{
   public class CrudQuestionController : ApiController
   {
      private readonly ICrudQuestionRepository<QuizQuestion> _repository;

      public CrudQuestionController(ICrudQuestionRepository<QuizQuestion> repository)
      {
         _repository = repository;
      }

      // GET: api/CrudQuestion
      public async Task<IEnumerable<QuizQuestion>> GetAsync()
      {
         return await _repository.GetAllAsync().ConfigureAwait(false);
      }

      // GET: api/CrudQuestion/5
      public async Task<QuizQuestion> GetAsync(int questionId)
      {
         return await _repository.GetAsync(questionId).ConfigureAwait(false);
      }

      // POST: api/CrudQuestion
      public async Task PostAsync([FromBody] QuizQuestion question)
      {
         await _repository.AddAsync(question).ConfigureAwait(false);
      }

      // PUT: api/CrudQuestion/5
      public async Task PutAsync(int questionId, [FromBody] QuizQuestion question)
      {
         await _repository.UpdateAsync(question).ConfigureAwait(false);
      }

      // DELETE: api/CrudQuestion/5
      public async Task DeleteAsync(int questionId)
      {
         await _repository.DeleteAsync(questionId).ConfigureAwait(false);
      }
   }
}