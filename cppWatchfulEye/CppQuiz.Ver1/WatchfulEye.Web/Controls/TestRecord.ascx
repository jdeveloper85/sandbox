﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestRecord.ascx.cs" Inherits="WatchfulEye.Web.Controls.TestRecord"
            ClientIDMode="Static" %>
<table class="test-record">
    <tr>
        <td id="project" style="width: 10%;">
            <div style="margin-top: 35px;">&nbsp;</div>
            <div id="projectLogo" runat="server"></div>
            <div>
                This error was found in the<span id="projectName" runat="server"></span>
                project by <a class="standart" href="http://www.viva64.com/en/pvs-studio/" target="blank">PVS-Studio</a> C/C++ static code analyzer.
            </div>
        </td>
        <td style="width: 100px;">
            <div class="big">Click on the bug in the code</div>
            <div id="currentQuestionNumber" runat="server"></div>
            <div id="current-score" style="display: inline; float: right;">
                Current score is<span id="score"></span>
            </div>
            <div id="codeFragment" runat="server"></div>
        </td>
        <td style="padding-left: 30px;">
            <div style="margin-top: 40px;"></div>
            <div id="secondsLeft" runat="server">
                <span class="countdown"></span> seconds left
            </div>
            <div id="static-warn-message">PVS-Studio warning:</div>
            <div id="pvsWarning" runat="server">                
                <div style="float: left; margin-right: 15px;">
                    <a class="standart" href="http://www.viva64.com/en/pvs-studio/" target="blank">
                        <img src="../Images/unicorn.png" alt="PVS-Studio" /></a>
                </div>
            </div>
            <div id="explanatoryText" runat="server"></div>
        </td>
    </tr>
</table>