﻿using System.Text;
using System.Text.RegularExpressions;

namespace WatchfulEye.Web.Model
{
    public class CodeSegmentPreprocessor
    {
        private const string CodeFragmentRePattern = @"(?<CodeSegment>(([a-zA-Z_\d])+|" +
                                                     @"([\d]+)|" +
                                                     @"([\(\)\[\]\{\};\,\?""']{1})|" +
                                                     @"(\+[+,=]?)|" +
                                                     @"(\-[-,>,=]?)|" +
                                                     @"(%[=]?)|" +
                                                     @"(\*[*,=]?)|" +
                                                     @"(/[=]?)|" +
                                                     @"(\:{1,2})|" +
                                                     @"((\![=]?){1})|" +
                                                     @"(={1,2})|" +
                                                     @"(<[=,<]?)|" +
                                                     @"(>[>,=]?)|" +
                                                     @"(&{1,2})|" +
                                                     @"(\|{1,2})))";

        private const string DefaultStartTag = "<b class='highlight-fragment'>";
        private const string RightStartTag = "<b class='highlight-fragment right-fragment'>";
        private const string DefaultEndTag = "</b>";
        private const char MarkedCorrectSymbol = '@';
        private const string CodesegmentGroupname = "CodeSegment";

        private CodeSegmentPreprocessor(string startSurroundedTag, string endSurroundedTag)
        {
            StartSurroundedTag = startSurroundedTag;
            EndSurroundedTag = endSurroundedTag;
        }

        public CodeSegmentPreprocessor()
            : this(DefaultStartTag, DefaultEndTag)
        {
        }

        private string StartSurroundedTag { get; set; }
        private string EndSurroundedTag { get; set; }

        public string Decorate(string codeFragment)
        {
            var decoratedFragment = CodeFragmentRegex.Replace(codeFragment,
                match =>
                {
                    var codeSegmentValue = match.Groups[CodesegmentGroupname].Value;
                    var foundIndex = match.Index;
                    var startSurroundedTag = foundIndex > 0 && codeFragment[foundIndex - 1] == MarkedCorrectSymbol
                                             &&
                                             codeFragment[foundIndex + codeSegmentValue.Length] == MarkedCorrectSymbol
                        ? RightStartTag
                        : StartSurroundedTag;

                    return string.Format("{0}{1}{2}", startSurroundedTag, codeSegmentValue, EndSurroundedTag);
                });
            var decoratedBuilder = new StringBuilder(decoratedFragment);
            return decoratedBuilder.Replace(string.Format("{0}", MarkedCorrectSymbol), string.Empty)
                .ToString();
        }

        private static readonly Regex CodeFragmentRegex = new Regex(CodeFragmentRePattern, RegexOptions.Compiled);
    }
}