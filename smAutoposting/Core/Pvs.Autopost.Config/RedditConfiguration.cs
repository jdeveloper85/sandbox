﻿using System.Collections.Generic;

namespace Pvs.Autopost.Config
{
   /// <summary>
   /// Конфигурация для Reddit'а
   /// </summary>
   public struct RedditConfiguration
   {
      public string UserName { get; set; }

      public string Password { get; set; }

      public IEnumerable<string> Subreddits { get; set; }
   }
}