using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using VkNet.Enums.Filters;
using VkNet.Model;
using ContactApi = VkNet.VkApi;

namespace VkApi.Library
{
   public class VkApiCallWrapper
   {
      private const string GetUploadServerMethod = "docs.getUploadServer"; // vk-����� �������� �����
      private const string SaveMethod = "docs.save"; // vk-����� ���������� �����

      private readonly ContactApi _api;
      private readonly VkSettings _settings;

      public VkApiCallWrapper(VkSettings vkSettings)
      {
         _api = new ContactApi();
         _settings = vkSettings;
         _api.Authorize(vkSettings.AppId, vkSettings.Login, vkSettings.Password, Settings.All);
         VkObject groupScreenName = _api.Utils.ResolveScreenName(vkSettings.GroupName);
         if (groupScreenName.Id == null)
            throw new Exception("Group Id is undefined");
      }

      public void UploadDocument(string pdfFileName)
      {
         IDictionary<string, string> uploadParameters = new Dictionary<string, string>
         {
            {"group_id", _settings.GroupId.ToString(CultureInfo.InvariantCulture)}
         };

         string uploadJsonResult = _api.Invoke(GetUploadServerMethod, uploadParameters, true);
         // �������� Api-����� �������� ���������
         var vkDocResponse = JsonConvert.DeserializeObject<VkDocResponse>(uploadJsonResult);
         string uploadUrl = vkDocResponse.Response.Upload_Url;

         VkFileResponse vkFileResponse;
         using (var webClient = new WebClient())
         {
            byte[] uploadData = webClient.UploadFile(uploadUrl, pdfFileName);
            string responseMessage = Encoding.UTF8.GetString(uploadData);
            vkFileResponse = JsonConvert.DeserializeObject<VkFileResponse>(responseMessage);
         }

         if (vkFileResponse != null && !string.IsNullOrEmpty(vkFileResponse.File))
         {
            string documentTitle = PdfUtils.GetPdfTitle(pdfFileName); // ��������� ��������� ���������
            string tags = GetTags(documentTitle); // ��������� ������ �� �����

            IDictionary<string, string> saveParameters = new Dictionary<string, string>
            {
               {"file", vkFileResponse.File},
               {"title", documentTitle},
               {"tags", tags}
            };

            string saveJsonResult = _api.Invoke(SaveMethod, saveParameters, true);
            // �������� Api-����� ���������� ���������
            var saveResponse = JsonConvert.DeserializeObject<VkSaveResponse>(saveJsonResult);
            if (saveResponse != null && saveResponse.Response.Count == 1)
            {
               Uri docUriResult;
               bool successUriCreate =
                  Uri.TryCreate(saveResponse.Response[0].Url, UriKind.Absolute, out docUriResult) &&
                  docUriResult.Scheme == Uri.UriSchemeHttp;
               if (!successUriCreate)
                  throw new Exception(string.Format("Error uploading document {0}", pdfFileName));
            }
            else
               throw new Exception(string.Format("Error uploading document {0}", pdfFileName));
         }
         else
            throw new Exception(string.Format("Error uploading file {0}", pdfFileName));
      }

      private static string GetTags(string aTitle)
      {
         if (string.IsNullOrEmpty(aTitle))
            throw new ArgumentException(aTitle);

         string[] splitStrings = Regex.Split(aTitle, @"\W+");
         var splitBuilder = new StringBuilder();
         foreach (string splitString in splitStrings)
         {
            splitBuilder.AppendFormat("{0} ", splitString);
         }

         return splitBuilder.ToString();
      }

      #region ���� ��� Json-�������

      private class VkDocResponse
      {
         public VkUploadResponse Response { get; set; }
      }

      private class VkFileResponse
      {
         public string File { get; set; }
      }

      private class VkSaveResponse
      {
         public List<VkSaveResult> Response { get; set; }
      }

      private class VkSaveResult
      {
         public int Did { get; set; }

         public int Owner_Id { get; set; }

         public string Title { get; set; }

         public int Size { get; set; }

         public string Ext { get; set; }

         public string Url { get; set; }
      }

      private class VkUploadResponse
      {
         public string Upload_Url { get; set; }
      }

      #endregion
   }
}