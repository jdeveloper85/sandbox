﻿using System;
using System.Runtime.Serialization;

namespace Pvs.Autopost.Core
{
   /// <summary>
   /// Исключение, возникающее при ошибке размещения поста
   /// </summary>
   [Serializable]
   public class PostFailException : Exception
   {
      public PostFailException()
      {
      }

      public PostFailException(string message)
         : base(message)
      {
      }

      public PostFailException(string message, Exception inner)
         : base(message, inner)
      {
      }

      protected PostFailException(SerializationInfo info, StreamingContext context)
         : base(info, context)
      {
      }
   }
}