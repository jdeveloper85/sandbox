﻿using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject.Web.Common;
using Quiz.Web;
using WebActivator;

[assembly: PreApplicationStartMethod(typeof (NinjectWeb), "Start")]

namespace Quiz.Web
{
   public static class NinjectWeb
   {
      public static void Start()
      {
         DynamicModuleUtility.RegisterModule(typeof (NinjectHttpModule));
      }
   }
}