﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pvs.Autopost.Config;
using Pvs.Autopost.Core;
using Pvs.Autoposting.Entities;

namespace PostEditor
{
   public partial class EditPosts : Page
   {
      private readonly HashSet<string> _currentSocialTypes = new HashSet<string>();
      private readonly IAutopostRepository _repository = new SocialAutopostRepository();

      protected void Page_Load(object sender, EventArgs e)
      {
      }

      public IQueryable<Post> GetPosts()
      {
         return _repository.GetPosts().AsQueryable();
      }

      public void UpdatePost(int? postId)
      {
         Post postToUpdate = _repository.GetPosts().FirstOrDefault(post => post.PostId == postId);
         if (postToUpdate == null)
            return;         
         
         if (!TryUpdateModel(postToUpdate))
            return;

         byte socialByte = 0;
         foreach (string socialTypeString in _currentSocialTypes)
         {
            SocialType socialType;
            if (!Enum.TryParse(socialTypeString, true, out socialType))
               continue;

            if (socialType == SocialType.Twitter)
            {
               int pureTextLength = postToUpdate.PostText.Length - postToUpdate.PureUri.Length;
               int postLimit = socialType.GetPostLimit();
               if (pureTextLength + socialType.GetLinkTruncationLength() > postLimit)
               {
                  ModelState.AddModelError("Twitter Error",
                     string.Format("The length of the text is not suitable for Twitter. Must be {0}", postLimit));
                  return;
               }
            }

            socialByte += (byte)socialType;
         }
         
         postToUpdate.SocialBits = socialByte;
         _repository.Save(postToUpdate);
      }

      public void InsertPost(Post aPost)
      {
         string errorMessage;
         if (!PureUriValidator.CheckPureUri(aPost.PureUri, out errorMessage))
         {
            ModelState.AddModelError("Pure URI Error", errorMessage);
            return;
         }

         if (!TryUpdateModel(aPost))
            return;

         byte socialByte = 0;
         foreach (string socialTypeString in _currentSocialTypes)
         {
            SocialType socialType;
            if (!Enum.TryParse(socialTypeString, true, out socialType))
               continue;

            if (socialType == SocialType.Twitter)
            {
               int pureTextLength = aPost.PostText.Length - aPost.PureUri.Length;
               int postLimit = socialType.GetPostLimit();
               if (pureTextLength + socialType.GetLinkTruncationLength() > postLimit)
               {
                  ModelState.AddModelError("Twitter Error",
                     string.Format("The length of the text is not suitable for Twitter. Must be {0}", postLimit));
                  return;
               }
            }

            socialByte += (byte)socialType;
         }

         var pureUri = new Uri(aPost.PureUri);
         string host = pureUri.Host;
         aPost.IsPriority = string.Equals(host, SocialKeysReader.PriorityHost, StringComparison.InvariantCultureIgnoreCase);
         aPost.CreatedDate = DateTime.Now;
         aPost.SocialBits = socialByte;

         _repository.Save(aPost);
      }

      public void DeletePost(Post aPost)
      {
         _repository.Delete(aPost);
      }

      public IEnumerable<string> GetSocialTypes()
      {
         return from SocialType socialType in Enum.GetValues(typeof(SocialType))
                select Enum.GetName(typeof(SocialType), socialType);
      }

      protected void AutopostListView_OnItemInserting(object sender, ListViewInsertEventArgs e)
      {         
         ListViewItem insertItem = e.Item;
         if (insertItem.ItemType != ListViewItemType.InsertItem)
            return;

         UpdateSelectedSocialValues(insertItem);
      }

      protected void AutopostListView_OnItemUpdating(object sender, ListViewUpdateEventArgs e)
      {         
         var updateItem = AutopostListView.Items[e.ItemIndex] as ListViewItem;
         if (updateItem == null)
            return;

         UpdateSelectedSocialValues(updateItem);
      }

      private void UpdateSelectedSocialValues(Control control)
      {
         if (_currentSocialTypes.Count > 0)
            _currentSocialTypes.Clear();

         var socialCheckBoxList = control.FindControl("SocialCheckBoxList") as CheckBoxList;
         if (socialCheckBoxList == null)
            return;

         foreach (
            ListItem socialListItem in
               socialCheckBoxList.Items.Cast<ListItem>().Where(socialListItem => socialListItem.Selected))
         {
            _currentSocialTypes.Add(socialListItem.Value);
         }
      }

      protected static string GetSocialTypes(Post postItem)
      {
         var socialTypeSettings = (SocialType)postItem.SocialBits;
         var socialSettings = new StringBuilder();
         foreach (string socialName in from SocialType socialType in Enum.GetValues(typeof(SocialType))
                                       where socialTypeSettings.HasFlag(socialType)
                                       select Enum.GetName(typeof(SocialType), socialType))
         {
            socialSettings.AppendFormat("{0}, ", socialName);
         }

         string socials = socialSettings.ToString();
         const int cutCount = 2;
         socials = socials.Length > cutCount
            ? socials.Substring(0, socials.Length - cutCount)
            : "Not selected any social network";

         return socials;
      }

      protected static string GetPriority(Post postItem)
      {
         return postItem.IsPriority ? "Yes" : "No";
      }

      protected static string GetPending(Post postItem)
      {
         return postItem.IsPending ? "Pending to post" : "No in pending";
      }

      protected static string GetPosted(Post postItem)
      {
         return postItem.IsPosted ? "Posted" : "Not posted";
      }

      protected static string GetResourceAvailable(Post postItem)
      {
         bool? available = postItem.IsResourceAvailable;
         if (available == null)
            return "Not yet known";

         return available.Value ? "Available" : "Not available";
      }

      protected static string GetPostedDate(Post postItem)
      {
         DateTime? postedDate = postItem.PostedDate;
         return postedDate == null ? "Not posted yet" : postedDate.Value.ToString(CultureInfo.CurrentCulture);
      }

      protected static string GetErrorMessage(Post postItem)
      {
         return postItem.ErrorMessage ?? "No errors";
      }

      protected static string GetPureUri(Post postItem)
      {
         return AutopostUtils.HighlightLinks(postItem.PureUri);
      }

      protected static string GetPostText(Post postItem)
      {
         return AutopostUtils.HighlightLinks(postItem.PostText);
      }
   }
}