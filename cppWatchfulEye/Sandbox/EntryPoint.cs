﻿/**
 * Вставка логотипов в БД
 */

using System;

namespace InsertLogoSandbox
{
   internal static class EntryPoint
   {
      private static void Main()
      {
         var logosSaver = new BatchLogosSaver(DatabaseType.CppQuiz);
         var affected = logosSaver.Go();
         Console.WriteLine("Affected: {0}", affected);
      }
   }
}