﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SocialContent.ascx.cs"
    Inherits="WatchfulEye.Web.Controls.SocialContent" %>
<table>
    <tr>
        <td>&nbsp;</td>
        <td>
            <!--Twitter-->
            <div style="float: left; width: 120px;">
                <a href="https://twitter.com/share" class="twitter-share-button"
                    data-via=""
                    data-url="http://q.viva64.com"
                    data-size="large"
                    data-text="Test your C++ skills - find bugs in popular open-source projects!">Tweet</a>
                <script type="text/javascript">
                    !function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                        if (!d.getElementById(id)) {
                            js = d.createElement(s);
                            js.id = id;
                            js.src = p + '://platform.twitter.com/widgets.js';
                            fjs.parentNode.insertBefore(js, fjs);
                        }
                    }(document, 'script', 'twitter-wjs');
                </script>
            </div>
            <!--Google+-->
            <div style="float: left; width: 120px;">
                <div class="g-plusone" data-href="<%= PostLevelUrl %>"></div>                
                <script type="text/javascript">
                    window.___gcfg = { lang: 'en', parsetags: 'onload' };
                    (function () {
                        var po = document.createElement('script');
                        po.type = 'text/javascript';
                        po.async = true;
                        po.src = 'https://apis.google.com/js/platform.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(po, s);
                    })();
                </script>
            </div>
            <!--Facebook-->
            <div style="float: left;">
                <div class="fb-like" data-href="<%= PostLevelUrl %>"
                    data-layout="standard" data-action="like" data-show-faces="true" data-share="true">
                </div>
            </div>
        </td>
    </tr>
</table>
