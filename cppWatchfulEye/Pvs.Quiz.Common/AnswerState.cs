﻿namespace Pvs.Quiz.Common
{
   /// <summary>
   /// Статус ответа
   /// </summary>
   public enum AnswerState
   {
      /// <summary>
      /// Не знает ответа
      /// </summary>
      DontKnow,

      /// <summary>
      /// Ответ верен
      /// </summary>
      Right,

      /// <summary>
      /// Ответ неверен
      /// </summary>
      Wrong,

      /// <summary>
      /// Время вышло
      /// </summary>
      TimeIsUp
   }
}