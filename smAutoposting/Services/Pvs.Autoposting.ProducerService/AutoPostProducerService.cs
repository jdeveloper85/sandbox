﻿using System.ServiceProcess;
using Pvs.Autopost.Config;
using Pvs.Autopost.Core;
using Pvs.Autoposting.ProducerServer;

namespace Pvs.Autoposting.ProducerService
{
   public partial class AutoPostProducerService : ServiceBase
   {
      private AutopostProducer _autopostProducer;

      public AutoPostProducerService()
      {
         InitializeComponent();
      }

      protected override void OnStart(string[] args)
      {
         _autopostProducer = new AutopostProducer(SocialKeysReader.PostLimit, SocialKeysReader.PollingInterval,
            AutopostLoggerWrapper.GetInstance(Loggers.Autoposter));
         _autopostProducer.Start();
      }

      protected override void OnStop()
      {
         _autopostProducer.Stop();
      }

      protected override void OnPause()
      {
         _autopostProducer.Suspend();
      }

      protected override void OnContinue()
      {
         _autopostProducer.Resume();
      }
   }
}