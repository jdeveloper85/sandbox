USE [1gb_watchfuleye] -- CppQuiz
GO

IF EXISTS (
       SELECT *
       FROM   INFORMATION_SCHEMA.TABLES
       WHERE  TABLE_NAME = 'QuizQuestion'
   )
    DROP TABLE QuizQuestion
GO

IF EXISTS (
       SELECT *
       FROM   INFORMATION_SCHEMA.TABLES
       WHERE  TABLE_NAME = 'QuizQuestionStat'
   )
    DROP TABLE QuizQuestionStat
GO

CREATE TABLE QuizQuestion
(
	[QuestionId]           INT IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[CodeFragment]         VARCHAR(4096) NOT NULL,
	[ProjectName]          NVARCHAR(64) NOT NULL,
	[ProjectLogo]          VARBINARY(MAX) NULL,
	[ExplanatoryText]      NVARCHAR(MAX) NULL,
	[PvsStudioWarning]     NVARCHAR(MAX) NULL
)

CREATE TABLE QuizQuestionStat
(
	QuestionStatId     INT IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	QuestionId         INT NOT NULL,
	DontKnowCnt        BIGINT NOT NULL DEFAULT 0,
	RightCnt           BIGINT NOT NULL DEFAULT 0,
	WrongCnt           BIGINT NOT NULL DEFAULT 0,
	TimeIsUpCnt        BIGINT NOT NULL DEFAULT 0,
	CONSTRAINT FK_QuestionStat_Question FOREIGN KEY(QuestionId) REFERENCES 
	QuizQuestion(QuestionId) ON DELETE CASCADE ON UPDATE CASCADE
)