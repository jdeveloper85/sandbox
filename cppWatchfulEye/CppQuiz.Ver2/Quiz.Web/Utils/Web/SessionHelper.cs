﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;
using System.Web.SessionState;
using Pvs.Quiz.Common;

namespace Quiz.Web.Utils.Web
{
   /// <summary>
   ///    Типобезопасная обвертка над сеансом
   /// </summary>
   public sealed class SessionHelper
   {
      private static SessionHelper _sessionHelperInstance;
      private readonly HttpSessionState _httpSessionState;

      private SessionHelper(HttpSessionState httpSessionState)
      {
         _httpSessionState = httpSessionState;
         Initialize();
      }

      /// <summary>
      ///    Текущий номер вопроса
      /// </summary>
      public int CurrentQuestionNumber
      {
         get { return InternalSessionHelper.Get<int>(_httpSessionState, SessionKey.CurrentQuestionNumber); }
         set { InternalSessionHelper.Set(_httpSessionState, SessionKey.CurrentQuestionNumber, value); }
      }

      /// <summary>
      ///    Текущий список правильных индексов для сегментов С++-кода
      /// </summary>
      public IList<int> CurrentCorrectIndices
      {
         get { return InternalSessionHelper.Get<IList<int>>(_httpSessionState, SessionKey.CurrentCorrectIndices); }
         set { InternalSessionHelper.Set(_httpSessionState, SessionKey.CurrentCorrectIndices, value); }
      }

      /// <summary>
      ///    Счет по текущему состоянию ответов на вопросы
      /// </summary>
      public IDictionary<AnswerState, int> AnswerStat
      {
         get
         {
            return InternalSessionHelper.Get<IDictionary<AnswerState, int>>(_httpSessionState, SessionKey.AnswerStat);
         }
         private set { InternalSessionHelper.Set(_httpSessionState, SessionKey.AnswerStat, value); }
      }

      /// <summary>
      ///    Очередь доступных оставшихся идентификаторов
      ///    <remarks>Нужно для случайного порядка</remarks>
      /// </summary>
      public Queue<int> QuestionIds
      {
         get { return InternalSessionHelper.Get<Queue<int>>(_httpSessionState, SessionKey.AvailableQuestionIds); }
         private set { InternalSessionHelper.Set(_httpSessionState, SessionKey.AvailableQuestionIds, value); }
      }

      /// <summary>
      ///    Карта {Номер вопроса, Время поступления вопроса}
      /// </summary>
      private IDictionary<int, DateTime> TimeoutPool
      {
         get
         {
            return InternalSessionHelper.Get<IDictionary<int, DateTime>>(_httpSessionState, SessionKey.TimeoutPool);
         }
         set { InternalSessionHelper.Set(_httpSessionState, SessionKey.TimeoutPool, value); }
      }

      private void Initialize()
      {
         var questionQueue =
            new Queue<int>(RawQuizSqlUtils.GetRandomQuestionIds(WebConfigReader.TestQuestionsNumber,
               WebConfigurationManager.ConnectionStrings["QuizConnection"].ConnectionString));
         questionQueue.TrimExcess();
         CurrentQuestionNumber = 1;
         IDictionary<AnswerState, int> answerStat = new Dictionary<AnswerState, int>();
         foreach (var state in Enum.GetValues(typeof(AnswerState)).Cast<AnswerState>())
         {
            answerStat.Add(state, 0);
         }

         AnswerStat = answerStat;
         QuestionIds = questionQueue;
         TimeoutPool = new Dictionary<int, DateTime>();
      }

      /// <summary>
      ///    Кэширующий Factory-метод
      /// </summary>
      /// <param name="httpSessionState">Состояние сеанса</param>
      /// <returns>Одиночный экземпляр класса</returns>
      public static SessionHelper GetOrCreate(HttpSessionState httpSessionState)
      {
         return _sessionHelperInstance ?? (_sessionHelperInstance = new SessionHelper(httpSessionState));
      }

      /// <summary>
      ///    Получает оставшееся время на вопрос
      /// </summary>
      /// <param name="questionId">Идентификатор вопроса</param>
      /// <returns>Оставшееся время на вопрос</returns>
      public TimeSpan GetTimeRemaining(int questionId)
      {
         if (!TimeoutPool.ContainsKey(questionId))
         {
            TimeoutPool.Add(questionId, DateTime.Now);
         }

         return DateTime.Now.Subtract(TimeoutPool[questionId]);
      }

      public void Clear()
      {
         CurrentQuestionNumber = 0;
         CurrentCorrectIndices?.Clear();
         AnswerStat?.Clear();
         QuestionIds?.Clear();
         TimeoutPool?.Clear();
         _sessionHelperInstance = null;
      }

      #region Служебные типы

      private static class InternalSessionHelper
      {
         public static void Set(HttpSessionState session, SessionKey key, object value)
         {
            session[Enum.GetName(typeof(SessionKey), key)] = value;
         }

         public static T Get<T>(HttpSessionState session, SessionKey key)
         {
            var dataValue = session[Enum.GetName(typeof(SessionKey), key)];
            return dataValue is T ? (T)dataValue : default(T);
         }
      }

      private enum SessionKey
      {
         CurrentQuestionNumber,
         CurrentCorrectIndices,
         AnswerStat,
         AvailableQuestionIds,
         TimeoutPool
      }

      #endregion
   }
}