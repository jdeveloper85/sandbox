﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using CppQuiz.Data;
using Quiz.Web.Infrastructure.Abstract;

namespace Quiz.Web.Infrastructure.Concrete
{
   public sealed class CrudQuizEntityRepository : ICrudQuestionRepository<QuizQuestion>
   {
      private readonly QuizEntities _quizEntities;

      public CrudQuizEntityRepository()
      {
         _quizEntities = new QuizEntities();
      }

      public async Task<IEnumerable<QuizQuestion>> GetAllAsync()
      {
         return await (from q in _quizEntities.QuizQuestions
                       select q).ToArrayAsync();
      }

      public async Task<QuizQuestion> GetAsync(int questionId)
      {
         return await _quizEntities.QuizQuestions.FindAsync(questionId);
      }

      public async Task<QuizQuestion> AddAsync(QuizQuestion question)
      {
         var newQuestion = _quizEntities.QuizQuestions.Add(question);
         var affected = await _quizEntities.SaveChangesAsync();
         return affected > 0 ? newQuestion : QuizQuestion.Empty;
      }

      public async Task<bool> UpdateAsync(QuizQuestion question)
      {
         var quizQuestion = await _quizEntities.QuizQuestions.FindAsync(question.QuestionId);
         if (quizQuestion == null)
            return await AddAsync(question) != QuizQuestion.Empty;

         quizQuestion.CodeFragment = question.CodeFragment;
         quizQuestion.ProjectName = question.ProjectName;
         quizQuestion.ProjectLogo = question.ProjectLogo;
         quizQuestion.ExplanatoryText = question.ExplanatoryText;
         quizQuestion.PvsStudioWarning = question.PvsStudioWarning;

         return await _quizEntities.SaveChangesAsync() > 0;
      }

      public async Task<bool> DeleteAsync(int questionId)
      {
         var quizQuestion = _quizEntities.QuizQuestions.Find(questionId);
         if (quizQuestion == null)
            return false;

         _quizEntities.QuizQuestions.Remove(quizQuestion);
         return await _quizEntities.SaveChangesAsync() > 0;
      }

      public void Dispose()
      {
         _quizEntities.Dispose();
      }
   }
}