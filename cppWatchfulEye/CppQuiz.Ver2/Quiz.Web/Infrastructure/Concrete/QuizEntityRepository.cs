﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using CppQuiz.Data;
using Pvs.Quiz.Common;
using Quiz.Web.Infrastructure.Abstract;

namespace Quiz.Web.Infrastructure.Concrete
{
   public class QuizEntityRepository : IQuizRepository<QuizQuestion>
   {
      private readonly QuizEntities _quizContext;

      public QuizEntityRepository()
      {
         _quizContext = new QuizEntities();
      }

      public void Dispose()
      {
         _quizContext.Dispose();
      }

      public async Task<QuizQuestion> GetQuestionAsync(int questionId)
      {
         return await _quizContext.QuizQuestions.FindAsync(questionId);
      }

      public async Task<byte[]> GetProjectLogoAsync(int questionId)
      {
         return await (from question in _quizContext.QuizQuestions
                       where question.QuestionId == questionId
                       select question.ProjectLogo).FirstOrDefaultAsync();
      }

      public async Task<bool> UpdateQuestionStatAsync(int questionId, AnswerState answerState)
      {
         var quizQuestion = await GetQuestionAsync(questionId);
         if (quizQuestion == null)         
            return false;         

         var questionStat = quizQuestion.QuizQuestionStats.FirstOrDefault();
         if (questionStat == null)
         {
            questionStat = new QuizQuestionStat
            {
               QuestionId = quizQuestion.QuestionId,
               DontKnowCnt = 0L,
               WrongCnt = 0L,
               RightCnt = 0L,
               TimeIsUpCnt = 0L
            };

            _quizContext.QuizQuestionStats.Add(questionStat);
         }

         switch (answerState)
         {
            case AnswerState.DontKnow:
               questionStat.DontKnowCnt++;
               break;

            case AnswerState.Right:
               questionStat.RightCnt++;
               break;

            case AnswerState.TimeIsUp:
               questionStat.TimeIsUpCnt++;
               break;

            case AnswerState.Wrong:
               questionStat.WrongCnt++;
               break;
         }

         return await _quizContext.SaveChangesAsync() > 0;
      }
   }
}