USE [1gb_x_email20b]
GO
/****** Object:  DatabaseRole [aspnet_Membership_FullAccess]    Script Date: 19.02.2015 15:21:39 ******/
CREATE ROLE [aspnet_Membership_FullAccess]
GO
ALTER ROLE [aspnet_Membership_ReportingAccess] ADD MEMBER [aspnet_Membership_FullAccess]
GO
ALTER ROLE [aspnet_Membership_BasicAccess] ADD MEMBER [aspnet_Membership_FullAccess]
GO
