﻿using System;
using System.Web.Routing;
using System.Web.UI;
using Pvs.Quiz.Common;
using WatchfulEye.DataLayer;
using WatchfulEye.Web.Model;
using WatchfulEye.Web.Pages.Helpers;

namespace WatchfulEye.Web.Controls
{
    /// <summary>
    ///    Элемент управления для визулизации одного вопроса теста
    /// </summary>
    public partial class TestRecord : UserControl
    {
        private readonly CodeSegmentPreprocessor _segmentPreprocessor = new CodeSegmentPreprocessor();

        /// <summary>
        ///    Номер вопроса
        /// </summary>
        public int QuestionNumber
        {
            private get
            {
                object numberObj = ViewState["QuestionNumber"];
                int questionNumber;
                return numberObj == null || string.IsNullOrEmpty(numberObj.ToString())
                       || !int.TryParse(numberObj.ToString(), out questionNumber)
                   ? -1
                   : questionNumber;
            }
            set
            {
                ViewState["QuestionNumber"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                VirtualPathData rootVirtualPathData =
                   RouteTable.Routes.GetVirtualPath(null, "RootRoute", new RouteValueDictionary());
                if (rootVirtualPathData == null) return;
                string rootVirtualPath = rootVirtualPathData.VirtualPath;

                QuestionsRandomOrder randomOrder = SessionHelper.GetQuestionsRandomOrder(Session);
                int questionId = randomOrder[QuestionNumber];
                Question question = WatchfulEyeRepository.GetQuestion(questionId);
                if (question == Question.EmptyQuestion)
                {
                    Response.RedirectToRoute("StartRoute");
                    return;
                }

                projectLogo.InnerHtml = question.ProjectLogo != null
                   ? string.Format("<img src='{0}GetImageHandler.ashx?{1}={2}' alt='{3}' title='{3}' />",
                      rootVirtualPath, GetImageHandler.QuestionIdQueryStringName, questionId, question.ProjectName)
                   : string.Format("<img src='{0}Images/no_logo.png' alt='No Logo'>", rootVirtualPath);

                projectName.InnerText = question.ProjectName;
                currentQuestionNumber.InnerText =
                   string.Format("{0} of {1} questions", QuestionNumber, WebConfigReader.TestQuestionsNumber);
                codeFragment.InnerHtml = string.Format("<pre>{0}</pre>",
                   _segmentPreprocessor.Decorate(question.CodeFragment));

                explanatoryText.InnerHtml = question.ExplanatoryText;
                pvsWarning.InnerHtml += question.PvsStudioWarning;
            }
        }
    }
}