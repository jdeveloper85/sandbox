﻿CREATE TABLE Account
(
	AccountId     INT IDENTITY(1, 1) NOT NULL,
	UserName      VARCHAR(256) NOT NULL,
	CONSTRAINT Account_AccountId_PK PRIMARY KEY(AccountId),
	CONSTRAINT Account_UserName_UNIQUE UNIQUE(UserName)
)