﻿using System;

namespace Pvs.Quiz.Common
{
   [AttributeUsage(AttributeTargets.All, Inherited = false)]
   public sealed class LevelImageAttribute : Attribute
   {
      public LevelImageAttribute(string imagePath)
      {
         ImagePath = imagePath;
      }

      public string ImagePath { get; private set; }
   }
}