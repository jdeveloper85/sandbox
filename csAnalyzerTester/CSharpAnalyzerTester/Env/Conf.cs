﻿//  2006-2008 (c) Viva64.com Team
//  2008-2016 (c) OOO "Program Verification Systems"
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Windows.Forms;
using CSharpAnalyzerTester.Helpers;
using CSharpAnalyzerTester.Properties;
using Microsoft.CSharp.RuntimeBinder;

namespace CSharpAnalyzerTester.Env
{
    public interface IConf
    {
        string AnalyzerImagePath { get; }
        int ConcurrencyLevel { get; }
        bool EnableBenchmark { get; }
        int MaximumAnalysisTime { get; }
    }

    public sealed class ConfImpl : IConf
    {
        public string AnalyzerImagePath
        {
            get
            {
                dynamic dynConf = SingletonInjector<DynConf>.Instance;
                try
                {
                    return dynConf.AnalyzerImagePath;
                }
                catch (RuntimeBinderException)
                {
                    MessageBox.Show(Resources.SpecifyAnalyzerImagePath, Resources.PvsStudioCsExeNotFound,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Environment.Exit(1);
                    throw;
                }
            }
        }

        public int ConcurrencyLevel
        {
            get
            {
                dynamic dynConf = SingletonInjector<DynConf>.Instance;
                try
                {
                    int concurrencyLevel;
                    return int.TryParse(dynConf.ConcurrencyLevel, out concurrencyLevel)
                        ? concurrencyLevel
                        : Environment.ProcessorCount;
                }
                catch (RuntimeBinderException )
                {
                    return 1;
                }
            }
        }

        public bool EnableBenchmark
        {
            get
            {
                dynamic dynConf = SingletonInjector<DynConf>.Instance;
                try
                {
                    bool isBenchmarkEnable;
                    return bool.TryParse(dynConf.EnableBenchmark, out isBenchmarkEnable) && isBenchmarkEnable;
                }
                catch (RuntimeBinderException)
                {
                    return false;
                }
            }
        }

        public int MaximumAnalysisTime
        {
            get
            {
                dynamic dynConf = SingletonInjector<DynConf>.Instance;
                const int defaultTimeout = 90;
                try
                {
                    int timeout;
                    return int.TryParse(dynConf.MaximumAnalysisTime, out timeout)
                        ? timeout
                        : defaultTimeout;
                }
                catch (RuntimeBinderException)
                {
                    return defaultTimeout;
                }
            }
        }
    }

    public sealed class DynConf : DynamicObject
    {
        private readonly AppSettingsReader _appSettingsReader;
        private readonly Dictionary<string, dynamic> _configValues = new Dictionary<string, dynamic>();

        public DynConf()
        {
            _appSettingsReader = new AppSettingsReader();
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            bool success;
            if (_configValues.ContainsKey(binder.Name))
            {
                result = _configValues[binder.Name];
                success = true;
            }
            else
            {
                success = TryGet(binder.Name, out result);
                if (success) _configValues.Add(binder.Name, result);
            }

            return success;
        }

        private T Get<T>(string key)
        {
            try
            {
                return (T)_appSettingsReader.GetValue(key, typeof(T));
            }
            catch (InvalidOperationException invOpEx)
            {
                throw new InvalidSettingsException(
                    string.Format("Key {0} does not exists in App.config or incompatible type", invOpEx));
            }
            catch (ArgumentNullException argNullEx)
            {
                throw new InvalidSettingsException(string.Format("Key {0} does not found", argNullEx));
            }
        }

        private bool TryGet<T>(string key, out T configValue)
        {
            try
            {
                configValue = Get<T>(key);
                return true;
            }
            catch (InvalidSettingsException)
            {
                configValue = default(T);
                return false;
            }
        }        
    }
}