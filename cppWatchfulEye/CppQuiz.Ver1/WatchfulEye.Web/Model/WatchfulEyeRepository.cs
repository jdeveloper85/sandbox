﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Configuration;
using WatchfulEye.DataLayer;

namespace WatchfulEye.Web.Model
{
    /// <summary>
    ///     Бизнес-логика с БД
    /// </summary>
    public sealed class WatchfulEyeRepository
    {
        private readonly string _watchfulEyeConnectionString =
            WebConfigurationManager.ConnectionStrings["WatchfulEye"].ConnectionString;

        /// <summary>
        ///     Получение O/R-сущности для вопроса по номеру
        /// </summary>
        /// <param name="questionId">Номер вопроса (QuestionId)</param>
        /// <returns>O/R-Сущность</returns>
        public static Question GetQuestion(int questionId)
        {
            using (var eyeEntities = new WatchfulEyeEntities())
            {
                var questionRecord = (from question in eyeEntities.Questions
                                      where question.QuestionId == questionId
                                      select question).FirstOrDefault();
                return questionRecord ?? Question.EmptyQuestion;
            }
        }

        /// <summary>
        ///     Получение массива байтов для изображения логотипа
        /// </summary>
        /// <param name="questionId">Номер вопроса (QuestionId)</param>
        /// <returns>Массив байтов изображения</returns>
        public static byte[] GetImageLogo(int questionId)
        {
            using (var eyeEntities = new WatchfulEyeEntities())
            {
                return (from question in eyeEntities.Questions
                        where question.QuestionId == questionId
                        select question.ProjectLogo).FirstOrDefault();
            }
        }

        /// <summary>
        ///     Получение случайного набора идентификаторов QuestionId из БД
        /// </summary>
        /// <param name="aTopLimit">Требуемое кол-во идентификаторов</param>
        /// <returns>Случайный набор идентификаторов QuestionId из БД</returns>
        public int[] RandomQuestionIds(int aTopLimit)
        {
            var randomIds = new List<int>();

            using (var sqlConnection = new SqlConnection(_watchfulEyeConnectionString))
            {
                sqlConnection.Open();
                using (
                    var randomIdsCommand =
                        new SqlCommand(
                            string.Format("SELECT TOP {0} QuestionId FROM Question ORDER BY NEWID()", aTopLimit),
                            sqlConnection))
                {
                    var sqlDataReader = randomIdsCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        randomIds.Add((int)sqlDataReader[0]);
                    }
                }

                return randomIds.ToArray();
            }
        }

        /// <summary>
        ///     Обновление существующего или вставка нового вопроса
        /// </summary>
        /// <param name="aQuestion">Вопрос</param>
        /// <returns>Кол-во обновленных записей</returns>
        public static void Save(Question aQuestion)
        {
            using (var eyeEntities = new WatchfulEyeEntities())
            {
                if (aQuestion.QuestionId == 0)
                {
                    eyeEntities.Questions.Add(aQuestion);
                }
                else
                {
                    var foundQuestion = eyeEntities.Questions.Find(aQuestion.QuestionId);
                    if (foundQuestion == null)
                    {
                        eyeEntities.SaveChanges();
                        return;
                    }

                    foundQuestion.CodeFragment = aQuestion.CodeFragment;
                    foundQuestion.ExplanatoryText = aQuestion.ExplanatoryText;
                    if (aQuestion.ProjectLogo != null)
                        foundQuestion.ProjectLogo = aQuestion.ProjectLogo;

                    foundQuestion.ProjectName = aQuestion.ProjectName;
                    foundQuestion.PvsStudioWarning = aQuestion.PvsStudioWarning;
                }

                eyeEntities.SaveChanges();
            }
        }

        /// <summary>
        ///     Список вопросов
        /// </summary>
        /// <returns>Список вопросов</returns>
        public static IEnumerable<Question> GetQuestions()
        {
            using (var eyeEntities = new WatchfulEyeEntities())
            {
                return (from question in eyeEntities.Questions
                        select question).ToArray();
            }
        }

        /// <summary>
        ///     Удаление вопроса
        /// </summary>
        /// <param name="aQuestion">Вопрос для удаления</param>
        /// <returns>Кол-во удаленных записей</returns>
        public static void Delete(Question aQuestion)
        {
            using (var eyeEntities = new WatchfulEyeEntities())
            {
                var questionToDelete = eyeEntities.Questions.Find(aQuestion.QuestionId);
                if (questionToDelete != null)
                {
                    eyeEntities.Questions.Remove(questionToDelete);
                }

                eyeEntities.SaveChanges();
            }
        }
    }
}