USE [1gb_x_email20b]
GO
/****** Object:  DatabaseRole [aspnet_Roles_FullAccess]    Script Date: 19.02.2015 15:21:39 ******/
CREATE ROLE [aspnet_Roles_FullAccess]
GO
ALTER ROLE [aspnet_Roles_ReportingAccess] ADD MEMBER [aspnet_Roles_FullAccess]
GO
ALTER ROLE [aspnet_Roles_BasicAccess] ADD MEMBER [aspnet_Roles_FullAccess]
GO
