﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;
using CppQuiz.Data;
using Quiz.Web.Infrastructure;
using Quiz.Web.Infrastructure.Abstract;
using Quiz.Web.Infrastructure.Concrete;
using Quiz.Web.Utils.Web;

namespace Quiz.Web
{
   /// <summary>
   ///    Получение логотипа изображения проекта для текущего вопроса
   /// </summary>
   public class GetLogoHandler : HttpTaskAsyncHandler, IRequiresSessionState
   {
      private const string NoLogoPath = "Pics/no_logo.png";

      private readonly IQuizRepository<QuizQuestion> _quizRepository = new AppCachedRepository(new QuizEntityRepository())
      {
         QuizQuestions = AppCacheHelper.GetQuestions(HttpContext.Current.Cache)
      };

      public override async Task ProcessRequestAsync(HttpContext context)
      {
         context.Response.Clear();
         context.Response.ContentType = "image/png";
         context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
         context.Response.Cache.SetExpires(DateTime.MinValue);

         var httpContext = HttpContext.Current;
         var oQuestionId = httpContext.Request.QueryString["questionId"];

         if (httpContext.Session != null)
         {
            int questionId;
            if (int.TryParse(oQuestionId, out questionId))
            {
               var projectLogoData = await _quizRepository.GetProjectLogoAsync(questionId);
               if (projectLogoData != null)
               {
                  context.Response.BinaryWrite(projectLogoData);
               }
               else
               {
                  context.Response.TransmitFile(NoLogoPath);
               }
            }
            else
            {
               context.Response.TransmitFile(NoLogoPath);
            }
         }
         else
         {
            context.Response.TransmitFile(NoLogoPath);
         }
      }
   }
}