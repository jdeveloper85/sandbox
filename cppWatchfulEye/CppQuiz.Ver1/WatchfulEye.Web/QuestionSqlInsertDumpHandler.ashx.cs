﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace WatchfulEye.Web
{
    /// <summary>
    ///     HttpHandler для генерации текущих операторов INSERT для таблицы Question
    /// </summary>
    public class QuestionSqlInsertDumpHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            var gbServer = new Server(new ServerConnection
            {
                ConnectionString = WebConfigurationManager.ConnectionStrings["WatchfulEye"].ConnectionString
            });
            gbServer.ConnectionContext.Connect();
            var watchfulEyeDb = gbServer.Databases["1gb_watchfuleye"];
            var dumpBuilder = new StringBuilder();

            foreach (var line in from Table table in watchfulEyeDb.Tables
                                 let tblScripter = new Scripter(gbServer) { Options = new ScriptingOptions { ScriptData = true } }
                                 select tblScripter.EnumScript(new SqlSmoObject[] { table })
                                     into insertScript
                                     from line in insertScript
                                     select line)
            {
                dumpBuilder.AppendLine(line);
            }

            context.Response.Clear();
            context.Response.ClearHeaders();
            context.Response.ClearContent();
            context.Response.Cache.SetCacheability(HttpCacheability.Public);
            context.Response.Cache.SetExpires(DateTime.MinValue);
            context.Response.ContentType = "application/octet-stream";
            context.Response.AddHeader("Content-Disposition",
                string.Format("attachment; filename=WatchfulEye_BackUp-{0}.sql",
                    DateTime.Now.ToString("u")));
            var dumpBuffer = GetBytes(dumpBuilder.ToString());
            context.Response.OutputStream.Write(dumpBuffer, 0, dumpBuffer.Length);
            context.Response.Flush();
        }

        public bool IsReusable
        {
            get { return false; }
        }

        private static byte[] GetBytes(string aString)
        {
            var bytes = new byte[aString.Length * sizeof(char)];
            Buffer.BlockCopy(aString.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}