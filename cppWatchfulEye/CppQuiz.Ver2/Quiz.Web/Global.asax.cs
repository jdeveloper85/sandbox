﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Optimization;
using System.Web.Routing;
using Quiz.Web.Utils;
using Quiz.Web.Utils.Web;

namespace Quiz.Web
{
   public class Global : HttpApplication
   {
      protected void Application_Start(object sender, EventArgs e)
      {
         GlobalConfiguration.Configure(WebApiConfig.Register);
         BundleConfig.RegisterBundles(BundleTable.Bundles);
         RouteConfig.RegisterRoutes(RouteTable.Routes);         
         
         AppCacheHelper.AddQuestions(Context.Cache, RawQuizSqlUtils.GetQuestions());         
      }

      protected void Session_Start()
      {
         SessionHelper.GetOrCreate(HttpContext.Current.Session);
      }

      protected void Session_End()
      {
         var sessionHelper = SessionHelper.GetOrCreate(HttpContext.Current.Session);
         sessionHelper.Clear();
      }
   }
}