﻿USE [1gb_watchfuleye]
GO

BEGIN TRANSACTION;

-- 1
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
static ShiftResult shift(....)
{
  ...
  qreal l = (orig->x1 - orig->x2)*(orig->x1 - orig->x2) +
            (orig->y1 - orig->y2)*(orig->y1 - orig->@y1@) *
            (orig->x3 - orig->x4)*(orig->x3 - orig->x4) +
            (orig->y3 - orig->y4)*(orig->y3 - orig->y4);
  qreal dot = (orig->x1 - orig->x2)*(orig->x3 - orig->x4) +
              (orig->y1 - orig->y2)*(orig->y3 - orig->y4);
  ...
}',
N'Qt',
N'Опечатка. Переменная orig->y1 вычитается сама из себя.
Должно быть Написано:
<pre>(orig->y1 - orig->y2)*(orig->y1 - orig->y2) *</pre>',
N'<a href="http://www.viva64.com/en/d/0090/">V501</a> There are identical sub-expressions to the left and to the right of the "-" operator: orig->y1 - orig->y1 QtGui qbezier.cpp 329'
  )
GO
-- 2
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
    N'
QQuickJSContext2DPrototype::method_getImageData(....)
{
  ....
  qreal x = ctx->callData->args[0].toNumber();
  qreal y = ctx->callData->args[1].toNumber();
  qreal w = ctx->callData->args[2].toNumber();
  qreal h = ctx->callData->args[3].toNumber();
  if (!qIsFinite(x) || !qIsFinite(y) ||
      !qIsFinite(w) || !qIsFinite(@w@))
    V4THROW_DOM(DOMEXCEPTION_NOT_SUPPORTED_ERR,
                "getImageData(): Invalid arguments");
  ....
}
',
N'Qt',
N'Опечатка. Забыли проверить переменную "h".
Вместо этого, дважды проверяется переменная "w"',
N'<a href="http://www.viva64.com/en/d/0090/">V501</a> There are identical sub-expressions ''!qIsFinite(w)'' to the left and to the right of the ''||'' operator. qquickcontext2d.cpp 3305'
  )
GO
-- 3
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
void Frustum(....)
{
  float delta_x = right - left;
  float delta_y = top - bottom;
  float delta_z = far_z - near_z;
  if ((near_z <= 0.0f) || (far_z <= 0.0f) ||
      (delta_z <= 0.0f) || (@delta_y@ <= 0.0f) ||
      (@delta_y@ <= 0.0f))
    return;
  ....
}
',
N'Chromium',
N'Опечатка. Забыли проверить значение переменной ''delta_x''.
Вместо этого, два раза проверяется переменная ''delta_y''.',
N'<a href="http://www.viva64.com/en/d/0090/">V501</a> There are identical sub-expressions "(delta_y <= 0.0f)" to the left and to the right of the "||" operator. spinning_cube.cc 207'
  )
GO
-- 4
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
    N'
void GetCacheParameters(ContextType type, FilePath* cache_path,
                        int* max_size) {
  ...
  *max_size = 0;
  if (!base::StringToInt(value, max_size)) {
    *max_size = 0;
  } else if (@max_size@ @<@ 0) {
    *max_size = 0;
  }
  ...
}
',
N'Chromium',
N'Проверятся, что указатель "max_size" меньше нуля.
Это бессмысленная проверка. Забыли разменивать указатель.
Должно быть написано: <pre>} else if (*max_size < 0) {</pre>',
N'<a href="http://www.viva64.com/en/d/0092/">V503</a> This is a nonsensical comparison: pointer < 0.  browser  profile_impl.cc 169'
  )
GO
-- 5
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
    N'
void G4VTwistSurface::GetBoundaryLimit(G4int areacode,
                                       G4double limit[]) const
{
  ....
  if (areacode & @sC0Min1Max@) {
     limit[0] = fAxisMin[0];
     limit[1] = fAxisMin[1];
  } else if (areacode & sC0Max1Min) {
     limit[0] = fAxisMax[0];
     limit[1] = fAxisMin[1];
  } else if (areacode & sC0Max1Max) {
     limit[0] = fAxisMax[0];
     limit[1] = fAxisMax[1];
  } else if (areacode & sC0Min1Max) {
     limit[0] = fAxisMin[0];
     limit[1] = fAxisMax[1];
  }
  ....
}
',
    N'Geant4 software',
    N'
Что-бы ошибка стала заметной,
упростим и перепишем код:
<pre>
     if (a & Min_Max) { Min[0] Min[1] }
else if (a & Max_Min) { Max[0] Min[1] }
else if (a & Max_Max) { Max[0] Max[1] }
else if (a & Min_Max) { Min[0] Max[1] }
</pre>
Теперь стало видно, что везде, кроме первой строки,
соблюдается соответствие между именем константы в
условии и именами массивов. В первом условии,
следовало использовать константу sC0Min1Min.',
    N'<a href="http://www.viva64.com/en/d/0106/">V517</a> The use of ''if (A) {...} else if (A) {...}'' pattern was detected. There is a probability of logical error presence. Check lines: 793, 802. G4specsolids g4vtwistsurface.cc 793'
  );
GO
-- 6
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
void Q1_AllocMaxBSP(void)
{
  ...
  q1_allocatedbspmem +=
    Q1_MAX_MAP_CLIPNODES * sizeof(q1_dclipnode_t);
  ...
  q1_allocatedbspmem +=
    Q1_MAX_MAP_EDGES@,@ sizeof(q1_dedge_t);
  ...
  q1_allocatedbspmem +=
    Q1_MAX_MAP_MARKSURFACES * sizeof(unsigned short);
  ...
}
',
N'Quake-III-Arena',
N'
Опечатка. Вместо умножения используется оператор запятая.
Поясним для тех, кто плохо знаком с этим оператором.
Операция запятая имеет самый низкий приоритет среди
всех операций языка Си++. У этой операции есть 2 операнда
(левый и правый). Вначале вычисляется левый операнд,
затем правый, а в качестве результата возвращается правый
операнд.
Пример использования:
<pre>
if (x)
  return Foo(), N;
</pre>
Чтобы не использовать фигурные скобки, использован
оператор ",". Будет вызвана функция Foo().
Затем, функция вернёт значение переменной N.
Это пример. Мы не считаем, это хорошим стилем.
Лучше не экономить на скобках и написать более понятный код:
<pre>
if (x) {
  Foo();
  return N;
}
</pre>
В нашем случае, из-за запятой, выражение:
<pre>q1_allocatedbspmem += Q1_MAX_MAP_EDGES , sizeof(q1_dedge_t);</pre>
будет эквивалентно выражению:
<pre>q1_allocatedbspmem += sizeof(q1_dedge_t);</pre>',
N'<a href="http://www.viva64.com/en/d/0110/">V521</a> Such expressions using the "," operator are dangerous. Make sure the expression is correct. l_bsp_q1.c 136'
  )
GO
-- 7
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
static int rr_cmp(uchar *a,uchar *b)
{
  if (a[0] != b[0])
    return (int) a[0] - (int) b[0];
  if (a[1] != b[1])
    return (int) a[1] - (int) b[1];
  if (a[2] != b[2])
    return (int) a[2] - (int) b[2];
  if (a[3] != b[3])
    return (int) a[3] - (int) b[3];
  if (a[4] != b[4])
    return (int) a[4] - (int) b[4];
  if (a[5] != b[5])
    return (int) a[@1@] - (int) b[5];
  if (a[6] != b[6])
    return (int) a[6] - (int) b[6];
  return (int) a[7] - (int) b[7];
}
',
N'MySQL',
N'
Последствия Copy-Paste.
В одной из скопированных строк, забыли заменить 1 на 5.
',
N'<a href="http://www.viva64.com/en/d/0114/">V525</a> The code containing the collection of similar blocks. Check items "0", "1", "2", "3", "4", "1", "6" in lines 680, 682, 684, 689, 691, 693, 695. sql records.cc 680'
  )
GO
-- 8
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
    N'
void idSurface_Polytope::FromPlanes(....)
{
  ...
  for (j = 0; j < w.GetNumPoints(); j++) {
    for (k = 0; k < verts.Num(); @j@++) {
      if (verts[k].xyz.Compare(w[j].ToVec3(),
                                POLYTOPE_VERTEX_EPSILON)) {
        break;
      }
    }
    if (k >= verts.Num()) {
      newVert.xyz = w[j].ToVec3();
      k = verts.Append(newVert);
    }
    windingVerts[j] = k;
  }
  ...
}
',
    N'Doom 3',
    N'
Опечатка.
Во втором цикле увеличивается переменная "j",
а следовало увеличивать переменную "k".
',
    N'<a href="http://www.viva64.com/en/d/0122/">V533</a> It is likely that a wrong variable is being incremented inside the "for" operator. Consider reviewing "j". surface_polytope.cpp 65'
  )
GO
-- 9
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
class ALIGN16 FourVectors
{
public:
  fltx4 x, y, z;
  ....
};

FourVectors BackgroundColor;

void RayTracingEnvironment::RenderScene(....)
{
  ....
  intens.x = OrSIMD(AndSIMD(BackgroundColor.x,no_hit_mask),
                  AndNotSIMD(no_hit_mask,intens.x));
  intens.y = OrSIMD(AndSIMD(BackgroundColor.y,no_hit_mask),
                  AndNotSIMD(no_hit_mask,intens.y));
  intens.z = OrSIMD(AndSIMD(BackgroundColor.@y@,no_hit_mask),
                  AndNotSIMD(no_hit_mask,intens.z));

  ....
}
',
N'Source Engine SDK',
N'
Последствия Copy-Paste.
В скопированной строке, забыли в одном месте
заменить символ "y" на "z"
',
N'<a href="http://www.viva64.com/en/d/0126/">V537</a> Consider reviewing the correctness of "y" item"s usage. Raytrace trace2.cpp 189'
  )
GO
-- 10
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
void addAttribute(....)
{
  ...
  int index = _snprintf(temp, 1023, 
    "%02x%02x:%02x%02x:%02x%02x:%02x%02x:"
    "%02x%02x:@02@@x@%02x:%02x%02x:%02x%02x",
    value[0], value[1], value[2], value[3], value[4],
    value[5], value[6], value[7], value[8],
    value[9], value[10], value[11], value[12],
    value[13], value[14], value[15]);
  ...
}
',
N'Intel AMT SDK',
N'
Опечатка.
Пропустили один символ %.
Будет сформирована неправильная строка.
',
N'<a href="http://www.viva64.com/en/d/0176/">V576</a> Incorrect format. A different number of actual arguments is expected while calling _snprintf function. Expected: 18. Present: 19. mod_pvs.cpp 308'
  )
GO
-- 11
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
static int asn1_cb(const char *elem, int len, void *bitstr)
{
  ....
  case ASN1_GEN_FLAG_FORMAT:
  if (!strncmp(vstart, "ASCII", 5))
    arg->format = ASN1_GEN_FORMAT_ASCII;
  else if (!strncmp(vstart, "UTF8", 4))
    arg->format = ASN1_GEN_FORMAT_UTF8;
  else if (!strncmp(vstart, "HEX", 3))
    arg->format = ASN1_GEN_FORMAT_HEX;
  else if (!strncmp(vstart, "BITLIST", @3@))
    arg->format = ASN1_GEN_FORMAT_BITLIST;
  else
  ....
}
',
N'OpenSSL',
N'
Для функции strncmp() следует указать количество сравниваемых
символов. В этом коде, длина строк задаётся числовыми
константами. Код по всей видимости писался с помощью Copy-Paste.
Одну из констант забыли поправить. Поэтому, произведёт
сравнение не с словом "BITLIST", а только с "BIT".
',
N'
<a href="http://www.viva64.com/en/d/0291/">V666</a> Consider inspecting third argument of the function "strncmp". It is possible that the value does not correspond with the length of a string which was passed with the second argument. asn1_gen.c 371'
  )
GO
-- 12
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
class AltOp: public RegExp
{
private:
  RegExp *exp1, *exp2;
  ...
};

uint AltOp::fixedLength()
{
  uint l1 = exp1->fixedLength();
  uint l2 = @exp1@->fixedLength();

  if (l1 != l2 || l1 == ~0u)
    return ~0;

  return l1;
}
',
N'Doom 3',
N'
Простенькая опечатка.
Для инициализации переменной l2
следовало использовать указатель exp2
',
N'<a href="http://www.viva64.com/en/d/0277/">V656</a> Variables are initialized through the call to the same function. Its probably an error or un-optimized code. Consider inspecting the exp1->fixedLength() expression. actions.cc 391'
  )
GO
-- 13
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
template<typename Scalar> EIGEN_DEVICE_FUNC
inline bool isApprox(const Scalar& x, const Scalar& y,
  typename NumTraits<Scalar>::Real precision =
    NumTraits<Scalar>::dummy_precision())

template< .... >
void evalSolverSugarFunction(....)
{
  ....
  const Scalar psPrec = sqrt(test_precision<Scalar>());
  ....
  if (internal::isApprox(
        calc_realRoots[i], real_roots[j] @)@, psPrec)
  {
    found = true;
  }
  ....
}
',
N'Eigen',
N'
Функция isApprox() имеет два обязательных аргумента
и один необязательный. Это послужило поводом к возникновению
ошибки. Из-за опечатки, закрывающаяся скобка стоит не на
своём месте. Запятая перед третьим аргументом превратилась
в оператор запятая. Переменная psPrec стала правым операндом
этого оператора.
Поясним для тех, кто плохо знаком с оператором запятая.
Операция запятая имеет самый низкий приоритет среди всех
операций языка Си++. У этой операции есть 2 операнда
(левый и правый). Вначале вычисляется левый операнд,
затем правый, а в качестве результата возвращается правый операнд.
Получается, что в начале вызывается функция isApprox()
с двумя аргументами. Но результат её работы никак не
используется. Оператор запятая возвращает значение
переменной psPrec. В результате, код работает так:
<pre>
internal::isApprox(_realRoots[i], real_roots[j]);
if (psPrec)
</pre>
Правильный вариант кода:
<pre>
if (internal::isApprox(
    calc_realRoots[i], real_roots[j], psPrec))
</pre>
',
N'
<a href="http://www.viva64.com/en/d/0257/">V639</a> Consider inspecting the expression for isApprox function call. It is possible that one of the closing ) brackets was positioned incorrectly. polynomialsolver.cpp 123
')
GO
-- 14
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
void LanguageModel::FillConsistencyInfo(....)
{
  ....
  float actual_gap =
    static_cast<float>(word_res->GetBlobsGap(curr_col-1));
  float gap_ratio = expected_gap / actual_gap;
  if (gap_ratio < @1@@/@@2@ || gap_ratio > 2) {
    consistency_info->num_inconsistent_spaces++;
  }
  ....
}
',
N'Tesseract',
N'
Невнимательность.
Неудачно записали значение 0.5.
Происходит целочисленное деление единицы на двойку.
Результат деления равен 0.
Ошибку можно исправить множеством способов.
Некоторые варианты:
<pre>
    - if (gap_ratio < 1.0f/2 || gap_ratio > 2) {
    - if (gap_ratio < 1f/2 || gap_ratio > 2) {
    - if (gap_ratio < float(1)/2 || gap_ratio > 2) {
    - if (gap_ratio < 0.5f || gap_ratio > 2) {
</pre>',
N'
<a href="http://www.viva64.com/en/d/0257/">V636</a> The ''1/2'' expression was implicitly casted from int type to float type. Consider utilizing an explicit type cast to avoid the loss of a fractional part. An example: double A = (double)(X) / Y;. language_model.cpp 1163
'
  )
GO
-- 15
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
__inline__ unsigned long long int rdtsc()
{
#ifdef __x86_64__
  unsigned int a, d;
  __asm__ volatile ("rdtsc" : "=a" (a), "=d" (d));
  return (unsigned long)a | ((@unsigned@ @long@)d @<<@ 32);
#elif defined(__i386__)
  unsigned long long int x;
  __asm__ volatile ("rdtsc" : "=A" (x));
  return x;
#else
#define NO_CYCLE_COUNTER
  return 0;
#endif
}',
N'SMHasher',
N'
Этот код будет неправильно работать, если его скомпилировать
для платформы Win64. Дело в том, что в 64-битной Windows
тип long остался 32-битным. Подробнее про размеры типов
можно почитать здесь.
Сдвиг 32-битной переменной на 32 разряда приводит к
неопределённому поведению. Скорее всего, на практике,
выражение <pre>((unsigned long)d << 32)</pre> окажется равно 0.
Исправленный вариант кода:
<pre>
return (unsigned long long)a |
    ((unsigned long long)d << 32);
</pre>
',
N'<a href="http://www.viva64.com/en/d/0246/">V629</a> Consider inspecting the (unsigned long) d << 32 expression. Bit shifting of the 32-bit value with a subsequent expansion to the 64-bit type. Platform.h 78'
  )
GO
-- 16
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
#define FILE_ATTRIBUTE_READONLY 0x00000001

BOOL DeleteAnyFile( char* cFileName ) {
  ....
  if (nAttr @|@ FILE_ATTRIBUTE_READONLY) {
    nAttr &= ~FILE_ATTRIBUTE_READONLY;
    SetFileAttributes(cFileName, nAttr);
  }
  ....
}
',
N'ABackup',
N'
Опечатка.
Требуется проверить, что в переменной младший бит
равняется единице. Для этого следует использовать
выражение
<pre>(nAttr & 0x00000001).</pre>
Но вместо оператора & написан оператор |. В результате, условие
<pre>(nAttr | 0x00000001)</pre> всегда является истинным.
',
    N'
<a href="http://www.viva64.com/en/d/0234/">V617</a> Consider inspecting the condition. The 0x00000001 argument of the | bitwise operation contains a non-zero value. kitcpp.cpp 304
'
  )
GO
-- 17
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
void ExprParser::replaceBinaryOperands()
{
  char t1 = getOperandType(1);
  char t2 = getOperandType();

  popOperand();
  popOperand();

  if (t1 == t2)
    mOperands.push_back (t1);
  else if (t1 == ''f'' || t2 == ''f'')
    mOperands.push_back (''f'');
  else
    @std@@::@@logic_error@ ("failed to determine result operand type");
}
',
N'OpenMW',
N'
Забыли написать оператор throw.
В результате, создаётся и тут-же уничтожается
временный объект типа <pre>std::logic_error</pre>
Исключение не генерируется. 
Правильный вариант:
<pre>
throw std::logic_error ("failed to determine result operand type");
</pre>
',
N'
<a href="http://www.viva64.com/en/d/0206/">V596</a> The object was created but it is not being used. The throw keyword could be missing: throw logic_error(FOO); exprparser.cpp 101
')
GO
-- 18
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
char* crypt_md5(const char* pw, const char* salt)
{
  MD5_CTX ctx,ctx1;
  unsigned long l;
  int sl, pl;
  u_int i;
  u_char final[MD5_SIZE];
  static const char *sp, *ep;

  ....

  /* Dont leave anything around in vm they could use. */
  @memset@(final, 0, sizeof final);
  return passwd;
}
',
N'CamStudio',
N'
Перед выходом из функции требуется очистить буфер,
содержащий приватные данные. Для этого используется
функция memset(). Это неправильно. Компилятор знает,
что после функции memset() буфер final больше не
используется. Он имеет полное право удалить вызов
функции memset(). Более того, он не только имеет право,
но так и сделает при сборке release-версии. Подробнее
про это можно почитать в статье "Перезаписывать
память - зачем?" и в описании диагностики V597.
Чтобы этого не произошло, следует использовать
специальные функции, которые компилятор не имеет право удалить.
',
N'
<a href="http://www.viva64.com/en/d/0208/">V597</a> The compiler could delete the memset function call, which is used to flush final buffer. The RtlSecureZeroMemory() function should be used to erase the private data. md5.c 342
'
  )
GO
-- 19
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
void TSAPI LoadFavoritesAndRecent()
{
  RCENTRY *recentEntries, rceTemp;
  DWORD   dwRecent;
  int     iIndex = 0, i, j;
  HANDLE hContact = (HANDLE)CallService(
                      MS_DB_CONTACT_FINDFIRST, 0, 0);
  recentEntries = new RCENTRY[nen_options.wMaxRecent + 1];
  ....
  if (iIndex == 0) {
    @free@(@recentEntries@);
    return;
  }

  for (i = 0; i < iIndex - 1; i++) {
  ....
  }
}
',
N'Miranda IM',
N'
Память выделяется с помощью оператора new,
а освобождается с помощью функции free().
Исправленный код:
<pre>
if (iIndex == 0) {
  delete[] recentEntries;
  return;
}
</pre>',
N'
<a href="http://www.viva64.com/en/d/0226/">V611</a> The memory was allocated using new operator but was released using the free function. Consider inspecting operation logics behind the recentEntries variable. trayicon.cpp 355
')
GO
-- 20
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
int SSL_shutdown(SSL *s)
{
  if (@s@@->@@handshake_func@ == 0)
  {
    SSLerr(SSL_F_SSL_SHUTDOWN, SSL_R_UNINITIALIZED);
    return -1;
  }

  if ((s != NULL) && !SSL_in_init(s))
    return(s->method->ssl_shutdown(s));
  else
    return(1);
  }
  ....
}
',
N'OpenSSL',
N'
В начале указатель s разыменовывается внутри
выражения <pre>(s->handshake_func == 0)</pre>
И только затем, этот указатель проверяется на равенство нулю.
',
N'<a href="http://www.viva64.com/en/d/0205/">V595</a> The s pointer was utilized before it was verified against nullptr. Check lines: 1013, 1019. ssl_lib.c 1013'
  )
GO
-- 21
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
bool QConfFileSettingsPrivate::readIniLine(....)
{
  int dataLen = data.length();
  ....
  if (i == lineStart + 1) {
    char ch;
    while (i < dataLen &&
           ((ch = data.at(i) != ''\n''@)@ && ch != ''\r''))
      ++i;
    lineStart = i;
  } else if (!inQuotes) {
  ....
}',
N'Qt',
N'
Одна из закрывающихся скобок '')'' находится не на своём месте.
Разберём, как работает подвыражение <pre>((ch = data.at(i) != ''\n'')</pre>
- Вызывается функция data.at(i);<br/>
- Символ, который вернула функция сравнивается с ''\n'' (См. приоритеты операций);<br/>
- В переменную ''ch'' помещается не символ, а значение 0 или 1;<br/>
Получается, что сравнение ch != ''\r'' не имеет смысла.
0 или 1 всегда не равны ''\r''. Правильный вариант кода:
<pre>
while (i < dataLen &&
       ((ch = data.at(i)) != ''\n'' && ch != ''\r''))
</pre>',
N'<a href="http://www.viva64.com/en/d/0197/">V593</a> Consider reviewing the expression of the A = B != C kind. The expression is calculated as following: A = (B != C). qsettings.cpp 1702'
)
GO
-- 22
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
void Assign(const AVSValue* src, bool init) {
  if (src->IsClip() && src->clip)
    src->clip->AddRef();
  if (!init && IsClip() && clip)
    clip->Release();
  memcpy(this,src,@sizeof@(@this@));
}
',
N'Ffdshow',
N'
Копируется только часть объекта.
Выражение sizeof(this) вычисляет размер указателя,
а вовсе не размер объекта. В результате будет
скопировано только 4 или 8 байт, в зависимости
от разрядности платформы.
Кстати, автор знает, что с этим кодом что-то не так.
Но не может понять, что. О неладном свидетельствуют
комментарии в коде, которые я удалил.
На самом деле полный текст функции выглядит так:
<pre>
void Assign(const AVSValue* src, bool init) {
  if (src->IsClip() && src->clip)
    src->clip->AddRef();
  if (!init && IsClip() && clip)
    clip->Release();
  // make sure this copies the whole struct!
  //((__int32*)this)[0] = ((__int32*)src)[0];
  //((__int32*)this)[1] = ((__int32*)src)[1];
  memcpy(this,src,sizeof(this));
}
</pre>
В общем, автор так и не смог понять, что не так с кодом.
Правильный код:
<pre>
memcpy(this,src,sizeof(*this));
</pre>
',
N'<a href="http://www.viva64.com/en/d/0181/">V579</a> The memcpy function receives the pointer and its size as arguments. It is possibly a mistake. Inspect the third argument. avisynth.h 695'
  )
GO
-- 23
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
PassRefPtr<Structure> Structure::
  getterSetterTransition(Structure* structure)
{
  ...
  RefPtr<Structure> transition = 
    create(structure->storedPrototype(), structure->typeInfo());
  transition->m_propertyStorageCapacity = 
    structure->m_propertyStorageCapacity;
  transition->m_hasGetterSetterProperties =
    @transition@->m_hasGetterSetterProperties;
  transition->m_hasNonEnumerableProperties = 
    structure->m_hasNonEnumerableProperties;
  transition->m_specificFunctionThrashCount = 
    structure->m_specificFunctionThrashCount;
  ...
}
',
N'Qt',
N'
Опечатка.
Переменная <pre>transition->m_hasGetterSetterProperties</pre>
присваивается сама себе.
Правильный вариант:
<pre>
transition->m_hasGetterSetterProperties
    = structure->m_hasGetterSetterProperties;
</pre>',
N'<a href="http://www.viva64.com/en/d/0168/">V570</a> The transition->m_hasGetterSetterProperties variable is assigned to itself. structure.cpp 512'
  )
GO
-- 24
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
bool Matrix4::operator==(const Matrix4& other) const {
    // If the bit patterns are identical, they must be
    // the same matrix. If not, they *might* still have
    // equal elements due to floating point weirdness.
    if (memcmp(this, &other, sizeof(Matrix4) == 0@)@) {
        return true;
    }

    for (int r = 0; r < 4; ++r) {
        for (int c = 0; c < 4; ++c) {
            if (elt[r][c] != other.elt[r][c]) {
                return false;
            }
        }
    }

    return true;
}
',
N'G3D Content Pak',
N'
Опечатка.
Не там поставлена закрывающаяся скобка ")".
Количество сравниваемых элементов определяется
выражением "sizeof(Matrix4) == 0". Результат
этого выражения 0. В результате, функция memcmp()
ничего не сравнивает.
Правильный код:
<pre>
if (memcmp(this, &other, sizeof(Matrix4)) == 0) {
</pre>',
N'<a href="http://www.viva64.com/en/d/0175/">V575</a> The ''memcmp'' function processes "0" elements. Inspect the "third" argument. matrix4.cpp 269
#info
A parenthesis put in a wrong place. This is how it should be: if (memcmp(this, &other, sizeof(Matrix4)) == 0) {
'
  )
GO
-- 25
INSERT INTO Question
  (
    CodeFragment,
    ProjectName,
    ExplanatoryText,
    PvsStudioWarning
  )
VALUES
  (
N'
void tcd_malloc_decode(....) {
  ...
  x0 = j == 0 ? tilec->x0 :
                int_min(x0, (unsigned int) tilec->x0);
  y0 = j == 0 ? tilec->y0 :
                int_min(y0, (unsigned int) tilec->@x0@);
  x1 = j == 0 ? tilec->x1 :
                int_max(x1, (unsigned int) tilec->x1);      
  y1 = j == 0 ? tilec->y1 :
                int_max(y1, (unsigned int) tilec->y1);
  ...
}
',
N'Blender',
N'Простая опечатка.
В выделенном месте должен использоваться член ''y0''.',
N'<a href="http://www.viva64.com/en/d/0126/">V537</a> Consider reviewing the correctness of ''x0'' item''s usage. tcd.c 650'
)
GO
-- 26
INSERT INTO Question (CodeFragment, ProjectName, ExplanatoryText, PvsStudioWarning)
VALUES (
N'
BOOL AddTail(LPVOID p)
{ 
    ...
    if (queue.GetSize() >= this -> _limit)@;@
    {
        while (queue.GetSize() > this - > _limit -1)
        {
            ::WaitForSingleObject(handles[SemaphoreIndex], 1);
            queue.Delete(0);
        }
    }
    ...
}
',
N'Xpdfwin',
N'
Опечатка. Лишняя точка с запятой '';'' после условия <pre>if(queue.GetSize() >= this->_limit)</pre>
',
N'<a href="http://www.viva64.com/en/d/0118/">V529</a> Odd semicolon '';'' AFTER ''if'' operator. QUEUE.h 66'
)
GO
-- 27
INSERT INTO Question (CodeFragment, ProjectName, ExplanatoryText, PvsStudioWarning)
VALUES (
N'
int set_view_property(....)
{
    BOOL status = FALSE;
    ....
    status =
        setGraphicObjectProperty(pobjUID, __GO_VIEW__,
                                &viewType, jni_int, 1);
    if (status @=@ TRUE)
    {
        return SET_PROPERTY_SUCCEED;
    }
    else
    {
        Scierror(999, _("''%s'' property does not exist ")
                _("for this handle.\n"), "view");
        return SET_PROPERTY_ERROR;
    }
    ....
}
',
N'Scilab',
N'
Опечатка.
Вместо проверки результата, переменной ''status''
присваивается значение TRUE.
Кстати, сравнивать с TRUE это плохой стиль,
который может привести к неприятным 
ошибкам. Истина, это не обязательно 1.
Правильная проверка: <pre>if (status != false)</pre>
',
N'<a href="http://www.viva64.com/en/d/0152/">V559</a> Suspicious assignment inside the condition expression OF ''if'' operator
: status = 1. set_view_property.c 61
')
GO
-- 28
INSERT INTO Question (CodeFragment, ProjectName, ExplanatoryText, PvsStudioWarning)
VALUES (
N'
   CV_IMPL IplImage * icvCreateIsometricImage(....)
   {
   ....
    if (!dst || dst->depth != desired_depth ||
        dst->nChannels != desired_num_channels ||
        dst_size.width != src_size.width ||
        dst_size.height != @dst_size@.height)
       ....
    }
       ',
N'OpenCV',
N'Переменная <pre>dst_size.height</pre> сравнивается сама с собой',
N'<a href="http://www.viva64.com/en/d/0090/">V501</a> There are identical sub -expressions TO the LEFT AND TO the RIGHT OF the ''!='' operator : dst_size.height != dst_size.height  epilines.cpp 2118
')
GO
-- 29
INSERT INTO Question (CodeFragment, ProjectName, ExplanatoryText, PvsStudioWarning)
VALUES (
N'
#define MAXCOORDS 5
          
void Mapdesc::identify(REAL dest[MAXCOORDS][MAXCOORDS])
{
    memset(dest, 0, @sizeof@(@dest@));
    for (int i = 0; i != hcoords; i++)
        dest[i][i] = 1.0;
}
',
N'ReactOS',
N'
The ''dest'' OBJECT IS simply a pointer.
VALUE [MAXCOORDS][MAXCOORDS] IN the 
SQUARE brackets indicates TO the programmer that he IS working 
WITH an array OF MAXCOORDS x MAXCOORDS items.
     But it IS NOT an array OF items which IS passed
     INTO the FUNCTION - it IS 
     ONLY the pointer.
     So, the sizeof(dest) expression will

 RETURN VALUE 4 OR 8(the SIZE OF the pointer
 IN a 32 -BIT / 64 -BIT SYSTEM).
This IS what should have been written here :
<pre>Mapdesc::identify(REAL(&dest)[MAXCOORDS][MAXCOORDS])</pre>
',
N'<a href="http://www.viva64.com/en/d/0100/">V511</a> The sizeof() operator RETURNS SIZE OF the pointer, AND NOT OF the array, IN ''sizeof (dest)'' 
expression. glu32 mapdesc.cc 95'
)
GO
-- 30
INSERT INTO Question (CodeFragment, ProjectName, ExplanatoryText, PvsStudioWarning)
VALUES (
N'
void ClientSession :: findIpAddress(CSCPMessage * request)
{
  ....
    if (subnet != NULL)
    {
        debugPrintf(5, _T("findIpAddress(%s): found subnet %s"),
                    ipAddrText,
                    subnet->Name());

        found = subnet->findMacAddress(ipAddr, macAddr);
    }
    else
    {
        debugPrintf(5, _T("findIpAddress(%s): subnet not found"),
                    ipAddrText,
                    @subnet@@->@@Name@());
    }
  ....
}
',
N'NetXMS',
N'
Здесь сразу две ошибки. Обе они связаны с выражением
"subnet->Name()" в ''else'' -ветке.
Самое главное, если указатель subnet равен NULL,
то произойдёт разыменование  нулевого указателя
при получении имени. Все дело в том, что имя и не
должно было печататься. Об этом свидетельствует
только один спецификатор формата "%s".
Это вторая ошибка.
Правильный код:
<pre>
debugPrintf(5,
            _T("findIpAddress(%s): subnet not found"),
            ipAddrText);
</pre>',
N'<a href="http://www.viva64.com/en/d/0111/">V522</a> Dereferencing OF the NULL pointer ''subnet'' might take place. session.cpp 10823'
)
GO
-- 31
INSERT INTO Question (CodeFragment, ProjectName, ExplanatoryText, PvsStudioWarning)
VALUES (
N'
vdlist_iterator@&@ operator--(int) {
  vdlist_iterator tmp(*this);
  mp = mp->mListNodePrev;
  @return@ @tmp@;
}
',
N'VirtualDub',
N'
Возвращается ссылка на локальный объект ''tmp'',
созданный на стеке. В момент 
выхода из функции объект будет разрушен.
В результате operator-- возвращает ссылку на
несуществующий объект.
Чтобы исправить ошибку, нужно возвращать объект
по значению.
Правильный код:
<pre>
vdlist_iterator operator--(int) { ..... }
</pre>
',
N'<a href="http://www.viva64.com/en/d/0149/">V558</a> FUNCTION RETURNS the reference TO temporary LOCAL OBJECT : tmp. VirtualDub 
vdstl.h 460'
)
GO
-- 32
INSERT INTO Question (CodeFragment, ProjectName, ExplanatoryText, PvsStudioWarning)
VALUES (
N'
// Coefficients USED TO CONVERT FROM RGB TO monochrome.
const uint32 kRedCoefficient = 2125;
const uint32 kGreenCoefficient = 7154;
const uint32 kBlueCoefficient = @0721@;
const uint32 kColorCoefficientDenominator = 10000;
',
N'Chromium',
N'
Иногда удобно работать с целочисленными коэффициентами.
Тогда, за единицу берётся, например, число 10000.
Тогда, значение 0.5 будет закодировано как 5000. 
Именно так и поступил программист в приведённом коде.
Числа 2125, 7154 и 721 в сумме дают как раз 10000.
Для красоты, перед 721 он написал ноль. Это ошибка.
Он забыл, что если число начинается с 0, то это
восьмеричное число. Восьмеричное число 0721 это
на самом деле 465 в десятичной системе.
',
N'<a href="http://www.viva64.com/en/d/0125/">V536</a> Be advised that the utilized constant VALUE IS represented BY an octal 
form. Oct : 0721, Dec : 465. pwg_encoder.cc 24')
GO

COMMIT;