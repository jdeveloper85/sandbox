﻿using System.Web.Routing;

namespace WatchfulEye.Web
{
    /// <summary>
    ///    Класс конфигурации маршрутизации URL
    /// </summary>
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapPageRoute(null, "", "~/Pages/Start.aspx");
            routes.MapPageRoute("RootRoute", "", "~/");
            routes.MapPageRoute("StartRoute", "start", "~/Pages/Start.aspx");
            routes.MapPageRoute("FinishRoute", "finish", "~/Pages/Finish.aspx");
            routes.MapPageRoute("QuestionRoute", "question/{number}", "~/Pages/ProcessTest.aspx");
            routes.MapPageRoute("CrudRoute", "crud/questions", "~/Pages/Crud/Questions.aspx");
        }
    }
}