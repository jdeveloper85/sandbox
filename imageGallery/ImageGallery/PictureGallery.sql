﻿CREATE TABLE PictureGallery
(
	PictureId              INT IDENTITY(1, 1) NOT NULL,
	AccountId              INT NOT NULL,
	PictureDescription     VARCHAR(1024) NOT NULL DEFAULT '',
	Width                  INT NOT NULL,
	Height                 INT NOT NULL,
	PictureFileName        VARCHAR(256) NOT NULL,
	PictureData            VARBINARY(MAX) NOT NULL,
	PictureMimeType        VARCHAR(256) NOT NULL,
	CONSTRAINT PictuteGallery_PictureId_PK PRIMARY KEY(PictureId),
	CONSTRAINT PictureGalary_AccountId_Account_FK FOREIGN KEY(AccountId)
	REFERENCES Account(AccountId) ON UPDATE CASCADE ON DELETE CASCADE
)