﻿using System.Web.Optimization;

namespace WatchfulEye.Web
{
    /// <summary>
    /// Конфигуратор подключения скриптов и статического содержимого
    /// </summary>
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
               "~/Scripts/jquery-{version}.js",
               "~/Scripts/processAnswer.js"));
        }
    }
}