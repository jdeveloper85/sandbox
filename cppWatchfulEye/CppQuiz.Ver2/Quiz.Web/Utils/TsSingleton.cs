﻿using System.Threading;

namespace Quiz.Web.Utils
{
   /// <summary>
   ///    "Одиночка" обобщенного типа
   /// </summary>
   /// <remarks>Создание происходит в потокобезопасном режиме</remarks>
   /// <typeparam name="T">Параметр типа
   ///    <remarks>Должен иметь конструктор по-умолчанию</remarks>
   /// </typeparam>
   public static class TsSingleton<T>
      where T : class, new()
   {
      private static T _instance;

      public static T Instance
      {
         get
         {
            if (_instance != null)
            {
               return _instance;
            }

            var tempInstance = new T();
            Interlocked.CompareExchange(ref _instance, tempInstance, null);

            return _instance;
         }
      }
   }
}