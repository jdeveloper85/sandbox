namespace Quiz.Web.Model
{
   /// <summary>
   ///    ����� ��� ���������� �������� ������������
   /// </summary>
   public class AnswerResponse
   {
      /// <summary>
      ///    ��������� ������
      /// </summary>
      public string AnswerState { get; set; }

      /// <summary>
      ///    ������ ������
      /// </summary>
      public int DataIndex { get; set; }

      /// <summary>
      ///    ������ ���������� �������� ������
      /// </summary>
      public int[] CorrectIndices { get; set; }

      /// <summary>
      /// ������� ���-�� ���������� �������
      /// </summary>
      public int CurrentCorrectAnswersNumber { get; set; }
   }
}