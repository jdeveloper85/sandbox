﻿using System;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using WatchfulEye.Web.Pages.Helpers;

namespace WatchfulEye.Web
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var currentHttpContext = HttpContext.Current;
            var lastError = currentHttpContext.Server.GetLastError();
            CacheHelper.AddLastServerError(currentHttpContext.Cache, lastError);
        }
    }
}