using System.Web.Configuration;

namespace Pvs.Quiz.Common
{
   /// <summary>
   ///    ��������������� ����� ��� ������ ���������� ������������
   /// </summary>
   public static class WebConfigReader
   {
      private const int DefaultTestQuestionsNumber = 15;
      private const int DefaultQuestionTime = 60;

      /// <summary>
      ///    ���-�� �������� � �����
      /// </summary>
      public static int TestQuestionsNumber
      {
         get
         {
            var testQuestionsNumber = WebConfigurationManager.AppSettings.Get("TestQuestionsNumber");
            int testQnum;
            return int.TryParse(testQuestionsNumber, out testQnum) ? testQnum : DefaultTestQuestionsNumber;
         }
      }

      /// <summary>
      ///    ���-�� ������, ���������� �� ������
      /// </summary>
      public static int QuestionTime
      {
         get
         {
            var questionTimeStr = WebConfigurationManager.AppSettings.Get("QuestionTime");
            int questionTime;
            return int.TryParse(questionTimeStr, out questionTime) ? questionTime : DefaultQuestionTime;
         }
      }
   }
}