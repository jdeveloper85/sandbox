﻿namespace Pvs.Autopost.Config
{
   /// <summary>
   /// Конфигурация отправки почтовых сообщений
   /// </summary>
   public struct EmailConfiguration
   {
      public string FromAddress { get; set; }

      public string FromDisplayName { get; set; }

      public string MailLogin { get; set; }

      public string MailPassword { get; set; }

      public string SmtpServerName { get; set; }

      public int SmtpPort { get; set; }
   }
}