﻿/**
 * Размещение постов на Pastebin
 * Usage: PastebinPosting.exe TopLimitPerRun MinTimeslotSec MaxTimeslotSec
 * 
 * - TopLimitPerRun: Предельное кол-во постов за один запуск программы
 * - MinTimeslotSec: Минимальное значение времени между размещениями в секундах
 * - MaxTimeslotSec: Максимальное значение времени между размещениями в секундах
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PastebinPosting
{
   internal static class PastebinPosting
   {
      private const string Format = "cpp";
      private static readonly string Username;
      private static readonly string Password;
      private static readonly string DeveloperApiKey;
      private static int _topLimitPerRun;
      private static int _minTimeslotSec;
      private static int _maxTimeslotSec;
      private static readonly AppSettingsReader AppSettingsReader = new AppSettingsReader();
      private static readonly Random Random = new Random();
      private static readonly CancellationTokenSource TokenSource = new CancellationTokenSource();

      static PastebinPosting()
      {
         PastebinPost.PastebinLoginUrl = GetByKey<string>("LoginUrl");
         PastebinPost.PastebinPostUrl = GetByKey<string>("PostUrl");
         Username = GetByKey<string>("Username");
         Password = GetByKey<string>("Password");
         DeveloperApiKey = GetByKey<string>("DeveloperApiKey");
      }

      private static void Main(string[] args)
      {
         if (args == null || args.Length != 3)
         {
            PrintUsage();
            return;
         }

         string errorMessage;
         if (ParseArguments(args, out errorMessage))
         {
            var timeSlots = new Queue<int>(_topLimitPerRun);
            for (int i = 0; i < _topLimitPerRun; i++)
            {
               int nextValue = Random.Next(_minTimeslotSec, _maxTimeslotSec);
               timeSlots.Enqueue(nextValue);
            }

            Task.Factory.StartNew(PerRunWorkItem, timeSlots, TokenSource.Token);
            Console.WriteLine(">>>>> Press any key to exit the program");
            Console.ReadKey();
            TokenSource.Cancel();
         }
         else
         {
            Console.WriteLine(errorMessage);
         }
      }

      private static void PerRunWorkItem(object state)
      {
         var timeSlots = state as Queue<int>;
         if (timeSlots != null)
         {
            while (!TokenSource.IsCancellationRequested && timeSlots.Count > 0)
            {
               try
               {
                  Pastebin pastebin = PastebinPostWorkItem();
                  int sleepValue = timeSlots.Dequeue();
                  NotifyConsole(pastebin, timeSlots);
                  Thread.Sleep(sleepValue * 1000);
               }
               catch (Exception ex)
               {
                  Console.WriteLine(ex.Message);
               }
            }

            Environment.Exit(0);
         }
      }

      private static void NotifyConsole(Pastebin pastebin, ICollection timeSlots)
      {
         if (pastebin == null) return;
         Console.WriteLine("Pastebin with title {0} published successfully", pastebin.Subject);
         Console.WriteLine("{0} remains in the current startup", timeSlots.Count);
         Console.WriteLine();
      }

      private static Pastebin PastebinPostWorkItem()
      {
         Pastebin paste;

         using (var entities = new SocialAutopostEntities())
         {
            paste = (from pastebin in entities.Pastebin
                     where !pastebin.IsPosted
                     select pastebin).FirstOrDefault();
            if (paste != null)
            {
               var pastebinPost = new PastebinPost(Username, Password, DeveloperApiKey, PastebinPost.PastebinLoginUrl);
               pastebinPost.Send(paste.Body, paste.Subject, Format, PastebinPost.PastebinPostUrl);
               paste.IsPosted = true;
               paste.PostedDate = DateTime.Now;
               entities.SaveChanges();
            }
         }

         return paste;
      }

      private static bool ParseArguments(IList<string> args, out string errorMessage)
      {
         if (!int.TryParse(args[0], out _topLimitPerRun))
         {
            errorMessage = "TopLimitPerRun must be int";
            return false;
         }

         if (!int.TryParse(args[1], out _minTimeslotSec))
         {
            errorMessage = "MinTimeslotSec must be int";
            return false;
         }

         if (!int.TryParse(args[2], out _maxTimeslotSec))
         {
            errorMessage = "MaxTimeslotSec must be int";
            return false;
         }

         errorMessage = string.Empty;
         return true;
      }

      private static void PrintUsage()
      {
         Console.WriteLine("Usage: PastebinPosting.exe TopLimitPerRun MinTimeslotSec MaxTimeslotSec");
         Console.WriteLine("\tTopLimitPerRun: Предельное кол-во постов за один запуск программы");
         Console.WriteLine("\tMinTimeslotSec: Минимальное значение времени между размещениями в секундах");
         Console.WriteLine("\tMaxTimeslotSec: Максимальное значение времени между размещениями в секундах");
      }

      private static T GetByKey<T>(string key)
      {
         try
         {
            return (T)AppSettingsReader.GetValue(key, typeof(T));
         }
         catch (InvalidOperationException invalidOperationEx)
         {
            throw new Exception(invalidOperationEx.Message, invalidOperationEx);
         }
         catch (ArgumentNullException argNullEx)
         {
            throw new Exception(argNullEx.Message, argNullEx);
         }
      }
   }
}