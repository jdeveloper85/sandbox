﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnswerStatus.ascx.cs"
    Inherits="WatchfulEye.Web.Controls.AnswerStatus"
    ClientIDMode="Static" %>
<table class="answer-status">
    <tr>
        <td colspan="3" style="text-align: center;">
            <div id="dontKnow">
                <input class="actionButtons" id="dontknowAction" type="button" value="Don't know" />
            </div>
            <div id="rightAnswer">
            </div>
            <div id="nextQuestion">
                <a href="<%= GetNextQuestionUrl() %>" class="actionButtons"><%= GetQuestionText() %></a>
            </div>
        </td>
    </tr>
</table>
