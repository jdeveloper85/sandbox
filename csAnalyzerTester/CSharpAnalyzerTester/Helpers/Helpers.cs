﻿//  2006-2008 (c) Viva64.com Team
//  2008-2016 (c) OOO "Program Verification Systems"
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Xml.Serialization;
using ProgramVerificationSystems.PVSStudio;
using ProgramVerificationSystems.PVSStudio.CommonTypes;
using Res = CSharpAnalyzerTester.Properties.Resources;

namespace CSharpAnalyzerTester.Helpers
{
    public static class BatchHelper
    {
        public static void WriteBatForApproveEtalon(string cmdPath, string logFileName)
        {
            logFileName = Path.GetFileName(logFileName);
            logFileName = logFileName != null && logFileName.Contains(' ')
                ? string.Format("\"{0}\"", logFileName)
                : logFileName;
            const string relativeToLog = @"..\..\";
            const string relativeToEtalons = @" ..\..\..\..\EtalonLogs\";
            string[] batCommands =
            {
                string.Format("copy {0}{1} \\\\viva64-build\\SelfTester_setup\\EtalonLogsCSharp\\{1} /V /Y /Z",
                    relativeToLog, logFileName),
                string.Format("copy {0}{1}{2}{1} /V /Y /Z",
                    relativeToLog, logFileName, relativeToEtalons),
                "pause"
            };
            File.WriteAllLines(cmdPath, batCommands);
        }

        public static void WriteBatForRunDevenv(string cmdPath, string logFileNameDiffs)
        {
            var batCommand = string.Format(@"..\..\..\..\{0} --diff {1}",
                Path.GetFileName(Assembly.GetEntryAssembly().Location),
                logFileNameDiffs.Contains(' ') ? string.Format("\"{0}\"", logFileNameDiffs) : logFileNameDiffs);
            File.WriteAllText(cmdPath, batCommand);
        }

        public static void CreateRestartBatScript(TestProject project, string solutionName, string logsDirectory)
        {
            var restartProjectCommand = string.Format("CD ..\\..\\..\\{0}{3} --runone \"{1}\" \"{2}\"{0}pause",
                Environment.NewLine,
                project.SolutionPath,
                logsDirectory.TrimEnd(Path.DirectorySeparatorChar),
                Path.GetFileName(Assembly.GetEntryAssembly().Location));
            var restartBatName = Path.Combine(logsDirectory, string.Format("{0}_Restart.bat", solutionName));
            File.WriteAllText(restartBatName, restartProjectCommand);
        }
    }

    public static class OutputHelper
    {
        public static void WriteLogNoThrow(string logName, StringBuilder stdOut, StringBuilder stdErr, TextWriter writer)
        {
            try
            {
                stdOut.AppendLine(stdErr.ToString());
                var buffer = stdOut.ToString().Trim('\r', '\n', ' ');
                if (!string.IsNullOrEmpty(buffer))
                    File.WriteAllText(logName, buffer);
            }
            catch (Exception ex)
            {
                writer.WriteLine(ex.ToString());
                writer.WriteLine(stdOut.ToString());
            }
        }

        public static void SaveAnalyzerSettings(string analyzerSettingsPath, ApplicationSettings applicationSettings, TextWriter textWriter)
        {
            try
            {
                var serializer = new XmlSerializer(typeof(ApplicationSettings));

                using (TextWriter writer =
                    new StreamWriter(
                        new FileStream(analyzerSettingsPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite),
                        Encoding.Unicode))
                {
                    serializer.Serialize(writer, applicationSettings);
                }
            }
            catch (Exception ex)
            {
                textWriter.WriteLine(ex.ToString());
            }
        }

        public static ApplicationSettings LoadAnalyzerSettings(string analyzerSettingsPath, TextWriter @out)
        {
            try
            {
                if (!File.Exists(analyzerSettingsPath))
                    return null;

                var serializer = new XmlSerializer(typeof(ApplicationSettings));
                ApplicationSettings existApplicationSettings;
                using (var fs = new FileStream(analyzerSettingsPath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    existApplicationSettings = (ApplicationSettings)serializer.Deserialize(fs);
                }

                if (existApplicationSettings == null)
                    return null;

                existApplicationSettings.SourceTreeRoot = Environment.CurrentDirectory;
                return existApplicationSettings;
            }
            catch (Exception ex)
            {
                @out.WriteLine(ex.ToString());
                return null;
            }
        }
    }

    public static class LoadSettingsHelper
    {
        public static IEnumerable<TestProject> LoadSettings(string settingsPath,
            out bool success,
            out string errorMessage)
        {
            if (!File.Exists(settingsPath))
            {
                success = false;
                errorMessage = Res.FileSettingsNotFound;
                return Enumerable.Empty<TestProject>();
            }

            var projects = TestProject.LoadFromFile(settingsPath);
            if (projects == null)
            {
                errorMessage = Res.FileSettingsIncorrect;
                success = false;
                return Enumerable.Empty<TestProject>();
            }

            if (projects.Length == 0)
            {
                errorMessage = Res.FileSettingsNoProjects;
                success = false;
                return Enumerable.Empty<TestProject>();
            }

            var invalidSettingsFragment =
                (from project in projects
                 where string.IsNullOrWhiteSpace(project.SolutionPath)
                 select string.Format("{0}{1}{2}", Res.FileSettingsBadProject, Environment.NewLine, project))
                    .FirstOrDefault();
            if (!string.IsNullOrEmpty(invalidSettingsFragment))
            {
                success = false;
                errorMessage = invalidSettingsFragment;
                return Enumerable.Empty<TestProject>();
            }

           Array.ForEach(projects,
              project =>
                 project.SolutionPath = File.Exists(project.SolutionPath) ? project.SolutionPath : string.Format("{0}{1}", Environment.CurrentDirectory, project.SolutionPath));
            var notExistsSolution =
                (from project in projects
                 where !File.Exists(project.SolutionPath)
                 select string.Format("{0}{1}{2}", Res.FileSettingsNoProject, Environment.NewLine, project))
                    .FirstOrDefault();
            if (!string.IsNullOrEmpty(notExistsSolution))
            {
                success = false;
                errorMessage = notExistsSolution;
                return Enumerable.Empty<TestProject>();
            }

            success = true;
            errorMessage = string.Empty;
            return projects;
        }
    }

    public static class SingletonInjector<T>
        where T : class, new()
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance != null) return _instance;
                var tempInstance = new T();
                Interlocked.CompareExchange(ref _instance, tempInstance, null);
                return _instance;
            }
        }
    }

    public class AutoStopwatch : Stopwatch, IDisposable
    {
        public AutoStopwatch()
        {
            Start();
            StartTime = DateTime.Now;
        }

        public DateTime StartTime { get; private set; }
        public DateTime EndTime { get; private set; }

        public void Dispose()
        {
            Stop();
            EndTime = DateTime.Now;
        }
    }

    public static class DataRowExtensions
    {
        public static string ToMessage(this DataRow row)
        {
            if ((uint)row[TableIndexes.Level] >= 1 && (uint)row[TableIndexes.Level] <= 3)
            {
                var msgFormat = string.Format("{{0}}({{1}}): error {{2}}: {{3}}{0}", Environment.NewLine);
                var fileNameFull = row[TableIndexes.File] as string;
                if (fileNameFull != null && fileNameFull.StartsWith(ApplicationSettings.SourceTreeRootMarker))
                {
                    fileNameFull = fileNameFull.Replace(ApplicationSettings.SourceTreeRootMarker,
                        Environment.CurrentDirectory);
                }

                var msg = string.Format(msgFormat, fileNameFull, row[TableIndexes.Line], row[TableIndexes.ErrorCode],
                    row[TableIndexes.Message]);

                return msg;
            }

            return (string)row[TableIndexes.Message];
        }
    }

    internal static class Unmanaged
    {
        [DllImport("kernel32.dll")]
        internal static extern ErrorModes SetErrorMode(ErrorModes uMode);
    }

    public sealed class ErrorModeManager : IDisposable
    {
        private const ErrorModes DefaultErrorMode = ErrorModes.SystemDefault;
        private readonly ErrorModes _oldErrorMode;
        private bool _isDisposed;

        public ErrorModeManager()
            : this(DefaultErrorMode)
        {
        }

        public ErrorModeManager(ErrorModes errorMode)
        {
            _oldErrorMode = Unmanaged.SetErrorMode(errorMode);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ErrorModeManager()
        {
            Dispose(false);
        }

        private void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    /* NOTE: Если нужно что-то еще в случае явного освобождения ресурсов */
                }

                Unmanaged.SetErrorMode(_oldErrorMode);
            }

            _isDisposed = true;
        }
    }    

/*
    public struct LightweightLock
    {
        private int _resourceInUse;

        public void Enter()
        {
            while (Interlocked.Exchange(ref _resourceInUse, 1) != 0)
            {                
            }
        }

        public void Leave()
        {
            Thread.VolatileWrite(ref _resourceInUse, 0);
        }
    }
*/

    public static class TimeSpanExt
    {        
        public static string ToAbsoluteFormat(this TimeSpan aTimeSpan)
        {
            var totalHours = (int) Math.Floor(aTimeSpan.TotalHours);
            return string.Format("{0,2:D2}:{1,2:D2}:{2,2:D2}", totalHours, aTimeSpan.Minutes, aTimeSpan.Seconds);
        }
    }
}