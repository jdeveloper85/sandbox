﻿namespace Quiz.Web.Model
{
   /// <summary>
   /// Класс для передачи POST-запроса от пользователя
   /// </summary>
   public class AnswerRequest
   {
      public AnswerRequestKind AnswerRequestKind { get; set; }

      public int DataIndex { get; set; }

      public int CurrentQuestionNumber { get; set; }
   }
}