﻿/**
 * Простой тест сервера, производящего сообщения в msmq
 */

using System;
using Pvs.Autopost.Config;
using Pvs.Autopost.Core;
using Pvs.Autoposting.ProducerServer;

namespace AutopostProducerSample
{
   internal static class ProducerEntryPoint
   {
      private static void Main()
      {
         IPostService postProducer =
            new AutopostProducer(SocialKeysReader.PostLimit, SocialKeysReader.PollingInterval,
               AutopostLoggerWrapper.GetInstance(Loggers.Autoposter));
         postProducer.Start();

         Console.ReadLine();
      }
   }
}