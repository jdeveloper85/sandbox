﻿using System;
using log4net;
using log4net.Config;

namespace Pvs.Autopost.Core
{
   /// <summary>
   /// Wrapper для логгера Autoposter
   /// </summary>
   public static class AutopostLoggerWrapper
   {
      static AutopostLoggerWrapper()
      {
         XmlConfigurator.Configure();
      }

      public static ILog GetInstance(Loggers loggerType)
      {
         string loggerName = Enum.GetName(typeof(Loggers), loggerType);
         return LogManager.GetLogger(loggerName);
      }
   }
}