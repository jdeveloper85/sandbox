﻿using System;
using System.Collections.Generic;

namespace Pvs.Autopost.Core
{
   /// <summary>
   /// Структура для статистики по ссылкам
   /// </summary>
   public struct LinkStatistics
   {
      /// <summary>
      /// Общий процент ссылок для хоста
      /// </summary>
      public double Percent { get; set; }

      /// <summary>
      /// Хост
      /// </summary>
      public string Host { get; set; }

      /// <summary>
      /// Набор ссылок, размещенных на хосте
      /// </summary>
      public IEnumerable<Uri> Links { get; set; }

      public override string ToString()
      {
         return string.Format("Percent: {0}, Host: {1}", Percent, Host);
      }
   }
}