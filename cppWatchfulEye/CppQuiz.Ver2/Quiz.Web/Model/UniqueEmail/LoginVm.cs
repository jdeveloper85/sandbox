﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Quiz.Web.Model.UniqueEmail
{
    [Serializable]
    public class LoginVm
    {
        [Required(ErrorMessage = "Нужно заполнить поле Логина")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Нужно заполнить поле Пароля")]
        public string Password { get; set; }
    }
}