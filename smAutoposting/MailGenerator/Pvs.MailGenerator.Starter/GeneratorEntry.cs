﻿/**
 * Debug mode: cmd >> MailGenerator > debug.log
 */

using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using Pvs.MailGenerator.Common;
using Pvs.MailGenerator.MailGeneratorEntities;

namespace Pvs.MailGenerator.Starter
{
   internal static class GeneratorEntry
   {
      private const string Outfolder = @"D:\sendto\Generated";
      private const string MailTemplate = "MailTpl2.txt";
      private const int MaximumLinkLength = 60;
      private static readonly PropertyInfo[] ContactPropertyInfos = typeof(IContact).GetProperties().ToArray();

      private static readonly string[] ContactPropertyTemplates =
         typeof(IContact).GetProperties().Select(info => string.Format("{0}{1}{2}", '{', info.Name, '}')).ToArray();

      private static readonly string BitlyLogin = ConfigurationManager.AppSettings["BitlyLogin"];
      private static readonly string BitlyApiKey = ConfigurationManager.AppSettings["BitlyApiKey"];
      private static readonly Random GenRandom = new Random();
      private static readonly Encoding DefaultFileEncoding = Encoding.Default;
      private static readonly string[] WordLines = File.ReadLines(MailTemplate, DefaultFileEncoding).ToArray();

      private static void Main()
      {
         using (var genContext = new MailGeneratorContext())
         {
            int recordCount;

            do
            {
               recordCount = (from contact in genContext.Contacts
                              where contact.Generated == null
                              select contact).Count();
               if (recordCount == 0)
               {
                  break;
               }

               var person = (from contact in genContext.Contacts
                             where contact.Generated == null
                             orderby contact.Id
                             select contact).FirstOrDefault();

               if (person == null)
               {
                  Console.WriteLine("There is no contacts");
                  break;
               }

               if (string.IsNullOrEmpty(person.Link))
               {
                  person.Generated = false;
                  Console.WriteLine("Empty link in id: {0}", person.Id);
                  genContext.SaveChanges();
                  continue;
               }

               var orderIndex = person.Id;
               IContact contactImpl = new ContactImpl
               {
                  Name = person.Name,
                  Mail = person.Mail,
                  LinkText = person.Text,
                  LinkUrl = person.Link,
                  BitlyLink = person.BitlyLink
               };

               if (contactImpl.LinkUrl.Length > MaximumLinkLength && string.IsNullOrEmpty(person.BitlyLink))
               {
                  var bitlyApiWrapper = new BitlyApiWrapper(BitlyLogin, BitlyApiKey);
                  try
                  {
                     person.BitlyLink =
                        contactImpl.BitlyLink = contactImpl.LinkUrl = bitlyApiWrapper.Shorten(contactImpl.LinkUrl);
                     Thread.Sleep(TimeSpan.FromSeconds(GenRandom.Next(4, 7)));
                  }
                  catch (WebException webEx)
                  {
                     Console.WriteLine("Spanned by bitly: {0}", webEx.Message);
                     Thread.Sleep(TimeSpan.FromMinutes(GenRandom.Next(2, 5)));
                  }
               }
               else
               {
                  person.BitlyLink = contactImpl.BitlyLink = contactImpl.LinkUrl;
               }

               GenerateMailMessage(contactImpl, orderIndex);
               person.Generated = true;
               genContext.SaveChanges();
               Console.WriteLine("Generated mail message for Id: {0}", orderIndex);
            } while (recordCount > 0);
         }
      }

      private static void GenerateMailMessage(IContact contact, int orderIndex)
      {
         var outputLines = GenerateOutput(contact);
         var orderStr = GetOrderStr(orderIndex);
         GenerateMailFile(orderStr, contact, outputLines);
      }

      private static void GenerateMailFile(string orderStr, IContact contact, string outputLines)
      {
         File.WriteAllText(
            string.Format("{0}{1}{2}_({3}).txt", Outfolder, Path.DirectorySeparatorChar, orderStr, contact.Mail),
            outputLines, DefaultFileEncoding);
      }

      private static string GetOrderStr(long index)
      {
         const int orderLen = 4;
         const char orderChar = '0';
         var orderStr = string.Format("{0}", index);
         while (orderStr.Length < orderLen)
         {
            orderStr = orderChar + orderStr;
         }

         return orderStr;
      }

      private static string GenerateOutput(IContact contact)
      {
         var outputLines = new StringBuilder();
         foreach (var replacedLine in WordLines.Select(line => new StringBuilder(line)))
         {
            for (var index = 0; index < ContactPropertyTemplates.Length; index++)
            {
               replacedLine.Replace(ContactPropertyTemplates[index],
                  ContactPropertyInfos[index].GetValue(contact).ToString());
            }

            outputLines.AppendLine(replacedLine.ToString());
         }

         return outputLines.ToString();
      }
   }
}