﻿using System;
using System.Web.UI;
using Pvs.Quiz.Common;
using Quiz.Web.Utils.Web;

namespace Quiz.Web
{
   public partial class TestComplete : Page
   {
      protected void Page_Load(object sender, EventArgs e)
      {
         if (!IsPostBack)
         {
            var sessionHelper = SessionHelper.GetOrCreate(Session);
            var answerStat = sessionHelper.AnswerStat;
            int rightValue;
            if (answerStat != null && answerStat.TryGetValue(AnswerState.Right, out rightValue))
            {
               TestResultLabel.Text = string.Format("Number of correct answers - {0}", rightValue);
            }
            sessionHelper.Clear();
         }
      }
   }
}