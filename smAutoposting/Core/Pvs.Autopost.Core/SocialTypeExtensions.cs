﻿using System;

namespace Pvs.Autopost.Core
{
   /// <summary>
   ///    Методы расширения для перечисления SocialType
   /// </summary>
   public static class SocialTypeExtensions
   {
      /// <summary>
      ///    Получение максимального предела символов для сообщений
      /// </summary>
      /// <param name="aSocialType">Тип социальной сети</param>
      /// <returns>Максимальный предел символов для сообщений</returns>
      public static int GetPostLimit(this SocialType aSocialType)
      {
         var postLimitAttributes =
            (PostLimitAttribute[])
               aSocialType.GetType()
                  .GetField(aSocialType.ToString())
                  .GetCustomAttributes(typeof (PostLimitAttribute), false);
         if (postLimitAttributes.Length != 1)
         {
            throw new InvalidOperationException(string.Format("Invalid number of attributes PostLimitAttribute in {0}",
               aSocialType.GetType()));
         }

         return postLimitAttributes[0].PostLimit;
      }

      /// <summary>
      /// Получение длины усечения ссылки
      /// </summary>
      /// <param name="aSocialType">Тип социальной сети</param>
      /// <returns>Длина усечения ссылки</returns>
      public static int GetLinkTruncationLength(this SocialType aSocialType)
      {
         var postLimitAttributes =
            (PostLimitAttribute[])
               aSocialType.GetType()
                  .GetField(aSocialType.ToString())
                  .GetCustomAttributes(typeof(PostLimitAttribute), false);
         if (postLimitAttributes.Length != 1)
         {
            throw new InvalidOperationException(string.Format("Invalid number of attributes PostLimitAttribute in {0}",
               aSocialType.GetType()));
         }

         return postLimitAttributes[0].LinkTruncationLength;
      }
   }
}