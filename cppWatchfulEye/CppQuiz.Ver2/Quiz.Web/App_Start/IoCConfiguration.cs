﻿using CppQuiz.Data;
using Ninject;
using Quiz.Web.Infrastructure.Abstract;
using Quiz.Web.Infrastructure.Concrete;

namespace Quiz.Web
{
   public static class IoCConfiguration
   {
      public static void SetupDi(IKernel kernel)
      {
         kernel.Bind<ICrudQuestionRepository<QuizQuestion>>().To<CrudQuizEntityRepository>();
      }
   }
}