﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="EditPosts.aspx.cs"
    Inherits="PostEditor.EditPosts"
    ValidateRequest="false" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="MiddleContent" ContentPlaceHolderID="MiddleContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="AutopostUpdatePanel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Timer ID="AutopostTimer" Interval="60000" runat="server" />
            <asp:ListView runat="server" ID="AutopostListView" DataKeyNames="PostId"
                OnItemInserting="AutopostListView_OnItemInserting" OnItemUpdating="AutopostListView_OnItemUpdating"
                ItemType="Pvs.Autoposting.Entities.Post" InsertItemPosition="FirstItem"
                UpdateMethod="UpdatePost"
                InsertMethod="InsertPost"
                SelectMethod="GetPosts"
                DeleteMethod="DeletePost">
                <EditItemTemplate>
                    <li class="edit">
                        <table class="postTable">
                            <tr>
                                <td colspan="2" class="error">
                                    <asp:ValidationSummary ID="EditPostValidationSummary" DisplayMode="BulletList" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>Post text</td>
                                <td>
                                    <textarea runat="server" id="postText" rows="30" cols="80" value="<%#: BindItem.PostText %>" /></td>
                            </tr>
                            <tr>
                                <td>Social settings</td>
                                <td>
                                    <asp:CheckBoxList runat="server" ID="SocialCheckBoxList" SelectMethod="GetSocialTypes" ItemType="System.String">
                                        <asp:ListItem Text="All" Selected="True" />
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                        </table>
                    </li>
                    <div class="nav">
                        <asp:Button runat="server" CssClass="actionButtons" ID="UpdatePostButton" CommandName="Update" Text="Update post" />
                        <asp:Button runat="server" CssClass="actionButtons" ID="CancelEditButton" CommandName="Cancel" Text="Cancel" />
                        <asp:Button runat="server" CssClass="actionButtons" ID="DeletePostButton" CommandName="Delete" Text="Delete post" />
                    </div>
                </EditItemTemplate>
                <EmptyDataTemplate>
                    No Data
                </EmptyDataTemplate>
                <InsertItemTemplate>
                    <li class="insert">
                        <table class="postTable">
                            <tr>
                                <td colspan="4">
                                    <asp:ValidationSummary runat="server" ID="InsertPostValidationSummary" DisplayMode="BulletList" />
                                </td>
                            </tr>
                            <tr>
                                <td>URI</td>
                                <td>
                                    <input type="text" runat="server" id="pureUri" size="50" maxlength="255" value="<%#: BindItem.PureUri %>" /></td>
                                <td>Social settings</td>
                                <td rowspan="2">
                                    <asp:CheckBoxList runat="server" ID="SocialCheckBoxList" SelectMethod="GetSocialTypes" ItemType="System.String">
                                        <asp:ListItem Text="All" Selected="True" />
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="2">Post text</td>
                                <td colspan="2" rowspan="2">
                                    <textarea runat="server" id="postText" rows="20" cols="60" value="<%#: BindItem.PostText %>" />
                                </td>
                            </tr>
                        </table>
                        <div class="nav">
                            <asp:Button ID="InsertPostButton" CssClass="actionButtons" runat="server" CommandName="Insert" Text="Insert post" />
                            <asp:Button ID="CancelButton" CssClass="actionButtons" runat="server" CommandName="Cancel" Text="Cancel" />
                        </div>
                    </li>
                </InsertItemTemplate>
                <ItemSeparatorTemplate>
                    <hr class="item-separator" />
                </ItemSeparatorTemplate>
                <ItemTemplate>
                    <li class="item-template">
                        <table class="postTable">
                            <tr>
                                <td>URI</td>
                                <td><%# GetPureUri(Item) %></td>
                            </tr>
                            <tr>
                                <td>Post text</td>
                                <td><%# GetPostText(Item) %></td>
                            </tr>
                            <tr>
                                <td>Created date</td>
                                <td><%# Item.CreatedDate %></td>
                            </tr>
                            <tr>
                                <td>Social networks</td>
                                <td><%# GetSocialTypes(Item) %></td>
                            </tr>
                            <tr>
                                <td>Priority</td>
                                <td><%# GetPriority(Item) %></td>
                            </tr>
                            <tr>
                                <td>Pending to post</td>
                                <td><%# GetPending(Item) %></td>
                            </tr>
                            <tr>
                                <td>Is posted</td>
                                <td><%# GetPosted(Item) %></td>
                            </tr>
                            <tr>
                                <td>Resource availability</td>
                                <td><%# GetResourceAvailable(Item) %></td>
                            </tr>
                            <tr>
                                <td>Posted date</td>
                                <td><%# GetPostedDate(Item) %></td>
                            </tr>
                            <tr>
                                <td>Error message</td>
                                <td><%#: GetErrorMessage(Item) %></td>
                            </tr>
                        </table>
                    </li>
                    <div class="nav">
                        <asp:Button runat="server" CssClass="actionButtons" ID="EditPostButton" CommandName="Edit" Text="Edit post" />
                        <asp:Button runat="server" CssClass="actionButtons" ID="DeletePostButton" CommandName="Delete" Text="Delete post" />
                    </div>
                </ItemTemplate>
                <LayoutTemplate>
                    <table style="float: right;">
                        <th>
                            <td>Sort By:  </td>
                            <td>
                                <asp:LinkButton runat="server" CommandName="Sort" CommandArgument="CreatedDate" Text="Created date" /></td>
                            <td>
                                <asp:LinkButton runat="server" CommandName="Sort" CommandArgument="IsPriority" Text="Priority" /></td>
                            <td>
                                <asp:LinkButton runat="server" CommandName="Sort" CommandArgument="IsPosted" Text="Posted" /></td>
                            <td>
                                <asp:LinkButton runat="server" CommandName="Sort" CommandArgument="PostedDate" Text="Posted date" /></td>
                        </th>
                    </table>
                    <ul id="itemPlaceholderContainer" runat="server" class="layout-template">
                        <li runat="server" id="itemPlaceholder" />
                    </ul>
                    <div class="layout-template-pager">
                        <asp:DataPager ID="PostBottomDataPager" PageSize="5" runat="server">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowNextPageButton="True"
                                    ShowPreviousPageButton="True" PreviousPageText="Prev" NextPageText="Next"
                                    ButtonCssClass="pager" />
                                <asp:NumericPagerField NumericButtonCssClass="pager" />
                                <asp:NextPreviousPagerField ButtonType="Button" ShowLastPageButton="True" ShowNextPageButton="False"
                                    ShowPreviousPageButton="True" PreviousPageText="Prev"
                                    ButtonCssClass="pager" />
                            </Fields>
                        </asp:DataPager>
                    </div>
                </LayoutTemplate>
            </asp:ListView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
