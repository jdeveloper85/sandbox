USE [1gb_watchfuleye]
GO

IF EXISTS (
       SELECT *
       FROM   INFORMATION_SCHEMA.TABLES
       WHERE  TABLE_NAME = 'Question'
   )
    DROP TABLE Question
GO

CREATE TABLE [dbo].[Question]
(
	[QuestionId]           INT IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[CodeFragment]         VARCHAR(4096) NOT NULL,
	[ProjectName]          NVARCHAR(64) NOT NULL,
	[ProjectLogo]          VARBINARY(MAX) NULL,
	[ExplanatoryText]      NVARCHAR(MAX) NULL,
	[PvsStudioWarning]     NVARCHAR(MAX) NULL
)