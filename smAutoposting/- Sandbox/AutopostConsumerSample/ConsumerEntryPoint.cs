﻿using System;
using Pvs.Autopost.Core;
using Pvs.Autoposting.ConsumerServer;

namespace AutopostConsumerSample
{
   internal static class ConsumerEntryPoint
   {
      private static void Main()
      {
         IPostService autopostConsumer = new AutopostConsumer(AutopostLoggerWrapper.GetInstance(Loggers.Autoconsumer));
         autopostConsumer.Start();

         Console.ReadLine();
      }
   }
}