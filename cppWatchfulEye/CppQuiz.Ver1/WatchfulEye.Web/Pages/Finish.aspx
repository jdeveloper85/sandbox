﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Main.Master" AutoEventWireup="true" CodeBehind="Finish.aspx.cs"
    Inherits="WatchfulEye.Web.Pages.Finish" %>

<%@ OutputCache Location="None" %>
<%@ Import Namespace="Pvs.Quiz.Common" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div class="text-content">
        <div class="center">
            <h2 style="text-align: center;">Test complete</h2>
        </div>
        <div class="center">
            You are surely tired. And this is a great representation of how useful static code analysis tools can be.
            There is no need to manually search through your source code for simple misprints and silly errors.
            Static analyzer can automatically find many of such errors for you, so you can spend your time for something
            more useful (for example, for drinking your coffee)! No one is safe from such kind of errors,
            even such a well-known and high quality projects, as Chromium, Qt and MySQL.
        </div>
        <div class="center" id="found-errors">
            You have found&nbsp;<asp:Label runat="server" ID="TestResultLabel" />
        </div>
        <div class="center">Congratulations! You have earned the title of:</div>
        <div class="user-level">
            <!-- Изображения со статусами -->
            <div class="DynamicColumns">
                <table>
                    <%
                        var userLevels = Enum.GetValues(typeof (CppUserLevel)).Cast<CppUserLevel>().ToArray();
                        for (int i = 0; i < userLevels.Length; i += 2)
                        {
                            if (i % 2 == 0)
                                Response.Write("<tr>");
                            for (int j = i; j < i + 2; j++)
                            {
                                if (j < userLevels.Length)
                                {
                                    Response.Write("<td>");
                    %>
                    <div class="ImageDiv <% Response.Write(userLevels[j] == Gradation.Level ? "selected" : "not-selected"); %>">
                        <img style="float: left;" src="<%= userLevels[j].GetLevelImage() %>" alt="<%= GetDefinition(userLevels[j]) %>"
                            title="<%= GetDefinition(userLevels[j]) %>" />
                        <h3><%= GetDefinition(userLevels[j]) %></h3>
                    </div>
                    <%
                                            Response.Write("</td>");
                                }
                            }
                            if (i % 2 == 0)
                                Response.Write("</tr>");
                        }
                    %>
                </table>
            </div>
        </div>
        <div class="center">
            <!-- Кнопки социальных сетей -->
            <h3 style="text-align: center;">You can share the results</h3>
            <Eye:Social runat="server" ID="SocialContent" />
            <%--<table style="margin-top: 15px; width: 100%;">
                <tr>
                    <td>
                        <!-- G+ -->
                        <div class="g-plusone" data-href="<%= GetPostLevelUrl %>"></div>
                        <script type="text/javascript">
                            window.___gcfg = { lang: 'en', parsetags: 'onload' };
                            (function () {
                                var po = document.createElement('script');
                                po.type = 'text/javascript';
                                po.async = true;
                                po.src = 'https://apis.google.com/js/platform.js';
                                var s = document.getElementsByTagName('script')[0];
                                s.parentNode.insertBefore(po, s);
                            })();
                        </script>
                    </td>
                    <td>
                        <!-- Tweeter -->
                        <a href="https://twitter.com/share" class="twitter-share-button"
                            data-via=""
                            data-url="http://q.viva64.com"
                            data-size="large"
                            data-text="<%= Definition %>">Tweet</a>
                        <script type="text/javascript">
                            !function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                                if (!d.getElementById(id)) {
                                    js = d.createElement(s);
                                    js.id = id;
                                    js.src = p + '://platform.twitter.com/widgets.js';
                                    fjs.parentNode.insertBefore(js, fjs);
                                }
                            }(document, 'script', 'twitter-wjs');
                        </script>
                    </td>
                    <td>
                        <!-- Facebook -->
                        <div class="fb-like" data-href="<%= GetPostLevelUrl %>"
                            data-layout="standard" data-action="like" data-show-faces="true" data-share="true">
                        </div>
                    </td>
                </tr>
            </table>--%>
        </div>
        <div class="center">
            <div style="text-align: center;">
                <a class="actionButtons" href="/">Restart Test&nbsp;&nbsp;&nbsp;</a>
            </div>
        </div>
        <div class="center">
            <h3 style="text-align: center;">Additional links</h3>
        </div>
        <div class="center">
            <ul>
                <li>Get acquainted with static analysis methodology by downloading
                    <a class="standart" target="blank" href="http://www.viva64.com/en/pvs-studio/">PVS-Studio</a> analyzer
                </li>
                <li><a class="standart" target="blank" href="http://www.viva64.com/en/examples/">Here</a>
                    you can read about other bugs, which <a class="standart" target="blank" href="http://www.viva64.com/en/pvs-studio/">PVS-Studio</a>
                    analyzer had found in some other popular open source projects.
                </li>
                <li>Follow our twitter: <a class="standart" href="https://twitter.com/Code_Analysis">@Code_Analysis</a>.
                    Here we post regular reports about bugs in open source projects and other news from C++ world.
                </li>
            </ul>
        </div>
    </div>
</asp:Content>
<asp:Content ID="ResultContent" ContentPlaceHolderID="ResultContentPlaceHolder" runat="server">
</asp:Content>
