﻿<%@ Page Title="Start page" Language="C#" MasterPageFile="~/Site.Mobile.Master" AutoEventWireup="true" CodeBehind="Simple.Mobile.aspx.cs" Inherits="Quiz.Web.Simple_Mobile" %>
<%@ Import Namespace="Pvs.Quiz.Common" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContentPlaceHolder">
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-4 visible-lg">
                <img src="Pics/dummy.png" alt="" title="" class="img-responsive"/>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
                <h2>C++ Quiz: are you a code guru?</h2>
            </div>
            <div class="col-md-4 visible-lg">
                <img src="Pics/level5.png" alt="" title="" class="img-responsive"/>
            </div>
        </div>
    </div>
    <div class="container">
        Test your C++ skills - find bugs in popular open-source projects.
    </div>
    <div class="container">
        Do you think that it is impossible to make silly mistakes in your own code? Developers of
        <a target="blank" href="http://www.viva64.com/en/pvs-studio/">PVS-Studio</a> C/C++ static analyzer are inclined to disagree!
    </div>
    <div class="container">
        Code analyzers work non-stop and can find many bugs which are otherwise hard to notice. Here we've gathered
        many fragments from well-known open source projects in which static analyzer had found real bugs.
    </div>
    <div class="container">
        We offer you to compete against
        <a target="blank" href="http://www.viva64.com/en/pvs-studio/">PVS-Studio</a>
        static analyzer by locating bugs in <%= WebConfigReader.TestQuestionsNumber %> source code fragments
        randomly selected from our database of open source projects.
        You get a point for a correct answer if it was given within <%= WebConfigReader.QuestionTime %> seconds.
    </div>
    <div class="container">
        Please note that all the examples that appear in the test questions are compiled, ie
        can't be error syntax! (<a href="http://www.viva64.com/en/b/0222/">PVS-Studio for Visual C++</a>).
        Test but more on care than on knowledge. The real test, you can see <a href="http://cppquiz.org/quiz/clear">here</a>
    </div>
    <div class="container pull-right">        
        <a href="TestQuestion.aspx" class="ui-btn text-center">Begin Test</a>
    </div>
</asp:Content>