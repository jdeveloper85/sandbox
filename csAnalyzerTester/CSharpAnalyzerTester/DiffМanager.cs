﻿//  2006-2008 (c) Viva64.com Team
//  2008-2016 (c) OOO "Program Verification Systems"
using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using Microsoft.Win32;
using ProgramVerificationSystems.PVSStudio;
using ProgramVerificationSystems.PVSStudio.CommonTypes;
using Resources = CSharpAnalyzerTester.Properties.Resources;
// TODO: Inject Out
// TODO: Remove reduntant catch blocks
namespace CSharpAnalyzerTester
{
    public static class DiffМanager
    {
        private const string CSharpTesterMutex = "MutexSyncSettingsUpdate";

        private static readonly string PvsAppDataFolder =
            Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PVS-Studio\\";

        private static readonly string PvsStudioSettings = PvsAppDataFolder + "Settings.xml";
        private static readonly string PvsStudioSettingsBeckup = PvsAppDataFolder + "Settings.xml.tbeckup";

        private static readonly string ProcessCounterFile =
            Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + Path.DirectorySeparatorChar + "DevenvRuns.dat";

        private static readonly XmlSerializer Serializer = new XmlSerializer(typeof(ApplicationSettings));
        private static ApplicationSettings _settings;

        public static int OpenDiffInDevenv(string[] args)
        {
            EnvironmentUtils.ExecuteSynchronousOperationThroughMutex(BackupSettings, CSharpTesterMutex);
            StartDevenv(args[1], args[2]);
            EnvironmentUtils.ExecuteSynchronousOperationThroughMutex(RestoreSettings, CSharpTesterMutex);

            return 0;
        }

        public static void InitDevenv(string[] args)
        {
            if (args.Length < 2)
                return;

            try
            {
                var logFileNameDiffs = args[1];

                if (!File.Exists(logFileNameDiffs))
                {
                    Console.WriteLine(Resources.AnalysisPlogNotFound, Resources.None, logFileNameDiffs);
                    return;
                }

                var solPath = new DataTable { TableName = "Solution_Path" };
                solPath.Columns.Add("SolutionPath", typeof(string)); //0
                solPath.Columns.Add("SolutionVersion", typeof(string)); //1
                solPath.ReadXml(logFileNameDiffs);

                var ideVersionNotFound = Resources.IdeVersionNotFound;
                if (solPath.Rows.Count == 0)
                {
                    Console.WriteLine(ideVersionNotFound);
                    return;
                }

                var ideVersionString = solPath.Rows[0][1] as string;

                if (string.IsNullOrWhiteSpace(ideVersionString))
                {
                    Console.WriteLine(ideVersionNotFound);
                    return;
                }

                string vsPath;
                if (!IsInstalledVsVersion(ideVersionString, out vsPath))
                {
                    Console.WriteLine(Resources.IdeVersionOfVSNotFound, ideVersionString);
                    return;
                }                

                var devenv = string.Format("{0}devenv.exe", vsPath);
                if (!File.Exists(devenv))
                {
                    Console.WriteLine(Resources.IdeVersionOfVSNotFound, ideVersionString);
                    return;
                }

                var runner = new Process
                {
                    StartInfo =
                    {
                        FileName = Process.GetCurrentProcess().MainModule.FileName,
                        UseShellExecute = false,
                        CreateNoWindow = true,
                        Arguments = string.Format("--run \"{0}\" \"{1}\"", devenv, logFileNameDiffs)
                    }
                };
                runner.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private static void StartDevenv(string idePath, string plogPath)
        {
            try
            {
                var devenv = new Process
                {
                    StartInfo =
                    {
                        FileName = idePath,
                        Arguments = string.Format("/command \"PVSStudio.OpenAnalysisReport {0}\"", plogPath),
                        UseShellExecute = false
                    }
                };
                devenv.Start();
                devenv.WaitForExit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private static void BackupSettings()
        {
            var count = 0;

            try
            {
                if (File.Exists(ProcessCounterFile))
                {
                    var devenvCount = File.ReadAllText(ProcessCounterFile);
                    count = Math.Abs(int.Parse(devenvCount));
                }

                backup:
                if (count == 0)
                {
                    using (var fs = new FileStream(PvsStudioSettings, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
                    {
                        _settings = (ApplicationSettings)Serializer.Deserialize(fs);
                    }

                    if (_settings != null)
                    {
                        File.Copy(PvsStudioSettings, PvsStudioSettingsBeckup, true);
                        _settings.SourceTreeRoot = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

                        using (var fs = new FileStream(PvsStudioSettings, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
                        {
                            TextWriter writer = new StreamWriter(fs, Encoding.Unicode);
                            Serializer.Serialize(writer, _settings);
                        }
                    }
                }
                else
                {
                    //Если файл со счётчиком имеет записи, но текущий процесс один, то делаем бэкап
                    var processes = Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName);
                    if (processes.Length == 1)
                    {
                        count = 0;
                        goto backup;
                    }
                }

                count += 1;

                File.WriteAllText(ProcessCounterFile, count.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private static void RestoreSettings()
        {
            var count = 0;

            try
            {
                if (File.Exists(ProcessCounterFile))
                {
                    var devenvCount = File.ReadAllText(ProcessCounterFile);
                    count = Math.Abs(int.Parse(devenvCount));
                }

                restore:
                if (count == 1)
                {
                    if (File.Exists(PvsStudioSettingsBeckup))
                        File.Copy(PvsStudioSettingsBeckup, PvsStudioSettings, true);

                    File.Delete(ProcessCounterFile);
                    File.Delete(PvsStudioSettingsBeckup);

                    return;
                }
                //Если количество процессов равно 1 (т.е. остался только текущий процесс), то делаем восстановление настроек
                var processes = Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName);
                if (processes.Length == 1)
                {
                    count = 1;
                    goto restore;
                }

                count -= 1;
                count = (count < 0) ? 0 : count;

                File.WriteAllText(ProcessCounterFile, count.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private static bool IsInstalledVsVersion(string versionNumber, out string devenvPath)
        {
            var regPath = Environment.Is64BitProcess
                ? "HKEY_LOCAL_MACHINE\\Software\\Wow6432Node\\Microsoft\\VisualStudio\\"
                : "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\VisualStudio\\";
            regPath += versionNumber;            
            devenvPath = Registry.GetValue(regPath, "InstallDir", string.Empty) as string;
            return !string.IsNullOrEmpty(devenvPath);            
        }
    }
}