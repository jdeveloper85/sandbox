﻿using System;

namespace Pvs.Autopost.Core
{
   /// <summary>
   ///    Атрибут для задания предельного количества символов в тексте
   /// </summary>
   [AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = false)]
   public sealed class PostLimitAttribute : Attribute
   {
      private const int DefaultPostLimit = 0x400;
      private const int DefaultLinkLength = 0x100;

      /// <summary>
      ///    Конструктор атрибута для задания предельного количества символов в тексте
      /// </summary>
      /// <param name="postLimit">Предельное количество символов в тексте</param>
      /// <param name="linkTruncationLength">Длина усечения ссылки</param>
      public PostLimitAttribute(int postLimit, int linkTruncationLength)
      {
         PostLimit = postLimit;
         LinkTruncationLength = linkTruncationLength;
      }

      /// <summary>
      ///    Конструктор атрибута для задания предельного количества символов в тексте
      /// </summary>
      public PostLimitAttribute()
         : this(DefaultPostLimit, DefaultLinkLength)
      {
      }

      /// <summary>
      ///    Предельное количество символов в тексте
      /// </summary>
      public int PostLimit { get; set; }

      /// <summary>
      /// Длина усечения ссылки
      /// </summary>
      public int LinkTruncationLength { get; set; }
   }
}