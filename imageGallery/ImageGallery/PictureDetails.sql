﻿CREATE TABLE PictureDetails
(
	PictureId                  INT NOT NULL,
	ShootingDate               DATETIME,
	CameraManufacturer         VARCHAR(256),
	CameraModel                VARCHAR(256),
	FocalLength                INT,
	ExposureTime               FLOAT(8),
	ShutterSpeedValue          FLOAT(8),
	FNumber                    INT,
	CompressedBitsPerPixel     VARCHAR(32),
	CONSTRAINT PictureDetails_PictureId_PK PRIMARY KEY(PictureId),
	CONSTRAINT PictureDetails_PictureId_PictureGallery_FK FOREIGN KEY(PictureId)
	REFERENCES PictureGallery(PictureId) ON UPDATE CASCADE ON DELETE CASCADE
)