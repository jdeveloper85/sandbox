﻿using System;
using System.Net.Http.Formatting;
using Quiz.Web.Model;

namespace Quiz.Web.Utils.Web
{
   public static class QuestionApiControllerFormValueParser
   {
      public static bool ParseFormValues(FormDataCollection form, out AnswerRequest answerRequest,
         out string errorMessage)
      {
         var action = form["action"];
         answerRequest = new AnswerRequest();

         AnswerRequestKind answerRequestKind;
         if (!Enum.TryParse(action, true, out answerRequestKind))
         {
            errorMessage = string.Format("Detecting unknown action: {0}", action);
            answerRequest.DataIndex = 0;
            answerRequest.CurrentQuestionNumber = 0;
            return false;
         }

         var oDataIndex = form["dataIndex"];
         int dataIndex;
         if (!int.TryParse(oDataIndex, out dataIndex))
         {
            errorMessage = String.Format("Detecting wrong data index: {0}", oDataIndex);
            answerRequest.AnswerRequestKind = AnswerRequestKind.None;
            answerRequest.DataIndex = -1;
            return false;
         }

         var oQuestionId = form["questionId"];
         int questionId;
         if (!int.TryParse(oQuestionId, out questionId))
         {
            errorMessage = String.Format("Undefined question: {0}", oQuestionId);
            answerRequest.AnswerRequestKind = AnswerRequestKind.None;
            answerRequest.DataIndex = -1;
            return false;
         }

         errorMessage = string.Empty;
         answerRequest.AnswerRequestKind = answerRequestKind;
         answerRequest.DataIndex = dataIndex;
         answerRequest.CurrentQuestionNumber = questionId;

         return true;
      }
   }
}