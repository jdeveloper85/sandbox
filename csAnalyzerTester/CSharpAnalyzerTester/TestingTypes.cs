﻿//  2006-2008 (c) Viva64.com Team
//  2008-2016 (c) OOO "Program Verification Systems"
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using CSharpAnalyzerTester.Env;
using CSharpAnalyzerTester.Helpers;
using CSharpAnalyzerTester.Properties;

namespace CSharpAnalyzerTester
{
    [Serializable]
    public class TestProject
    {
        public string Configuration { get; set; }
        public string Platform { get; set; }
        public string SolutionPath { get; set; }

        public override string ToString()
        {
            return string.Format("SolutionPath: {0}, Configuration: {1}, Platform: {2}.",
                SolutionPath, Configuration, Platform);
        }

        public static void SaveToFile(TestProject[] projectList, string settringsPath)
        {
            var formatter = new XmlSerializer(typeof(TestProject[]));
            using (var fs = new FileStream(settringsPath, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, projectList);
            }
        }

        public static TestProject[] LoadFromFile(string settringsPath)
        {
            TestProject[] projectList = null;

            try
            {
                var formatter = new XmlSerializer(typeof(TestProject[]));
                using (var fs = new FileStream(settringsPath, FileMode.OpenOrCreate))
                {
                    projectList = (TestProject[])formatter.Deserialize(fs);
                }
            }
            catch
            {
                // NOTE: ignored
            }

            return projectList;
        }
    }

    public sealed class TestProjectSummary
    {
        public TestProject TestProject { get; set; }
        public string SolutionName { get; set; }
        public string SavedLogPath { get; set; }
        public string EtalonLogPath { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public TimeSpan Elapsed { get; set; }
        public AnalysisStatus Status { get; set; }
        public IEnumerable<string> AdditionalMessages { get; set; }
        public IEnumerable<string> MissingMessages { get; set; }
        public IEnumerable<string> ModifiedMessages { get; set; }
    }

    public struct RunResult
    {
        public StringBuilder OutputData { get; set; }
        public StringBuilder ErrorData { get; set; }
        public StringBuilder StdOut { get; set; }
        public int ExitCode { get; set; }
        public bool Success { get; set; }
        public bool IsTimeout { get; set; }
    }

    public static class TestProjectExtensions
    {
        private const int DefaultStringBuilderCapacity = 0x100;

        public static RunResult Run(this TestProject project, IEnv env, string logFileName)
        {
            var result = new RunResult
            {
                Success = false,
                ExitCode = 0,
                OutputData = new StringBuilder(DefaultStringBuilderCapacity),
                ErrorData = new StringBuilder(DefaultStringBuilderCapacity),
                StdOut = new StringBuilder(DefaultStringBuilderCapacity)
            };

            Process csAnProcess = null;
            var timeout = SingletonInjector<ConfImpl>.Instance.MaximumAnalysisTime; // минут
            try
            {
                // Задаем аргументы
                csAnProcess = new Process { StartInfo = BuildStartInfo(project, env, logFileName) };
                csAnProcess.OutputDataReceived += (o, e) => result.OutputData.AppendLine(e.Data);
                csAnProcess.ErrorDataReceived += (o, e) => result.ErrorData.AppendLine(e.Data);

                csAnProcess.Start();
                csAnProcess.BeginOutputReadLine();
                csAnProcess.BeginErrorReadLine();

                var successExit = csAnProcess.WaitForExit(60 * timeout * 1000);
                if (!successExit)
                {
                    result.StdOut.AppendLine(string.Format(Resources.AnalysisTimeout, timeout));
                    try
                    {
                        csAnProcess.Kill();
                    }
                    catch (InvalidOperationException)
                    {
                    }

                    result.Success = false;
                    result.IsTimeout = true;
                }
            }
            catch (Exception ex)
            {
                result.OutputData.AppendLine(ex.ToString());
                result.Success = false;
            }
            finally
            {
                try
                {
                    if (csAnProcess != null && csAnProcess.HasExited)
                    {
                        result.ExitCode = csAnProcess.ExitCode;
                    }
                }
                catch (InvalidOperationException)
                {
                    result.ExitCode = 0;
                }
            }

            result.Success = true;
            return result;
        }

        private static ProcessStartInfo BuildStartInfo(TestProject project, IEnv env, string logFileName)
        {
            return new ProcessStartInfo(env.AnalyzerImagePath, BuildArguments(project, env, logFileName))
            {
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };
        }

        private static string BuildArguments(TestProject project, IEnv env, string logFileName)
        {
            var arguments = new StringBuilder(0x100);
            arguments.Append(string.Format(" --target {0}",
                project.SolutionPath.Contains(' ')
                    ? string.Format("\"{0}\"", project.SolutionPath)
                    : project.SolutionPath));
            if (!string.IsNullOrWhiteSpace(project.Configuration))
                arguments.Append(string.Format(" --configuration \"{0}\"", project.Configuration));

            if (!string.IsNullOrWhiteSpace(project.Platform))
                arguments.Append(string.Format(" --platform \"{0}\"", project.Platform));

            arguments.Append(string.Format(" --settings \"{0}\"", env.AnalyzerSettingsPath));
            arguments.Append(string.Format(" --output {0}",
                logFileName.Contains(' ') ? string.Format("\"{0}\"", logFileName) : logFileName));
            if (env.EnableBenchmark)
            {
                arguments.Append(string.Format(" --benchmark {0}", env.EnableBenchmark));
            }

            return arguments.ToString();
        }

        public static string GetSolutionName(this TestProject aTestProject)
        {
            return Path.GetFileNameWithoutExtension(aTestProject.SolutionPath);
        }
    }
}