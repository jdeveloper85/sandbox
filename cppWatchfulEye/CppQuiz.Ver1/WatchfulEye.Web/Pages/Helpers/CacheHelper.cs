﻿using System;
using System.Web.Caching;

namespace WatchfulEye.Web.Pages.Helpers
{
    /// <summary>
    /// Wrapper для удобства обработки элементов ASP.NET кэша
    /// </summary>
    public static class CacheHelper
    {
        #region Обработка логотипа в кэше ASP.NET

        public static void AddProjectLogo(Cache cache, byte[] imageData)
        {
            Add(cache, CacheKey.ProjectLogo, imageData);
        }

        public static byte[] GetCurrentProjectLogo(Cache cache)
        {
            return Get<byte[]>(cache, CacheKey.ProjectLogo);
        }

        public static void RemoveProjectLogo(Cache cache)
        {
            Remove(cache, CacheKey.ProjectLogo);
        }

        #endregion

        #region Обработка ошибок 5хх в кэше ASP.NET

        public static void AddLastServerError(Cache cache, Exception lastException)
        {
            Add(cache, CacheKey.LastServerError, lastException);
        }

        public static Exception GetLastServerError(Cache cache)
        {
            return Get<Exception>(cache, CacheKey.LastServerError);
        }

        public static void RemoveLastServerError(Cache cache)
        {
            Remove(cache, CacheKey.LastServerError);
        }

        #endregion

        #region Private Helpers

        private static void Add<T>(Cache cache, CacheKey cacheKey, T data)
        {
            string cacheKeyName = Enum.GetName(typeof(CacheKey), cacheKey);
            if (cacheKeyName != null)
                cache.Insert(cacheKeyName, data);
        }

        private static T Get<T>(Cache cache, CacheKey cacheKey)
        {
            string cacheKeyName = Enum.GetName(typeof(CacheKey), cacheKey);
            if (cacheKeyName == null)
                return default(T);

            object cacheValue = cache[cacheKeyName];
            return cacheValue is T ? (T)cacheValue : default(T);
        }

        private static void Remove(Cache cache, CacheKey cacheKey)
        {
            string cacheKeyName = Enum.GetName(typeof(CacheKey), cacheKey);
            if (cacheKeyName != null)
                cache.Remove(cacheKeyName);
        }

        #endregion
    }
}