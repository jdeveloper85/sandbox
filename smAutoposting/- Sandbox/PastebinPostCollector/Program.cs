﻿/**
 * Сбор с Pastebin кусков кода на C++
 */

using System;
using Pvs.Autopost.Core;

namespace PastebinPostCollector
{
   internal static class Program
   {
      private const string PastebinPostsDir = "d:\\PastebinPosts";

      private static void Main()
      {
         IPostService pastebinPostService = new Pvs.Pastebin.CodeCollectorServer.PastebinPostCollector(PastebinPostsDir);
         pastebinPostService.Start();
         Console.WriteLine("Press any key to exit...");
         Console.ReadKey();
      }
   }
}