﻿using System;
using System.Web.UI;

namespace WatchfulEye.Web.Controls
{
    public partial class SocialContent : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected static string PostLevelUrl
        {
            get { return "/Pages/PostLevels/Neutral.aspx"; }
        }
    }
}