﻿using System;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

namespace VkApi.Library
{
   public static class PdfUtils
   {
      private const string AuthorDetectString = "Автор"; // Строка, на которой условно заканчивается заголовок документа

      public static string GetPdfTitle(string pdfFileName, string endTitleDetectionString = AuthorDetectString)
      {
         string pdfTitle = string.Empty;

         using (var pdfReader = new PdfReader(pdfFileName))
         {
            if (pdfReader.NumberOfPages >= 1)
            {
               ITextExtractionStrategy extractionStrategy = new SimpleTextExtractionStrategy();
               string currentPageText = PdfTextExtractor.GetTextFromPage(pdfReader, 1, extractionStrategy);
               if (!string.IsNullOrWhiteSpace(currentPageText))
               {
                  int authorStrIndex = currentPageText.IndexOf(endTitleDetectionString,
                     StringComparison.CurrentCultureIgnoreCase);
                  if (authorStrIndex > 0)
                  {
                     string title = currentPageText.Substring(0, authorStrIndex);
                     if (!string.IsNullOrWhiteSpace(title))
                        pdfTitle = title;
                  }
               }
            }
         }

         if (string.IsNullOrEmpty(pdfTitle))
            pdfTitle = Path.GetFileNameWithoutExtension(pdfFileName);

         return pdfTitle;
      }
   }
}