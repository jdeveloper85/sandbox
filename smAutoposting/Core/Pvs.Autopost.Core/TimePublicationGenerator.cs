﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pvs.Autopost.Core
{
   public sealed class TimePublicationGenerator
   {
      private readonly int _endPostingHour;
      private readonly int _maxSecondValue;
      private readonly int _minSecondValue;
      private readonly ISet<DateTime> _scheduledTimes = new SortedSet<DateTime>();
      private readonly Random _secondValueRnd = new Random();
      private readonly int _startPostingHour;

      public TimePublicationGenerator(int minSecondValue, int maxSecondValue, int startPostingHour, int endPostingHour)
      {
         _minSecondValue = minSecondValue;
         _maxSecondValue = maxSecondValue;
         _startPostingHour = startPostingHour;
         _endPostingHour = endPostingHour;
      }

      public int MinSecondValue
      {
         get { return _minSecondValue; }
      }

      public int MaxSecondValue
      {
         get { return _maxSecondValue; }
      }

      public int StartPostingHour
      {
         get { return _startPostingHour; }
      }

      public int EndPostingHour
      {
         get { return _endPostingHour; }
      }

      public void Regenerate()
      {
         if (_scheduledTimes.Count > 0)
         {
            _scheduledTimes.Clear();
         }

         DateTime nowDate = DateTime.Now;
         var lastGeneratedDate = new DateTime(nowDate.Year, nowDate.Month, nowDate.Day, _startPostingHour, 0, 0);
         var thresholdDate = new DateTime(nowDate.Year, nowDate.Month, nowDate.Day, _endPostingHour, 0, 0);

         while (lastGeneratedDate < thresholdDate)
         {
            int nextIntervalValue = _secondValueRnd.Next(_minSecondValue, _maxSecondValue);
            lastGeneratedDate = lastGeneratedDate.AddSeconds(nextIntervalValue);
            if (lastGeneratedDate <= thresholdDate)
            {
               _scheduledTimes.Add(lastGeneratedDate);
            }
         }
      }

      public bool Peek(out DateTime peekedDate)
      {
         peekedDate = _scheduledTimes.FirstOrDefault();
         return peekedDate != default(DateTime);
      }

      public bool Remove(DateTime valueToRemove)
      {
         return _scheduledTimes.Remove(valueToRemove);
      }

      public bool IsEmpty()
      {
         return _scheduledTimes.Count <= 0;
      }
   }
}