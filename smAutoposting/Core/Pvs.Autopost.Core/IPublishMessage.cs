﻿namespace Pvs.Autopost.Core
{
   /// <summary>
   /// Интерфейс для публикации сообщения в соц. сети
   /// </summary>
   public interface IPublishMessage
   {
      /// <summary>
      /// Опубликовать сообщение
      /// </summary>
      /// <param name="message">Сообщение для публикации</param>
      /// <returns>true, если сообщение успешно опубликовано, false - в противном случае</returns>
      bool Publish(string message);
   }
}