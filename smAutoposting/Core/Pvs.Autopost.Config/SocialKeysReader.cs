﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Pvs.Autopost.Config
{
   /// <summary>
   ///    Чтение конфигурации для социальных сетей
   /// </summary>
   public static class SocialKeysReader
   {
      public const string PriorityHost = "www.viva64.com";
      private static readonly AppSettingsReader AppSettingsReader = new AppSettingsReader();

      /// <summary>
      ///    Интервал между опросами в секундах
      /// </summary>
      public static int PollingInterval
      {
         get { return Get<int>("PollingInterval"); }
      }

      /// <summary>
      ///    Кол-во постов, посылаемых или извлекаемых из Msmq за один раз
      /// </summary>
      public static int PostLimit
      {
         get { return Get<int>("PostLimit"); }
      }

      /// <summary>
      ///    Максимальный процент ссылок для публикации на один домен
      /// </summary>
      public static int PercentDomainLimit
      {
         get { return Get<int>("PercentDomainLimit"); }
      }

      /// <summary>
      ///    Минимальное время в часах, с которого можно начинать публикацию
      /// </summary>
      public static int StartPostingHour
      {
         get { return Get<int>("StartPostingHour"); }
      }

      /// <summary>
      ///    Максимальное время в часах, на котором можно заканчивать публикацию
      /// </summary>
      public static int EndPostingHour
      {
         get { return Get<int>("EndPostingHour"); }
      }

      /// <summary>
      ///    Минимальный интревал в секундах между публикациями ссылок
      /// </summary>
      public static int MinTimeslotSeconds
      {
         get { return Get<int>("MinTimeslotSeconds"); }
      }

      /// <summary>
      ///    Максимальный интревал в секундах между публикациями ссылок
      /// </summary>
      public static int MaxTimeslotSeconds
      {
         get { return Get<int>("MaxTimeslotSeconds"); }
      }

      private static T Get<T>(string key)
      {
         try
         {
            return (T)AppSettingsReader.GetValue(key, typeof(T));
         }
         catch (InvalidOperationException invalidOperationEx)
         {
            throw new SocialConfigurationException(
               string.Format("Key {0} does not exists in App.config or incompatible type", key), invalidOperationEx);
         }
         catch (ArgumentNullException argNullEx)
         {
            throw new SocialConfigurationException(string.Format("Key {0} is not found", key), argNullEx);
         }
      }

      /// <summary>
      ///    Параметры отправки Email-уведомлений
      /// </summary>
      private static class Email
      {
         public static string FromAddress
         {
            get { return Get<string>("FromAddress"); }
         }

         public static string FromDisplayName
         {
            get { return Get<string>("FromDisplayName"); }
         }

         public static string MailLogin
         {
            get { return Get<string>("MailLogin"); }
         }

         public static string MailPassword
         {
            get { return Get<string>("MailPassword"); }
         }

         public static string SmtpServerName
         {
            get { return Get<string>("SmtpServerName"); }
         }

         public static int SmtpPort
         {
            get { return Get<int>("SmtpPort"); }
         }
      }

      public static class Factory
      {
         /// <summary>
         ///    Конфигурация электронной почты
         /// </summary>
         public static EmailConfiguration EmailConfig
         {
            get
            {
               return new EmailConfiguration
               {
                  FromAddress = Email.FromAddress,
                  FromDisplayName = Email.FromDisplayName,
                  MailLogin = Email.MailLogin,
                  MailPassword = Email.MailPassword,
                  SmtpServerName = Email.SmtpServerName,
                  SmtpPort = Email.SmtpPort
               };
            }
         }

         /// <summary>
         ///    Конфигурация для Twitter'а
         /// </summary>
         public static TwitterConfiguration TwitterConfig
         {
            get
            {
               return new TwitterConfiguration
               {
                  ConsumerKey = Twitter.ConsumerKey,
                  ConsumerSecret = Twitter.ConsumerSecret,
                  AccessToken = Twitter.AccessToken,
                  AccessTokenSecret = Twitter.AccessTokenSecret
               };
            }
         }

         /// <summary>
         ///    Конфигурация для Reddit'а
         /// </summary>
         public static RedditConfiguration RedditConfig
         {
            get
            {
               return new RedditConfiguration
               {
                  UserName = Reddit.Username,
                  Password = Reddit.Password,
                  Subreddits = Reddit.Subreddits
               };
            }
         }

         /// <summary>
         ///    Получатели почтовых уведомлений
         /// </summary>
         public static IEnumerable<EmailRecipient> EmailRecipients
         {
            get
            {
               var recipientsConfigSections =
                  ConfigurationManager.GetSection("EmailRecipients") as RecipientsConfigSections;
               if (recipientsConfigSections != null)
               {
                  return (from RecipientElement recipient in recipientsConfigSections.Recipients
                          select new EmailRecipient
                          {
                             ToDisplayName = recipient.DisplayName,
                             ToAddress = recipient.Address
                          }).ToList();
               }

               throw new SocialConfigurationException("Section 'EmailRecipients' not registered in App.config");
            }
         }

         /// <summary>
         /// Конфигурация для Pastebin
         /// </summary>
         public static PastebinConfiguration PastebinConfig
         {
            get
            {
               return new PastebinConfiguration
               {
                  DefaultEncoding = Pastebin.DefaultEncoding,
                  MinimumPastedDataLength = Pastebin.MinimumPastedDataLength,
                  WebSiteUrl = Pastebin.WebSiteUrl
               };
            }
         }
      }

      /// <summary>
      ///    Параметры для Reddit'а
      /// </summary>
      private static class Reddit
      {
         /// <summary>
         ///    Имя пользователя в Reddit
         /// </summary>
         public static string Username
         {
            get { return Get<string>("RedditUsername"); }
         }

         /// <summary>
         ///    Пароль пользователя в Reddit
         /// </summary>
         public static string Password
         {
            get { return Get<string>("RedditPassword"); }
         }

         /// <summary>
         ///    Subreddit'ы
         /// </summary>
         public static IEnumerable<string> Subreddits
         {
            get
            {
               string[] rawSubreddits = Get<string>("Subreddits")
                  .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
               return rawSubreddits.Select(rawSubreddit => rawSubreddit.Trim()).ToList();
            }
         }
      }

      /// <summary>
      ///    Параметры для Twitter'а
      /// </summary>
      private static class Twitter
      {
         public static string ConsumerKey
         {
            get { return Get<string>("TwitterConsumerKey"); }
         }

         public static string ConsumerSecret
         {
            get { return Get<string>("TwitterConsumerSecret"); }
         }

         public static string AccessToken
         {
            get { return Get<string>("TwitterAccessToken"); }
         }

         public static string AccessTokenSecret
         {
            get { return Get<string>("TwitterAccessTokenSecret"); }
         }
      }

      /// <summary>
      /// Параметры для Pastebin
      /// </summary>
      private static class Pastebin
      {
         public static string WebSiteUrl
         {
            get { return Get<string>("WebSiteUrl"); }
         }

         public static string DefaultEncoding
         {
            get { return Get<string>("DefaultEncoding"); }
         }

         public static int MinimumPastedDataLength
         {
            get { return Get<int>("MinimumPastedDataLength"); }
         }
      }
   }
}