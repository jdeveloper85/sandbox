﻿/**
 * Пример извлечение Reddit'ов с определенного URL
 */

using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Pvs.Autopost.Config;
using Pvs.Autoposting.Entities;
using RedditSharp;
using RedditSharp.Things;
using SubredditPost = RedditSharp.Things.Post;
using PostEntity = Pvs.Autoposting.Entities.Post;

namespace GetFromRedditSample
{
   internal static class Program
   {
      private const int TakeLimit = 1000;

      private static void Main()
      {
         string username = SocialKeysReader.Factory.RedditConfig.UserName;
         string password = SocialKeysReader.Factory.RedditConfig.Password;
         var reddit = new Reddit(username, password);
         reddit.LogIn(username, password);
         IEnumerable<string> subredditNames = SocialKeysReader.Factory.RedditConfig.Subreddits;
         var subscribePosts = new List<SubredditPost>();
         foreach (string subredditName in subredditNames)
         {
            Subreddit subreddit = reddit.GetSubreddit(subredditName);
            subreddit.Subscribe();
            subscribePosts.AddRange(subreddit.New.Take(TakeLimit));
         }

         int changesCount = 0;
         try
         {
            using (var autopostEntities = new SocialAutopostEntities())
            {
               foreach (SubredditPost subscribePost in subscribePosts)
               {
                  string postUri = subscribePost.Url.AbsoluteUri;
                  var postUris = new HashSet<string>();
                  foreach (var post in autopostEntities.Posts)
                  {
                     postUris.Add(post.PureUri);
                  }

                  if (!postUris.Contains(postUri))
                  {
                     var postEntity = new PostEntity
                     {
                        PureUri = postUri,
                        PostText = string.Format("{0} posted a link {1}", subscribePost.AuthorName, postUri),
                        CreatedDate = DateTime.Now
                     };

                     autopostEntities.Posts.Add(postEntity);
                     changesCount += autopostEntities.SaveChanges();
                  }
               }
            }
         }
         catch (DbUpdateException updateEx)
         {
            Console.WriteLine(updateEx.Message);
            Exception innerEx = updateEx.InnerException;
            if (innerEx != null)
            {
               Console.WriteLine(innerEx.Message);
            }
         }
         finally
         {
            Console.WriteLine("Saved count: {0}", changesCount);
         }
      }
   }
}