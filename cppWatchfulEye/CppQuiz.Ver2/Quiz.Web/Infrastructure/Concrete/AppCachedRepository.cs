﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CppQuiz.Data;
using Pvs.Quiz.Common;
using Quiz.Web.Infrastructure.Abstract;

namespace Quiz.Web.Infrastructure.Concrete
{
   public class AppCachedRepository : IQuizRepository<QuizQuestion>
   {
      private readonly IQuizRepository<QuizQuestion> _wrappedRepository;
      private QuizQuestion[] _questions;

      public AppCachedRepository(IQuizRepository<QuizQuestion> wrappedRepository)
      {
         _wrappedRepository = wrappedRepository;
      }

      public QuizQuestion[] QuizQuestions
      {
         private get { return _questions; }
         set
         {
            var dynQuestions = new List<QuizQuestion> { QuizQuestion.Empty };
            dynQuestions.AddRange(value);
            _questions = dynQuestions.ToArray();
         }
      }

      public void Dispose()
      {
         _wrappedRepository.Dispose();
      }

      public async Task<QuizQuestion> GetQuestionAsync(int questionId)
      {
         return await Task.Run(() => QuizQuestions[questionId]);
      }

      public async Task<byte[]> GetProjectLogoAsync(int questionId)
      {
         return await Task.Run(() => QuizQuestions[questionId].ProjectLogo);
      }

      public async Task<bool> UpdateQuestionStatAsync(int questionId, AnswerState answerState)
      {
         return await _wrappedRepository.UpdateQuestionStatAsync(questionId, answerState);
      }
   }
}