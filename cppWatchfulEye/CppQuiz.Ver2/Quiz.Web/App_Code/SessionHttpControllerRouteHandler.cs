﻿/**
 * Обработчики контроллеров для косвенной поддержки сеансов в WebAPI
 */

using System.Web;
using System.Web.Http.WebHost;
using System.Web.Routing;
using System.Web.SessionState;

namespace Quiz.Web
{   
   public class SessionHttpControllerRouteHandler : HttpControllerRouteHandler
   {
      protected override IHttpHandler GetHttpHandler(RequestContext requestContext)
      {
         return new SessionControllerHandler(requestContext.RouteData);
      }

      private class SessionControllerHandler : HttpControllerHandler, IRequiresSessionState
      {
         public SessionControllerHandler(RouteData routeData)
            : base(routeData)
         {
         }
      }
   }
}