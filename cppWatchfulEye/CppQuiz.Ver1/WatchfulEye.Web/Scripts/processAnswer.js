﻿/// <reference path="jquery-2.1.1.js" />
$(document).ready(function () {

    $('#dontKnow').show();
    $('#nextQuestion').hide();
    $('#explanatoryText').hide();
    $('#pvsWarning').hide();
    $('#static-warn-message').hide();
    $('#current-score').hide();

    var timeisup = false;
    var alreadyAnswered = false;
    var timeOut = null;
    
    var countdownEl = $('.countdown');  // Установка таймаута для вопроса
    var questionNumber = parseInt(document.URL.substring(document.URL.lastIndexOf('/') + 1));
    $.ajax({
        url: location.protocol + '//' + location.host + '/GetTimeSpentHandler.ashx?QuestionId=' + questionNumber,
        type: 'GET',
        success: function (result) {
            countdownEl.html(result);            
            var seconds = parseInt(countdownEl.html()); // Установка таймера
            if (isNaN(seconds)) {
                $('#secondsLeft').html(result);
            } else {
                timeOut = setInterval(function () {
                    if (seconds === 0) {
                        $.ajax({
                            type: 'POST',
                            data: { answerStatus: 'TimeIsUp' },
                            success: function () {
                                $('#current-score').show();
                                $.ajax({
                                    url: location.protocol + '//' + location.host + '/GetCurrentScoreHandler.ashx',
                                    type: 'GET',
                                    success: function (res) {
                                        $('#score').html(res);
                                    },
                                    error: function (res) {
                                        $('#score').html('Error: ' + res);
                                    }
                                });
                            }
                        });
                        $('#secondsLeft').html('Time is up');
                        clearTimeout(timeOut);
                        timeisup = true;
                    } else {
                        seconds--;
                        countdownEl.html(seconds);
                    }
                }, 1000);
            }
        },
        error: function (result) {
            countdownEl.html(result);
        }
    });    
    
    $('.highlight-fragment').bind('click', function () {    // Клик по фрагменту кода
        if (!timeisup && !alreadyAnswered && timeOut != null) {
            var status = $(this).hasClass('right-fragment') ? 'Right' : 'Wrong';                        
            if (status === 'Right') {
                $('#rightAnswer').html('CORRECT').css('color', 'green').css('font-weight', 'bold');
            } else if (status === 'Wrong') {
                $('#rightAnswer').html('INCORRECT').css('color', 'red').css('font-weight', 'bold');
            }

            $.ajax({
                type: 'POST',
                data: { answerStatus: status },
                success: function () {
                    $('#current-score').show();
                    $.ajax({
                        url: location.protocol + '//' + location.host + '/GetCurrentScoreHandler.ashx',
                        type: 'GET',
                        success: function (result) {
                            $('#score').html(result);
                        },
                        error: function (result) {
                            $('#score').html('Error: ' + result);
                        }
                    });
                }
            });

            clearTimeout(timeOut);
        }

        $('.right-fragment').css('color', 'red').css('font-weight', 'bolder');
        $('#dontKnow').hide();
        $('#explanatoryText').show();
        $('#pvsWarning').show();
        $('#static-warn-message').show();
        $('#nextQuestion').show();
        $('#secondsLeft').hide();

        alreadyAnswered = true;
    });

    // Клик на кнопке 'Не знаю'
    $('#dontknowAction').bind('click', function () {
        if (!timeisup && !alreadyAnswered && timeOut != null) {
            $.ajax({
                type: 'POST',
                data: { answerStatus: 'DontKnow' },
                success: function () {
                    $('#current-score').show();
                    $.ajax({
                        url: location.protocol + '//' + location.host + '/GetCurrentScoreHandler.ashx',
                        type: 'GET',
                        success: function (result) {
                            $('#score').html(result);
                        },
                        error: function (result) {
                            $('#score').html('Error: ' + result);
                        }
                    });
                }
            });

            clearTimeout(timeOut);
        }

        $('.right-fragment').css('color', 'red').css('font-weight', 'bolder');
        $('#dontKnow').hide();
        $('#explanatoryText').show();
        $('#pvsWarning').show();
        $('#static-warn-message').show();
        $('#nextQuestion').show();

        alreadyAnswered = true;
        $('#secondsLeft').hide();
    });
});