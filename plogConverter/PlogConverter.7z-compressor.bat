if exist "PlogConverter_src.zip" del /F "PlogConverter_src.zip"
cd Pvs.PlogConverter
for /d /r "." %%d in (obj) do @if exist "%%d" rd /s/q "%%d"
for /d /r "." %%d in (bin) do @if exist "%%d" rd /s/q "%%d"
cd ..\..\
svn export PlogConverter Converter
cd ..
7z a Utility/PlogConverter/PlogConverter_src.zip -aoa Utility/Converter/* -i!Nuget\packages\CommandLineParser.1.9.71\lib\net40\CommandLine.dll -i!PVS-Studio\PVS-Studio\CommonTypes\DataTableConsts.cs  -i!PVS-Studio\PVS-Studio\CommonTypes\ErrorInfo.cs -xr!*.txt -xr!*.bat -xr!*.suo -xr!*.user -xr!bin -xr!obj -xr!?svn\*
cd Utility
rmdir /s /q Converter
cd PlogConverter